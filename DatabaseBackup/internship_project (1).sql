-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 09, 2021 at 06:37 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `internship_project`
--

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(6) NOT NULL,
  `name` varchar(30) NOT NULL,
  `createdBy` int(6) NOT NULL,
  `updatedBy` int(6) NOT NULL,
  `createdDttm` int(13) NOT NULL,
  `updatedDttm` int(13) NOT NULL,
  `isActive` int(1) NOT NULL DEFAULT 1,
  `isDeleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cart`
--

CREATE TABLE `cart` (
  `id` int(6) NOT NULL,
  `productId` int(6) NOT NULL,
  `userId` int(6) NOT NULL,
  `quantity` int(6) NOT NULL,
  `createdBy` int(6) NOT NULL,
  `updatedBy` int(6) NOT NULL,
  `createdDttm` int(13) NOT NULL,
  `updatedDttm` int(13) NOT NULL,
  `isActive` int(1) NOT NULL DEFAULT 1,
  `isDeleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  `createdBy` int(6) NOT NULL,
  `updatedBy` int(6) NOT NULL,
  `createdDttm` int(13) NOT NULL,
  `updatedDttm` int(13) NOT NULL,
  `isActive` int(1) NOT NULL DEFAULT 1,
  `isDeleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customeraddress`
--

CREATE TABLE `customeraddress` (
  `id` int(6) NOT NULL,
  `userId` int(6) DEFAULT NULL,
  `addressLine1` varchar(50) DEFAULT NULL,
  `addressLine2` varchar(50) DEFAULT NULL,
  `city` varchar(15) DEFAULT NULL,
  `state` varchar(20) DEFAULT NULL,
  `pincode` int(6) DEFAULT NULL,
  `createdBy` int(6) DEFAULT NULL,
  `updatedBy` int(6) DEFAULT NULL,
  `createdDttm` int(13) DEFAULT NULL,
  `updatedDttm` int(13) DEFAULT NULL,
  `isActive` int(1) NOT NULL DEFAULT 1,
  `isDeleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(6) NOT NULL,
  `userId` int(6) NOT NULL,
  `productId` int(6) NOT NULL,
  `feedback` varchar(1000) NOT NULL,
  `createdBy` int(6) NOT NULL,
  `updatedBy` int(6) NOT NULL,
  `createdDttm` int(13) NOT NULL,
  `updatedDttm` int(13) NOT NULL,
  `isActive` int(1) NOT NULL DEFAULT 1,
  `isDeleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `imagetable`
--

CREATE TABLE `imagetable` (
  `id` int(6) NOT NULL,
  `productId` int(6) NOT NULL,
  `imageURL` varchar(512) NOT NULL,
  `imageSequence` int(2) NOT NULL,
  `createdBy` int(6) NOT NULL,
  `updatedBy` int(6) NOT NULL,
  `createdDttm` int(13) NOT NULL,
  `updatedDttm` int(13) NOT NULL,
  `isActive` int(1) NOT NULL DEFAULT 1,
  `isDeleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orderitems`
--

CREATE TABLE `orderitems` (
  `id` int(6) NOT NULL,
  `orderId` int(6) NOT NULL,
  `productId` int(6) NOT NULL,
  `productPrise` int(6) NOT NULL,
  `quantity` int(6) NOT NULL,
  `createdBy` int(6) NOT NULL,
  `updatedBy` int(6) NOT NULL,
  `createdDttm` int(13) NOT NULL,
  `updatedDttm` int(13) NOT NULL,
  `isActive` int(1) NOT NULL DEFAULT 1,
  `isDeleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(6) NOT NULL,
  `userId` int(6) NOT NULL,
  `contact` varchar(10) NOT NULL,
  `price` int(7) NOT NULL,
  `addressLine1` varchar(50) NOT NULL,
  `addressLine2` varchar(50) NOT NULL,
  `city` varchar(15) NOT NULL,
  `state` varchar(20) NOT NULL,
  `pincode` int(6) NOT NULL,
  `orderStatus` varchar(3) NOT NULL,
  `createdBy` int(6) NOT NULL,
  `updatedBy` int(6) NOT NULL,
  `createdDttm` int(13) NOT NULL,
  `updatedDttm` int(13) NOT NULL,
  `isActive` int(1) NOT NULL DEFAULT 1,
  `isDeleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(6) NOT NULL,
  `categoryId` int(6) NOT NULL,
  `subCategoryId` int(6) NOT NULL,
  `brandId` int(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  `productDescription` varchar(1000) NOT NULL,
  `model` varchar(100) NOT NULL,
  `currentStock` int(7) NOT NULL,
  `barcode` varchar(50) NOT NULL,
  `prise` int(7) NOT NULL,
  `tag` varchar(512) NOT NULL,
  `createdBy` int(6) NOT NULL,
  `updatedBy` int(6) NOT NULL,
  `createdDttm` int(13) NOT NULL,
  `updatedDttm` int(13) NOT NULL,
  `isActive` int(1) NOT NULL DEFAULT 1,
  `isDeleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `productspecification`
--

CREATE TABLE `productspecification` (
  `id` int(6) NOT NULL,
  `productId` int(6) NOT NULL,
  `key` varchar(30) NOT NULL,
  `value` varchar(1000) NOT NULL,
  `createdBy` int(6) NOT NULL,
  `updatedBy` int(6) NOT NULL,
  `createdDttm` int(13) NOT NULL,
  `updatedDttm` int(13) NOT NULL,
  `isActive` int(1) NOT NULL DEFAULT 1,
  `isDeleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(6) NOT NULL,
  `name` varchar(30) NOT NULL,
  `createdBy` int(6) NOT NULL,
  `updatedBy` int(6) NOT NULL,
  `createdDttm` int(13) NOT NULL,
  `updatedDttm` int(13) NOT NULL,
  `isActive` int(1) NOT NULL DEFAULT 1,
  `isDeleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `stock`
--

CREATE TABLE `stock` (
  `id` int(6) NOT NULL,
  `productId` int(6) NOT NULL,
  `brandId` int(6) NOT NULL,
  `subCategoryId` int(6) NOT NULL,
  `userId` int(6) NOT NULL,
  `totalStock` int(8) NOT NULL,
  `remainingStock` int(8) NOT NULL,
  `totalPrice` int(8) NOT NULL,
  `purchasePrisePerProduct` int(6) NOT NULL,
  `sellingPrisePerProduct` int(6) NOT NULL,
  `createdBy` int(6) NOT NULL,
  `updatedBy` int(6) NOT NULL,
  `createdDttm` int(13) NOT NULL,
  `updatedDttm` int(13) NOT NULL,
  `isActive` int(1) NOT NULL DEFAULT 1,
  `isDeleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subcategory`
--

CREATE TABLE `subcategory` (
  `id` int(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  `createdBy` int(6) NOT NULL,
  `updatedBy` int(6) NOT NULL,
  `createdDttm` int(13) NOT NULL,
  `updatedDttm` int(13) NOT NULL,
  `isActive` int(1) NOT NULL DEFAULT 1,
  `isDeleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tagtable`
--

CREATE TABLE `tagtable` (
  `id` int(6) NOT NULL,
  `name` varchar(30) NOT NULL,
  `createdBy` int(6) NOT NULL,
  `updatedBy` int(6) NOT NULL,
  `createdDttm` int(13) NOT NULL,
  `updatedDttm` int(13) NOT NULL,
  `isActive` int(1) NOT NULL DEFAULT 1,
  `isDeleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(6) NOT NULL,
  `roleId` int(6) DEFAULT NULL,
  `name` varchar(30) NOT NULL,
  `contact` varchar(10) NOT NULL,
  `emailId` varchar(100) NOT NULL,
  `gender` varchar(1) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `password` varchar(15) NOT NULL,
  `isVerified` int(1) NOT NULL DEFAULT 0,
  `verificationKey` varchar(512) DEFAULT NULL,
  `createdBy` int(6) DEFAULT NULL,
  `updatedBy` int(6) DEFAULT NULL,
  `createdDttm` int(13) DEFAULT NULL,
  `updatedDttm` int(13) DEFAULT NULL,
  `isActive` int(1) NOT NULL DEFAULT 1,
  `isDeleted` int(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customeraddress`
--
ALTER TABLE `customeraddress`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `imagetable`
--
ALTER TABLE `imagetable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orderitems`
--
ALTER TABLE `orderitems`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `productspecification`
--
ALTER TABLE `productspecification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategory`
--
ALTER TABLE `subcategory`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tagtable`
--
ALTER TABLE `tagtable`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `emailId` (`emailId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customeraddress`
--
ALTER TABLE `customeraddress`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `imagetable`
--
ALTER TABLE `imagetable`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orderitems`
--
ALTER TABLE `orderitems`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `productspecification`
--
ALTER TABLE `productspecification`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock`
--
ALTER TABLE `stock`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subcategory`
--
ALTER TABLE `subcategory`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tagtable`
--
ALTER TABLE `tagtable`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
