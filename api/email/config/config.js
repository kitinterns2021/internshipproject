// getting google as a named export from googleapis
const { google } = require("googleapis");

// importing nodemailer
const nodemailer = require("nodemailer");


// configuring keys required for authentication
const CLIENT_ID = '331355448750-elnttrv1j0n5jbtbou9c3tnesheujviu.apps.googleusercontent.com';
const CLIENT_SECRET = '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';
const REDIRECT_URI = 'https://developers.google.com/oauthplayground'; //==> Message will be redirected to this url and will get authenticated by google OAuth2 and then will be sent to the requested gmail id.
const REFRESH_TOKEN = '1//04qRnncmp2CBqCgYIARAAGAQSNwF-L9IrTb0iDJZUXm5SpQsU5Q-dAQfpGZ2BEEljZyBvaEknHapdYkGZnv1LyJDjeSSVyJTPnC8';


// creating new OAuth2 cliend
const OAuth2Client = new google.auth.OAuth2(CLIENT_ID, CLIENT_SECRET, REDIRECT_URI);

// setting credientials for refresh tokens
OAuth2Client.setCredentials({ refresh_token: REFRESH_TOKEN }); //==> Here, we are not sing Access token because access token will be expire after few minutes, and we have to create a new access token. So insted of it, we are using refresh token


// creating asynchronous function to transport the authenticated transporter
const transporter = async () => {

    // getting the access token
    const accessToken = await OAuth2Client.getAccessToken();//generated new access token

    // creating nodemailer transporter to transport the email
    const mailTransporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
            type: "OAuth2",
            user: 'kitinterns2021@gmail.com',
            clientId: CLIENT_ID,
            clientSecret: CLIENT_SECRET,
            refreshToken: REFRESH_TOKEN,
            accessToken: accessToken
        }
    });

    return mailTransporter;
    
}//end transporter

// exporting the transporter
module.exports = transporter;