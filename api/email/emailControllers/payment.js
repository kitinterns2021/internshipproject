const paymentSuccess = async (data) => {
    let start = `
    <!DOCTYPE html>
<html lang="en">

<head>
    <style>
        .table {
            width: 100%;
            border-collapse: separate;
            border-spacing: 0 1.5rem;
        }

        .td {
            width: 70px;
            padding: 10px 10px 10px 10px;
        }
    </style>
</head>

<body>
    <div
        style="border: 0.01rem solid lightgrey; border-radius: 10px; margin:1rem 1rem 1rem 1rem; padding:1rem 1rem 1rem 1rem">
        <h1>
            ${data.title}
        </h1>

        <p>
            Dear ${data.name},<br> ${data.message}
        </p>
        <hr>

        <div style="border:0.001rem solid lightgrey;border-radius:8px; padding:1rem 1rem 1rem 1rem;">
            <h2 style="color:orange">Order Items</h2>
            <hr>

            <div style="margin:1rem 1rem 1rem 1rem;">
                <table class="table">
        `;

    let end = `
        </table>
        <hr>

        <div style="border:0.001rem solid lightgrey;border-radius:8px; padding:1rem 1rem 1rem 1rem">
            <h2 style="color:orange">
                Order Summary
            </h2>
            <hr>

            <table>
                <tr>
                    <th style="width: 100px;text-align: left;">
                        Total Price:
                    </th>
                    <th>
                        RS. ${data.totalPrice}
                    </th>
                </tr>
                <tr style="width: 100px;text-align: left;">
                    <th>
                        Delivery Fees:
                    </th>
                    <th>
                        RS. ${data.deliveryCharges}
                    </th>
                </tr>
            </table>

            <hr>

            <table>
                <tr>
                    <th style="width: 100px;text-align: left;">
                        Order Total:
                    </th>
                    <th>
                        RS. ${data.grandTotal}
                    </th>
                </tr>
            </table>
        </div>
        <hr>

                <h3>
                    Shipping Address
                </h3>
                <b>
                ${
                    data.address.addressLine1 + '<br>' +
                    data.address.addressLine2 + '<br>' +
                    data.address.city + '<br>' +
                    data.address.state + '<br>' +
                    data.address.pincode
                }
                </b>
            </div>

            <p>
                Thank You For Using Fathom Services
            </p>
        </div>
    </div>
</body>

</html>
        `;


    // mapping the table row with given order items array
    let row = "";
    data.orderItems.map(element => {
        return row += (`
            <tr>
                        <td class="td">
                            <img src="cid:${element.img}" width="50px" height="50px"
                                style="margin-right: 20px;" alt="" />
                        </td>
                        <td colspan="5">
                            <h3 style="margin-top: 0px;margin-bottom: 0.1rem;">${element.productName}</h3>
                            <b>
                                <p style="color: grey;">
                                    Qty: ${element.qty}
                                </p>
                                
                                <p style="color: green;margin-top:5px;">
                                    RS. ${element.total}
                                </p>
                            </b>

                        </td>
                    </tr>`)
    });

    const finalMessage = start + row + end;
    return finalMessage;
}

module.exports = paymentSuccess;