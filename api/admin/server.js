const app = require("./Config/serverConfig");
const db = require("./Config/db");

// importing api controllers
const login = require("./controller/login");
const admin = require("./controller/admin");
const dashboard = require("./controller/dashboard");
const orders = require("./controller/orders");
const subCategory = require("./controller/subCategory");
const brand = require("./controller/brand");
const category = require("./controller/category");
const productImages = require("./controller/productImages");
const addProducts = require("./controller/addProducts");
const productSpecification = require("./controller/ProductSpecification");
const tag = require("./controller/tag");
const reports = require("./controller/reports");

// logging in to the system
app.get("/login", login);

// get current user/admin information according to its id
app.get("/admin-information", admin);

// getting all users
app.get("/all-users", (req, res) => {
    res.statusCode = 200;
    const query = "select *, FROM_UNIXTIME(dob,'%D-%M-%Y') as dob, FROM_UNIXTIME(createdDttm,'%D-%M-%Y') as createdDttm, FROM_UNIXTIME(updatedDttm,'%D-%M-%Y') as updatedDttm from users where  isDeleted = 0 and roleId = 3";

    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        res.send(rows);
    })
})

// getting dashboard data
app.get("/dashboard", dashboard.dashboard);


// Getting all orders data
app.get("/orders", orders.orders)

// Getting all order items related with order id
app.get("/orders/order-items", orders.orderItems)

// dispatching the order or updating the order status
app.put("/order/update-order-status", orders.updateOrderStatus)




// Sub Category end points

// getting all sub categories
app.get("/sub-categories", subCategory.subCategories);

// adding sub category
app.post("/sub-categories/add-subCategory", subCategory.addSubCategory);

// updating sub category
app.put("/sub-categories/update-subCategory", subCategory.updateSubCategory);

// deleting sub category
app.delete("/sub-categories/delete-subCategory", subCategory.deleteSubCategory);




//Brand end points

//for brand
app.get("/brands", brand.brands);

// adding brand
app.post("/brands/add-brand", brand.addBrand);

//update brand
app.put("/brands/update-brand", brand.updateBrand);

// //for maxid of brands
// app.get("/brands/maxid", brand.getId);

// deleting brand
app.delete("/brands/delete-brand", brand.deleteBrand);



// tag table endpoints
// getting all tags
app.get("/tags",tag.getAllTags);

// inserting tag
app.post("/tags/add-tag",tag.addTag);

// updating tag
app.put("/tags/update-tag",tag.updateTag);

// deleting tag
app.delete("/tags/delete-tag",tag.deleteTag);


//category endpoints
//for category
app.get("/categories", category.category);

app.post("/categories/add-category", category.addCategory);

//update category
app.put("/categories/update-category", category.updateCategory);

// deleting category
app.delete("/categories/delete-category", category.deleteCategory);





// addProducts end points
//getting product data
app.get("/product-table", addProducts.productTable);

//geeting product data on form
app.get("/get-product", addProducts.getProductData);

//deleting product
app.delete("/product-delete", addProducts.deleteProduct);

//inserting products
app.post("/add-product", addProducts.addProduct);


//update products
app.put("/update-product", addProducts.updateProduct);


//productSpecification end points
//getting product specification data
app.get("/product-specification", productSpecification.getSpecification);

// getting product name
app.get("/product-specification/get-product", productSpecification.getProduct);

// deleting product specification
app.delete("/product-specification/delete-specification", productSpecification.deleteSpecification);

// updating product specification
app.put("/productspecs/update-specification", productSpecification.updateSpecification);


//inserting product specification
app.post("/productspecs", productSpecification.addSpecification);


// getting product images related with product id
app.get("/product/product-images", productImages.getImages);


// getting super admin and admin name and its id to show in every selected tag of createdBy or updatedBy and product name and its id
app.get("/get-admin", productImages.getImages);

//adding product images
app.post("/products/product-images/add-product-image", productImages.addImages);

// updating product image status
app.put('/products/product-images/edit-product-image', productImages.editImageStatus);

// deleting product image
app.delete("/products/product-images/delete-product-image", productImages.deleteImage);




app.get(`/get-product`, (req, res) => {
    const query = `select id, name from category where isActive=1 and isDeleted=0;select id, name from subcategory where isActive=1 and isDeleted=0;select id, name from brands where isActive=1 and isDeleted=0;select id, name from tagtable where isActive=1 and isDeleted=0`;
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err)
        }
        else {
            res.send(rows);
        }
    })
})




// getting reports
app.get("/reports/users",reports.getUsers);