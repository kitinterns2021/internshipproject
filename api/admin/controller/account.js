const db = require("../Config/db");

// getting all sub categories
const admininfo = (req, res) => {
  // const query = "select * from subcategory where isActive = 1 and isDeleted = 0";
  if (req.query.id) {
    const query = `select id, name,emailId,password, isActive as status from users where isDeleted = 0 and id = ${req.query.id};select name as userName from users where (roleId = 1 or roleId = 2) and isDeleted = 0 and id = ${req.query.userId}`;
    db.query(query, (err, rows, fields) => {
        if(err)
        {
            console.log(err);
        }
        res.send(rows);
    })
}
else {
    query = "select id, name, emailId,password ,isActive as status from users where roleId = 2 and isDeleted = 0 order by users.id";
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        res.send(rows);
    })
}

};
// end subCategories

// adding subCategory
const addadmininfo = (req, res) => {
  res.statusCode = 200;

  const query = `INSERT INTO users (id, roleId,name, contact, emailId, gender, dob, Password,isVerified,verificationKey,createdBy, updatedBy, createdDttm, updatedDttm, isActive, isDeleted) VALUES (NULL, '2', '${req.body.name}', NULL, '${req.body.emailId}', NULL,NULL, '${req.body.password}',NULL,NULL, '1', '1', '${req.body.date}', '${req.body.date}' , '1', '0');`; db.query(query, (err, rows, fields) => {
    if (err) {
      console.log(err);
    } else {
      res.send("ok");
    }
  });
}; // end addSubCategory

// updating sub category
const updateadmininfo = (req, res) => {
  res.statusCode = 200;

  const query = `UPDATE users SET name = '${req.body.name}',emailId= '${req.body.emailId}',password = '${req.body.password}', isActive = '${req.body.status}' WHERE id = ${req.body.id} and isDeleted = 0`;
  db.query(query, (err, rows, fields) => {
    if (err) {
      console.log(err);
    } else {
      res.send("ok");
    }
  });
}; // end updateSubCategory

// deleting sub category
const deleteadmin = (req, res) => {
  res.statusCode = 200;

  const query = `UPDATE users set isDeleted = 1 where id = ${req.query.id}`;

  db.query(query, (err, rows, fields) => {
    if (err) {
      console.log(e);
    } else {
      res.send({ textMsg: "Record deleted" });
    }
  });
}; // end deleteSubCategory

// exporting all api handlers as object
module.exports = {
  admininfo,
  addadmininfo,
  updateadmininfo,
  deleteadmin,
};
