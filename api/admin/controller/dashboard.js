const db = require("../Config/db");

// getting data for dashboard
const dashboard = (req, res) => {

    res.statusCode = 200;


    const query = `select count(*) as totalUsers from users where isActive = 1 and isDeleted = 0 and roleId = 3;
    select count(*) as totalBrands from brands where isActive = 1 and isDeleted = 0;
    select count(*) as totalOrders from orders where isActive = 1 and isDeleted = 0 and orderStatus = 'BOK';
    SELECT count(*) as booked from orders where orderStatus = "BOK" and isDeleted = 0;
    SELECT count(*) as dispatched from orders where orderStatus = "DIS" and isDeleted = 0;
    SELECT count(*) as delivered from orders where orderStatus = "DLI" and isDeleted = 0;
    SELECT count(*) as cancelled from orders where orderStatus = "CNL" and isDeleted = 0;
    SELECT count(*) as activeUsers from users where isActive = 1 and isDeleted = 0;
    SELECT count(*) as inactiveUsers from users where isActive = 0 and isDeleted = 0;
    `;

    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        else {
            res.send(
                {
                    users: rows[0][0].totalUsers,
                    brands: rows[1][0].totalBrands,
                    orders: rows[2][0].totalOrders,
                    booked: rows[3][0].booked,
                    dispatched: rows[4][0].dispatched,
                    delivered: rows[5][0].delivered,
                    cancelled: rows[6][0].cancelled,
                    activeUsers: rows[7][0].activeUsers,
                    inactiveUsers: rows[8][0].inactiveUsers
                }
            );
        }
    })
} // end dashboard

// exporting api controllers as object
module.exports = { dashboard }