const db = require("../Config/db");

const productTable = (req, res) => {
    const query = `select category.name as category, subCategory.name as subcategory, brands.name as brand, products.id, products.name, products.model, products.currentStock, products.barcode, products.price, tagtable.name as tagName, products.isActive from products join category on products.categoryId = category.id join subCategory on products.subCategoryId = subCategory.id join brands on products.brandId = brands.id JOIN tagtable ON products.tag = tagtable.id where products.isDeleted = 0 order by products.id`;
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        res.send(rows)
    })
}

const getProductData = (req, res) => {
    let query = '';

    if(req.query.id)
    {
        query = `select id, name from category where isActive=1 and isDeleted=0;select id, name from subcategory where isActive=1 and isDeleted=0;select id, name from brands where isActive=1 and isDeleted=0;select id, name from tagtable where isActive=1 and isDeleted=0;select products.id as id, products.name as Name, products.tag as tagId, products.categoryId, subCategoryId, brandId, productDescription as discription, model, currentStock as Stock, barcode, price as price, FROM_UNIXTIME(products.updatedDttm, '%d/%m/%Y') as date, products.isActive as 'status' from products where products.id = ${req.query.id} and products.isDeleted = 0;`;
    }
    else{
        query = `select id, name from category where isActive=1 and isDeleted=0;select id, name from subcategory where isActive=1 and isDeleted=0;select id, name from brands where isActive=1 and isDeleted=0;select id, name from tagtable where isActive=1 and isDeleted=0;`;
    }
    
    
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err)
        }
        else {
            res.send(rows);
        }
    })
}

const deleteProduct = (req, res) => {
    res.statusCode = 200;

    const query = `UPDATE products set isDeleted = 1 where isActive = 0 and id = ${req.query.id}`;

    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(e);
        }
        else {
            res.send(
                { textMsg: "Record deleted" }
            );
        }
    })
}

const addProduct = (req, res) => {

    console.log(req.body);
    const epoch = Math.floor(new Date().getTime() / 1000.0);
    //  const query = `INSERT INTO productspecification(id, productId, key, value, createdBy, updatedBy, createdDttm, updatedDttm, isActive, isDeleted) VALUES (NULL, '${req.body.productname}','${req.body.key}','${req.body.value}''1','1','1', '1','1','0')`;
    const query = `INSERT INTO products(id, categoryId, subCategoryId, brandId, name, productDescription, model, currentStock, barcode, price, tag, createdBy, updatedBy, createdDttm, updatedDttm, isActive, isDeleted) VALUES (NULL, '${req.body.Category}','${req.body.SubCategoryName}','${req.body.BrandName}', '${req.body.Name}','${req.body.discription}','${req.body.model}','${req.body.Stock}','${req.body.barcode}','${req.body.price}','${req.body.tag}','${req.body.userId}','${req.body.userId}','${epoch}','${epoch}',1,0)`;
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        else {
            res.status(200).send({status:true})
        }
    })

}
const updateProduct = (req, res) => {

    // console.log(req.body);
    const epoch = Math.floor(new Date().getTime() / 1000.0);

    const query = `UPDATE products SET categoryId='${req.body.categoryId}',subCategoryId='${req.body.subCategoryId}',brandId='${req.body.brandId}',name='${req.body.Name}',productDescription='${req.body.discription}',model='${req.body.model}',currentStock='${req.body.Stock}',barcode='${req.body.barcode}',price='${req.body.price}',tag='${req.body.tagId}',updatedBy='${req.body.userId}',createdDttm='${epoch}',updatedDttm='${epoch}', isActive = '${req.body.status === "Active" ? 1 : 0}' WHERE id = ${req.body.id} and isDeleted = 0`;
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        else {
            res.send("record is update successfully")
        }
    })

}



module.exports = { productTable, getProductData, deleteProduct, addProduct, updateProduct };