const db = require("../Config/db");

// get current user/admin information according to its id
const admin = (req,res) =>{

    res.statusCode = 200;

    const query = `select name as userName from users where (roleId = 1 or roleId = 2) and isDeleted = 0 and id = ${req.query.userId}`;
    db.query(query,(err, rows, fields)=>{
        if(err)
        {
            console.log(err);
        }
        res.send(rows);
    })
}//end admin

// exporting api controller function
module.exports = admin;