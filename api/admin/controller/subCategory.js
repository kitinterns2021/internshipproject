const db = require("../Config/db");

// getting all sub categories
const subCategories = (req, res) => {
    // const query = "select * from subcategory where isActive = 1 and isDeleted = 0";
    if (req.query.id) {
        const query = `select id, name, isActive as status from subcategory where isDeleted = 0 and id = ${req.query.id};select name as userName from users where (roleId = 1 or roleId = 2) and isDeleted = 0 and id = ${req.query.userId}`;
        db.query(query, (err, rows, fields) => {
            if(err)
            {
                console.log(err);
            }
            res.send(rows);
        })
    }
    else {
        query = "select subcategory.id, subcategory.name, subcategory.updatedBy, FROM_UNIXTIME(subcategory.createdDttm,'%a %d-%m-%Y') as createdDttm, FROM_UNIXTIME(subcategory.updatedDttm,'%a %d-%m-%Y') as updatedDttm, subcategory.isActive as status, users.name as createdBy from subcategory JOIN users ON subcategory.updatedBy = users.id where (users.roleId = 1 or users.roleId =2) and subcategory.isDeleted = 0 order by subcategory.id";
        db.query(query, (err, rows, fields) => {
            if (err) {
                console.log(err);
            }
            res.send(rows);
        })
    }
}// end subCategories

// adding subCategory
const addSubCategory = (req, res) => {
    res.statusCode = 200;
    const epoch = Math.floor(new Date()/ 1000.0);
    const query = `INSERT INTO subcategory (id, name, createdBy, updatedBy, createdDttm, updatedDttm, isActive, isDeleted) VALUES (NULL, '${req.body.name}', '${req.body.createdBy}', '${req.body.createdBy}', '${epoch}', '${epoch}', '1', '0');`;
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        else {
            res.send("ok");
        }
    });
}// end addSubCategory

// updating sub category
const updateSubCategory =(req,res)=>{

    res.statusCode = 200;
    const epoch = Math.floor(new Date()/ 1000.0);
    const query = `UPDATE subcategory SET name = '${req.body.name}', updatedBy = '${req.body.createdBy}', updatedDttm = '${epoch}', isActive = '${req.body.status}' WHERE id = ${req.body.id} and isDeleted = 0`;
    db.query(query,(err, rows, fields)=>{
        if(err)
        {
            console.log(err);
        }
        else{
            res.send("ok");
        }
    })
}// end updateSubCategory

// deleting sub category
const deleteSubCategory =  (req, res) => {

    res.statusCode = 200;

    const query = `UPDATE subcategory set isDeleted = 1 where id = ${req.query.id}`;

    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(e);
        }
        else {
            res.send(
                { textMsg: "Record deleted" }
            );
        }
        })
} // end deleteSubCategory

// exporting all api handlers as object
module.exports = { subCategories, addSubCategory, updateSubCategory, deleteSubCategory };