const db = require("../Config/db");

// getting all users
const getAdmins = (req,res) => {

    res.statusCode = 200;
    const query =
      "select name,FROM_UNIXTIME(createdDttm,'%D-%M-%Y') as createdDttm, FROM_UNIXTIME(updatedDttm,'%D-%M-%Y') as updatedDttm,isActive as status from users where  isDeleted = 0 and roleId = 2";
  
    db.query(query, (err, rows, fields) => {
      if (err) {
        console.log(err);
      }
      res.send(rows);
    });
    
}//end getUsers


// exporting API controller functions as OBJECT
module.exports = { getAdmins };