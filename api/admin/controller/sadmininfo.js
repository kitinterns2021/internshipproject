const db = require("../Config/db");

const SadminInfo = (req, res) => {
  // const query = "select * from subcategory where isActive = 1 and isDeleted = 0";
  const query = `SELECT name,emailId,isActive as status FROM users WHERE id = ${req.query.userId} and  isDeleted = 0  and roleId = 1`;
  db.query(query, (err, rows, fields) => {
    if (err) {
      console.log(err);
    }
    res.send(rows);
  });
};

const updateSadmin = (req, res) => {
  res.statusCode = 200;
  const currentDate = Math.floor(new Date() / 1000.0);
  const query = `UPDATE users SET name = '${req.body.name}',emailId ='${req.body.emailID}', isActive = '${req.body.status}' WHERE id = ${req.body.userId} and isDeleted = 0`;
  db.query(query, (err, rows, fields) => {
    if (err) {
      console.log(err);
    } else {
      res.send("ok");
    }
  });
};

module.exports = { SadminInfo, updateSadmin };
