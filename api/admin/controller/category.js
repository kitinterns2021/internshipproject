const db = require("../Config/db");

const category = (req, res) => {
    // const query = "select * from subcategory where isActive = 1 and isDeleted = 0";
    if (req.query.id) {
        const query = `select id, name, isActive as status from category where isDeleted = 0 and id = ${req.query.id};select name as userName from users where (roleId = 1 or roleId = 2) and isDeleted = 0 and id = ${req.query.userId}`;
        db.query(query, (err, rows, fields) => {
            if(err)
            {
                console.log(err);
            }
            res.send(rows);
        })
    }
    else {
        query = "select category.id, category.name,category.updatedBy, FROM_UNIXTIME(category.createdDttm,'%a %d-%m-%Y') as createdDttm, FROM_UNIXTIME(category.updatedDttm,'%a %d-%m-%Y') as updatedDttm, category.isActive as status, users.name as createdBy from category JOIN users ON category.updatedBy = users.id where (users.roleId = 1 or users.roleId =2) and category.isDeleted = 0 order by category.id";
        db.query(query, (err, rows, fields) => {
            if (err) {
                console.log(err);
            }
            res.send(rows);
        })
    }
}// end subCategories

const addCategory = (req, res) => {
    res.statusCode = 200;
    const epoch = Math.floor(new Date()/ 1000.0);
    const query = `INSERT INTO category (id, name, createdBy, updatedBy, createdDttm, updatedDttm, isActive, isDeleted) VALUES (NULL, '${req.body.name}', '${req.body.createdBy}', '${req.body.createdBy}', '${epoch}', '${epoch}', '1', '0');`;
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        else {
            res.send("ok");
        }
    });
  }
  

 const updateCategory = (req,res)=>{
    res.statusCode = 200;
    const epoch = Math.floor(new Date()/ 1000.0);
    const query = `UPDATE category SET name = '${req.body.name}', updatedBy = '${req.body.createdBy}', updatedDttm = '${epoch}', isActive = '${req.body.status}' WHERE id = ${req.body.id} and isDeleted = 0`;
    db.query(query,(err, rows, fields)=>{
        if(err)
        {
            console.log(err);
        }
        else{
            res.send("ok");
        }
    })
  }
  
  
  
  const deleteCategory = (req, res) => {
  
    res.statusCode = 200;

    const query = `UPDATE category set isDeleted = 1 where id = ${req.query.id}`;

    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(e);
        }
        else {
            res.send(
                { textMsg: "Record deleted" }
            );
        }
       })
    }

module.exports = { category, addCategory, updateCategory, deleteCategory };