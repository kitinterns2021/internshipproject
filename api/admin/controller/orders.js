const db = require("../Config/db");
const sendMail = require("../../email/emailControllers/sendMail");
const htmlString = require("../../email/emailControllers/payment");

// getting all orders
const orders = (req, res) => {

    // sending status code
    res.statusCode = 200;

    if (!req.query.status) {
        // Firing query
        const query = "select orders.id, users.name, orders.contact, orders.price, orders.orderStatus, FROM_UNIXTIME(orders.createdDttm,'%a %d-%m-%Y') as createdDttm, FROM_UNIXTIME(orders.updatedDttm,'%a %d-%m-%Y') as updatedDttm from orders INNER JOIN users ON orders.userId = users.id where users.roleId = 3 and users.isActive = 1 and users.isDeleted = 0 and orders.isActive = 1 and orders.isDeleted = 0 order by orders.orderStatus";
        db.query(query, (err, rows, fields) => {
            if (err) {
                // res.send({
                //     err: err.toString()
                // });
                console.log("Error: " + err);
            }
            // else {
            res.send(rows)
            // }
        })
    }
    else {
        // Firing query
        const query = `select orders.id, users.name, orders.contact, orders.price, orders.orderStatus, FROM_UNIXTIME(orders.createdDttm,'%a %d-%m-%Y') as createdDttm, FROM_UNIXTIME(orders.updatedDttm,'%a %d-%m-%Y') as updatedDttm from orders INNER JOIN users ON orders.userId = users.id where orders.orderStatus = '${req.query.status}' and users.roleId = 3 and users.isActive = 1 and users.isDeleted = 0 and orders.isActive = 1 and orders.isDeleted = 0 order by orders.updatedDttm asc`;
        db.query(query, (err, rows, fields) => {
            if (err) {
                // res.send({
                //     err: err.toString()
                // });
                console.log("Error: " + err);
            }
            // else {
            res.send(rows)
            // }
        })
    }

} // end orders

// getting all order items related with order id
const orderItems = (req, res) => {
    // const query = `select orders.id, users.name as userName, orders.createdDttm, orders.addressLine1, orders.addressLine2,orders.city,orders.state,orders.pincode, orderitems.id,products.name as productName,orderitems.productPrise,orderitems.quantity, orders.createdDttm from orders INNER JOIN users ON orders.userId = users.id INNER JOIN orderitems on orders.id = orderitems.orderId INNER JOIN products on orderitems.productId = products.id where orderitems.orderId = ${req.query.id} and users.roleId = 3 and users.isActive = 1 and users.isDeleted = 0 and orders.isActive = 1 and orders.isDeleted = 0 order by orders.id`;

    const data = {
        userData: {},
        orderItems: {}
    }

    // getting users data
    const userQuery = `select orders.id, users.name as userName, FROM_UNIXTIME(orders.createdDttm,'%a %d-%m-%Y') as createdDttm, FROM_UNIXTIME(orders.updatedDttm,'%a %d-%m-%Y') as updatedDttm, orders.addressLine1, orders.addressLine2,orders.city,orders.state,orders.pincode from orders JOIN users on orders.userId = users.id where orders.id = ${req.query.id} and users.roleId = 3 and users.isActive = 1 and users.isDeleted = 0 and orders.isDeleted = 0 and orders.isActive = 1`;
    db.query(userQuery, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        data.userData = rows;

        // getting order items data
        const orderItemsQuery = `select orderitems.id, products.name as productName, orderitems.productPrise, orderitems.quantity, FROM_UNIXTIME(orderitems.updatedDttm,'%a %d-%m-%Y') as updatedDttm, imagetable.imageURL from orders  JOIN orderitems on orders.id = orderitems.orderId JOIN products on orderitems.productId = products.id JOIN imagetable on products.id = imagetable.productId where orderitems.orderId = ${req.query.id} and orders.isActive = 1 and orders.isDeleted = 0 and imagetable.id = (select id from imagetable where productId = products.id and imagetable.isActive = 1 LIMIT 1) order by orders.id`;
        db.query(orderItemsQuery, (err1, rows1, fields1) => {
            if (err1) {
                console.log(err1);
            }
            data.orderItems = rows1;
            res.send(data);
        })

    })
} // end orderItems

//creating function for status
const statusFunction = (status) => {
    switch (status) {
        case "DIS":
            return {
                emailTitle: "Order Dispatched!",
                emailSubject: "Order Dispatched!",
                emailMessage: "Your order has been dispatched.",
            };
        case "DLI":
            return {
                emailTitle: "Order Delivered Successfully!",
                emailSubject: "Order Delivered Successfully!",
                emailMessage: "Your order has been delivered to your registered address.",
            };
        case "CNL":
            return {
                emailTitle: "Order Cancelled.",
                emailSubject: "Order Cancelled.",
                emailMessage: "Your order has been cancelled.",
            };
        default:
            return {
                emailTitle: "",
                emailSubject: "",
                emailMessage: "",
            };
    }//end switch case
}//end status function

// dispatching the order or updating the order status
const updateOrderStatus = async (req, res) => {

    // const query = `update orders set isDeleted = 1, isActive = 0 where id = ${req.query.id}`;
    const epoch = Math.floor(new Date() / 1000.0);

    // getting title, subject and message
    const statusObj = statusFunction(req.query.status) ;


    const query = `update orders set orderStatus = "${req.query.status}", updatedDttm = ${epoch} where id = ${req.query.id} and isActive = 1 and isDeleted = 0;
    select users.name, users.emailId, orders.price, orders.addressLine1, orders.addressLine2,orders.city,orders.state,orders.pincode from orders JOIN users on orders.userId = users.id where orders.id = ${req.query.id} and users.roleId = 3 and users.isActive = 1 and users.isDeleted = 0;
    select products.name as productName, (orderitems.productPrise * orderitems.quantity) as total, orderitems.quantity as qty, imagetable.imageURL as img from orders  JOIN orderitems on orders.id = orderitems.orderId JOIN products on orderitems.productId = products.id JOIN imagetable on products.id = imagetable.productId where orderitems.orderId = ${req.query.id} and orders.isActive = 1 and orders.isDeleted = 0 and imagetable.id = (select id from imagetable where productId = products.id and imagetable.isActive = 1 LIMIT 1) order by orders.id;
    `;

    db.query(query, async (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        else {

            // console.log(rows[1])
            // console.log(rows[2])
            // getting prices
            const price = parseInt(rows[1][0].price) - 50;
            const totalPrice = price < 1000 ? price : (price + 50);
            const deliveryCharges = price < 1000 ? 50 : 0;

            // creating array for attachments
            const attachments = [];
            await rows[2].map(element => {
                return attachments.push({
                    filename:element.img,
                    path:`http://localhost:3010/images/${element.img}`,
                    cid:element.img
                })//end push
            })//end map function

            // creating data object to send with email
            const data = {
                name: rows[1][0].name,
                address: rows[1][0],
                orderItems: rows[2],
                totalPrice,
                deliveryCharges,
                grandTotal: rows[1][0].price,
                title:statusObj.emailTitle,
                message:statusObj.emailMessage,
            }//end data object

            // creating html to send
            const html = await htmlString(data);

            // creating email options
            const emailOptions = {
                from: "Fathom Services <fathomable@gmail.com>",
                to: rows[1][0].emailId,
                subject: statusObj.emailSubject,
                html,
                attachments
            }//end emailOptions obj


            // sending email to the user
            const result = await sendMail(emailOptions);
            // console.log(result);
            if(result.accepted.length !== 0)
            {
                res.status(200).send({textMsg:"Action Complete."})
            }
            else{
                // db.rollback((options,err)=>{
                    res.status(500).send({textMsg:"Something Went Wrong. Please Try Again."})
                // })
            }//end if
        }//end else
    })
}// end updateOrderStatus


// exporting all api handlers in a object
module.exports = { orders, orderItems, updateOrderStatus }