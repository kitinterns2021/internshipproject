const db = require("../Config/db");
const fs = require("fs");


// geting all images of product
const getImages = (req, res) => {

    // getting product details
    const query1 = `select category.name as category, subCategory.name as subCategory, brands.name as brand, products.id, products.name from products join category on products.categoryId = category.id join subCategory on products.subCategoryId = subCategory.id join brands on products.brandId = brands.id where products.id = ${req.query.id} and products.isActive = 1 and products.isDeleted = 0`;

    // Getting all images
    // const query2 = `select id, imageSequence, imageURL from imagetable where isActive = 1 and isDeleted = 0 and productId = ${req.query.id} order by imageSequence`;
    const query2 = `select imagetable.id, imagetable.productId, imagetable.imageURL, imagetable.imageSequence, FROM_UNIXTIME(imagetable.createdDttm, '%d-%m-%Y') as createdDttm, FROM_UNIXTIME(imagetable.updatedDttm, '%d-%m-%Y') as updatedDttm, imagetable.isActive, users.name as updatedBy from imagetable JOIN users on imagetable.updatedBy = users.id where imagetable.productId = ${req.query.id} and imagetable.isDeleted = 0 and (users.roleId = 1 or users.roleId = 2) order by imagetable.id`;
    db.query(query1, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        db.query(query2, (err2, rows2, fields2) => {
            if (err2) {
                console.log(err2)
            }

            res.send({
                productDetails: rows[0],
                productImages: rows2
            });
        })
    })
}// end getImages

// adding images
const addImages = (req, res) => {
    // console.log(req.body);
    // console.log(req.files);

    // getting object of all files
    const files = req.files;

    // getting body object
    const body = JSON.parse(req.body.body);
    // console.log(body)

    // creating array for each file status
    const status = [];

    // iterating for each file in the object
    for (let key in files) {
        // getting current epoch for file
        let epoch = Math.floor(new Date().getTime());
        // let epoch = Math.floor(new Date()/1000.0)
        
        // creating filename
        let fileName = `${epoch}_${key}.${files[key].name.split(".").pop()}`;
        
        // moving file to the file system
        files[key].mv("../images/" + fileName, (err) => {
            if (err) {
                status.push({ key: false });
            }
            else {
                // inserting data to the database
                let query = `INSERT INTO imagetable(id,productId,imageURL,imageSequence,createdBy,updatedBy,createdDttm,updatedDttm,isActive,isDeleted) VALUES (NULL, '${body.productId}', '${fileName}', NULL, '${body.createdBy}', '${body.createdBy}', '${epoch}', '${epoch}', '1', '0')`
                db.query(query, (err1, rows1, fields1) => {
                    if (err1) {
                        status.push({ key: false });
                    }
                    else {
                        status.push({ key: true });
                    }
                })
            }//end if-else
        })
    }//end for

    res.send({ status: true });
}

// editing product image status
const editImageStatus = (req, res) => {
    // creating epoch
    const epoch = Math.floor(new Date().getTime());
    // console.log(epoch)

    // creating query
    const query = `update imagetable set isActive=${req.body.status}, updatedBy=${req.body.userId}, updatedDttm = ${epoch} where id = ${req.body.imgId} and isDeleted = 0`;
    db.query(query, (err,rows,fields) =>{
        if(err)
        {
            console.log(err);
            res.send({status: false})
        }
        else{
            res.send({status: true});
        }
    })
}//end editImageStatus

// deleting product image
const deleteImage = (req, res) => {


    // deleting image from filesystem
    if(fs.existsSync(`C:/Software Projects/fathom-main-project/api/images/${req.query.imageUrl}`))
    {
        fs.unlinkSync(`C:/Software Projects/fathom-main-project/api/images/${req.query.imageUrl}`);
    }
    

    const query = `delete from imagetable where id = ${req.query.id}`;
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        res.send("OK");
    })
}//end deleteImage

// exporting all api controllers as object
module.exports = { getImages, deleteImage, addImages, editImageStatus };