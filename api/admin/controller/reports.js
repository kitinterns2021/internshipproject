const db = require("../Config/db");

// getting all users
const getUsers = (req,res) => {

    const query =  `select name, contact, emailId, gender, FROM_UNIXTIME(dob, "%d-%m-%Y") as dob, FROM_UNIXTIME(createdDttm, "%d-%m-%Y") as joiningDate, isActive as status from users where roleId = 3 and isDeleted = 0`;
    db.query(query, (err, rows, fields) => {
        if(err)
        {
            console.log(err);
            res.status(500).send({err:err.toString()});
        }
        else{
            // console.log(rows);
            res.status(200).send(rows)
        }
    })//end db callback function
    
}//end getUsers


// exporting API controller functions as OBJECT
module.exports = { getUsers };