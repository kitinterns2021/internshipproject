const db = require("../Config/db");

// getting all available tags
const getAllTags = (req, res) => {

    let query = "";
    if(req.query.id)
    {
        query = `select tagtable.id, tagtable.name,  tagtable.isActive from tagtable where tagtable.id = ${req.query.id} and tagtable.isDeleted = 0`;
    }
    else {
        query = `select tagtable.id, tagtable.name, FROM_UNIXTIME(tagtable.createdDttm, '%a %d-%m-%Y') as createdDttm, FROM_UNIXTIME(tagtable.updatedDttm, '%a %d-%m-%Y') as updatedDttm, tagtable.isActive, users.name as createdBy from tagtable JOIN users ON tagtable.createdBy = users.id where tagtable.isDeleted = 0`;
    }
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        else {
            res.send(rows);
        }
    })
}//end getAllTags

// inserting tag
const addTag = (req, res) => {
    const body = req.body;
    const epoch = Math.floor(new Date() / 1000.0);

    const query = `INSERT INTO tagtable (id,name,createdBy,updatedBy,createdDttm,updatedDttm,isActive,isDeleted) VALUES (NULL, '${body.name}', '${body.createdBy}', '${body.createdBy}', '${epoch}', '${epoch}', '1', '0')`;
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        else {
            res.send("ok")
        }
    })
}//end addTag

// updating tag
const updateTag = (req,res) =>{

    const body = req.body;
    const epoch = Math.floor(new Date()/ 1000.0);
    const query =  `update tagtable set name = '${body.name}', isActive = ${body.status}, updatedBy = ${body.updatedBy}, updatedDttm = ${epoch} where id = ${body.id} and isDeleted = 0`;

    db.query(query,(err,rows,fields) =>{
        if(err){
            console.log(err);
        }
        else{
            console.log(rows.affectedRows);
            res.send("ok");
        }
    })
}// end updateTag

// deleting the tag
const deleteTag = (req, res) => {
    const id = req.query.id;
    const query = `update tagtable set isDeleted = 1 where id = ${id}`;

    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        else {
            res.send({ txtMsg: "Tag deleted successfully..." });
        }
    })
}//end deleteTag

module.exports = { getAllTags, addTag, updateTag, deleteTag };