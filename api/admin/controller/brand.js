const db = require("../Config/db");

const brands = (req, res) => {
  // const query = "select * from subcategory where isActive = 1 and isDeleted = 0";
  if (req.query.id) {
      const query = `select id, name, isActive as status from brands where isDeleted = 0 and id = ${req.query.id};select name as userName from users where (roleId = 1 or roleId = 2) and isDeleted = 0 and id = ${req.query.userId}`;
      db.query(query, (err, rows, fields) => {
          if(err)
          {
              console.log(err);
          }
          res.send(rows);
      })
  }
  else {
      query = "select brands.id, brands.name, brands.updatedBy, FROM_UNIXTIME(brands.createdDttm,'%a %d-%m-%Y') as createdDttm, FROM_UNIXTIME(brands.updatedDttm,'%a %d-%m-%Y') as updatedDttm, brands.isActive as status, users.name as createdBy from brands JOIN users ON brands.updatedBy = users.id where (users.roleId = 1 or users.roleId =2) and brands.isDeleted = 0 order by brands.id";
      db.query(query, (err, rows, fields) => {
          if (err) {
              console.log(err);
          }
          res.send(rows);
      })
  }

};
// adding brand
const addBrand = (req, res) => {
  res.statusCode = 200;
  const epoch = Math.floor(new Date()/ 1000.0);
  const query = `INSERT INTO brands (id, name, createdBy, updatedBy, createdDttm, updatedDttm, isActive, isDeleted) VALUES (NULL, '${req.body.name}', '${req.body.createdBy}', '${req.body.createdBy}', '${epoch}', '${epoch}', '1', '0');`;
  db.query(query, (err, rows, fields) => {
      if (err) {
          console.log(err);
      }
      else {
          res.send("ok");
      }
  });
};

//update brand
const updateBrand = (req, res) => {
  res.statusCode = 200;
  const epoch = Math.floor(new Date()/ 1000.0);
  const query = `UPDATE brands SET name = '${req.body.name}', updatedBy = '${req.body.createdBy}', updatedDttm = '${epoch}', isActive = '${req.body.status}' WHERE id = ${req.body.id} and isDeleted = 0`;
  db.query(query,(err, rows, fields)=>{
      if(err)
      {
          console.log(err);
      }
      else{
          res.send("ok");
      }
  })
};

// //for maxid of brands
// const getId = (req, res) => {
//   const obj = {
//     id: "",
//     users: {},
//   };

//   const query1 = "select max(id) as id from brands";
//   const query2 =
//     "select id,name from users where (roleId = 1 or roleId = 2) and isActive = 1 and isDeleted = 0";

//   db.query(query1, (err1, rows1, fields1) => {
//     if (err1) {
//       console.log(err1);
//     } else {
//       db.query(query2, (err2, rows2, fields2) => {
//         if (err2) {
//           console.log(err2);
//         }
//         obj.id = rows1[0];
//         obj.users = rows2;

//         res.send(obj);
//       });
//     }
//   });
// };

// deleting brand
const deleteBrand = (req, res) => {
  res.statusCode = 200;

    const query = `UPDATE brands set isDeleted = 1 where id = ${req.query.id}`;

    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(e);
        }
        else {
            res.send(
                { textMsg: "Record deleted" }
            );
        }
        })
}

module.exports = { brands, addBrand, updateBrand, deleteBrand };
