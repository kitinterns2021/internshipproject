const db = require("../Config/db");

const getProduct = (req, res) => {
    // console.log(req.query)

    let query = ``;

    if(req.query.specId)
    {
        // query = `select products.name, productspecification.specKey, productspecification.value, productspecification.isActive from productspecification JOIN products ON prouctspecification.productId = products.id where productspecification.id = ${req.query.specId} and productspecification.productId = ${req.query.productId} and productspecification.isDeleted = 0`;
        query = `select products.name, productspecification.specKey, productspecification.value, productspecification.isActive from products JOIN productspecification ON products.id = productspecification.productId where productspecification.id = ${req.query.specId} and productspecification.productId = ${req.query.productId} and productspecification.isDeleted = 0`;
    }
    else{
     query = `select name from products where isActive = 1 and isDeleted = 0 and id = ${req.query.productId}`;
    }
    db.query(query, (err, rows, fields)=>{
        if(err)
        {
            console.log(err);
        }
        else{
            res.send(rows[0]);
        }
    })
}

const getSpecification = (req,res) =>{
    // const query = `SELECT productspecification.id as id, products.name AS productname, productspecification.specKey as speckey, productspecification.value as value, productspecification.createdBy as createdBy, productspecification.createdDttm as CreatedDttm, productspecification.isActive as isActive FROM productspecification JOIN products on products.id= productspecification.productId WHERE productspecification.isDeleted=0 and products.id = ${req.query.productId}`;
    const query = `select name from products where id = ${req.query.productId};SELECT productspecification.id as id, productspecification.specKey as specKey, productspecification.value as value, users.name as createdBy, FROM_UNIXTIME(productspecification.createdDttm, '%d-%m-%Y') as createdDttm, FROM_UNIXTIME(productspecification.updatedDttm, '%d-%m-%Y') as updatedDttm, productspecification.isActive as status FROM productspecification JOIN products on products.id= productspecification.productId JOIN users ON productspecification.createdBy = users.id WHERE productspecification.isDeleted=0 and products.id = ${req.query.productId}`;
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        else{
            res.status(200).send({
                productName: rows[0][0].name,
                allSpecifications: [...rows[1]]
            })
        }
    })
}

const addSpecification = (req,res)=>{
    // console.log(req.body);
    //  const query = `INSERT INTO productspecification(id, productId, key, value, createdBy, updatedBy, createdDttm, updatedDttm, isActive, isDeleted) VALUES (NULL, '${req.body.productname}','${req.body.key}','${req.body.value}''1','1','1', '1','1','0')`;
    const epoch = Math.floor(new Date().getTime() / 1000.0)
    const query = `INSERT INTO productspecification (id, productId, specKey, value, createdBy, updatedBy, createdDttm, updatedDttm, isActive, isDeleted) VALUES (NULL, ${req.body.productId}, '${req.body.specKey}', '${req.body.value}', ${req.body.createdBy}, ${req.body.createdBy}, ${epoch}, ${epoch}, 1, 0)`;
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        else {
            res.send({status: true})
        }
    })
}

const deleteSpecification = (req, res) => {
    const query = `update productspecification set updatedBy = ${req.query.userId}, updatedDttm = ${Math.floor(new Date() / 1000.0)}, isActive = 0, isDeleted = 1 where id = ${req.query.specId}`;
    db.query(query, (err, rows, fields) => {
        if(err)
        {
            console.log(err);
        }
        else{
            res.status(200).send({success:true});
        }
    })
}//end deleteSpecification

// updating product specification
const updateSpecification = (req,res) => {
    const body = req.body;
    const query = `update productspecification set specKey = '${body.specKey}', value = '${body.value}', updatedBy = ${body.createdBy}, updatedDttm = ${Math.floor(new Date() / 1000.0)}, isActive = ${body.status} where id = ${body.specId}`;
    db.query(query, (err, rows, fields) => {
        if(err)
        {
            console.log(err);
        }
        else{
            res.status(200).send({
                status: true
            })
        }
    })
}//end updateSpecification

module.exports = {getSpecification, addSpecification, getProduct, deleteSpecification, updateSpecification};