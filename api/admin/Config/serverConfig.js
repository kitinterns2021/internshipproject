const express = require("express");
const cors = require("cors");
const fileUpload = require("express-fileupload");
const path = require("path");



const app = express();

// Defining port for the server to listen
const port = 3010;

// This lets you allow any frontend domain to access data since domains are different
app.use(cors({
    origin: '*'
}));

// using express-fileupload as a middleware to perform operations on files
app.use(fileUpload());

// making images folder publically available in whole application
// app.use('/images',express.static('C:/Software Projects/fathom-main-project/api/images'));

app.use('/images',express.static(path.join(__dirname, '../../images')));


app.use('/images',express.static(path.join(__dirname, '../../images')));



// This method is inbuilt in express to recognize the incoming Request Object as a JSON Object. This method is called as a middleware in the application 
app.use(express.json());

// listning the app on the specified port
app.listen(port, () => {
    console.log("Server runnung on the port " +port);
})

module.exports = app;