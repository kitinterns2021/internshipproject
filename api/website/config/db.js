//connectivity of database

const mysql = require("mysql");

// creating database connection
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'internship_project',
    multipleStatements: true
});

module.exports = connection;