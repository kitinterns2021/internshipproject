const express = require('express');
const path = require("path");
const app = express();

//cross origin resource sharing 
const cors = require('cors')  

//Defining port for server to listen
const port = 3020;

// This lets you allow any frontend domain to access data since domains are different
app.use(cors({
    origin: '*'
}));


// making images statically available in the website
app.use('/images',express.static(path.join(__dirname, '../../images')));

// This method is inbuilt in express to recognize the incoming Request Object as a JSON Object. This method is called as a middleware in the application 
app.use(express.json());

//listening the app on the port
app.listen(port, () =>{
    console.log("Server listening on the port" +port);
} )

module.exports = app;
