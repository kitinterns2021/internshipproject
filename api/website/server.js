const app = require('./config/config.js');


// importing api controllers
const userInfo = require("./websiteController/userInfo");
const loginSecurity = require("./websiteController/loginSecurity");
const addresses = require("./websiteController/addresses");
const signup = require("./websiteController/signup");
const cart = require("./websiteController/cart");
const payment = require("./websiteController/payment");
const productSearch = require("./websiteController/productSearch");
const purchaseHistory = require("./websiteController/purchaseHistory");
// const sendMail = require("./emailControllers/sendMail.js");


// User Info end points
//for user information
app.get("/user-info", userInfo.userInformation);

//for updating user personal information
app.put("/user-info/update-user-info", userInfo.updateUser);



//login security end points
//for login info
app.get("/login-info", loginSecurity.loginInfo);

//for change password
app.post("/change-password", loginSecurity.changePassword);

//for update password
app.put("/update-password", loginSecurity.updatePassword);



// Addresses end points
//for getting address
app.get('/address-table', addresses.getAddress);

//for inserting address
app.post('/address-table/add-address', addresses.insertAddress);

//for showing address
app.get('/get-address', addresses.showAddress);

//for deleting address
app.delete('/address-table/delete-address', addresses.deleteAddress);

//for updating address
app.put('/address-table/update-address', addresses.updateAddress);



app.post('/signup', signup.signup);

app.get('/signin', signup.signin);



// getting products from cart
app.get("/product-item",cart.getProduct);



// handling payments

// getting all required data for payment
app.get("/payment/get-data", payment.getData);

// updating/entering user contact
app.put("/payment/add-contact",payment.addContact);

// handling order payment
app.post("/payment/make-payment",payment.makePayment);

// cancelling the order
app.put("/payment/cancel-order", payment.cancelOrder);




// searching for products

// getting single product data
app.get("/single-product",productSearch.getSingleProduct);

// getting data in dashboard
app.get("/dashboard",productSearch.getDashboardData);

// getting data for navbar
app.get('/dashboard/nav-data',productSearch.navbarData)

// searching products for category
app.get("/productSearch/category",productSearch.getCategoryData);



//purchaseHistory end points
app.get("/purchaseHistory/orders",purchaseHistory.getOrderHistory);

// getting perticular order item from the orders
app.get("/purchaseHistory/order-item",purchaseHistory.getOrderItemHistory);
// sending email
// app.post("/sendMail", async(req,res) =>{
//     //  const resp = await sendMail(req.body);
//     //  const result = await resp;
//     //  res.send(sendMail(req.body).);
//     const result = await sendMail(req.body);
//     res.send(result);
// });
