const db = require("../Config/db");

const getAddress = (req, res) => {
    const query = `SELECT * FROM customeraddress WHERE isActive = 1 and isDeleted = 0 and userId=${req.query.userId}`;
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        else {
            res.send(rows);
        }
    })
}

const insertAddress = (req,res) =>{
    const currentDate = Math.floor(new Date() / 1000.0);
    const query = `INSERT INTO customeraddress (id, userId,addressLine1, addressLine2, city, state, pincode, createdBy, updatedBy, createdDttm, updatedDttm, isActive, isDeleted) VALUES (NULL, '${req.body.userId}', '${req.body.addressLine1}', '${req.body.addressLine2}', '${req.body.city}', '${req.body.state}', ${req.body.pincode}, '${req.body.userId}', '${req.body.userId}', ${currentDate}, ${currentDate}, '1', '0'); `
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        else {
            res.send("Record Inserted Successfully");
        }
    })
}

const showAddress = (req,res) =>{
    const query = `SELECT * FROM customeraddress WHERE id=${req.query.id} and isActive = 1 and isDeleted = 0 `;
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        else {
            res.send(rows);
        }
    })
}

const deleteAddress = (req,res) =>{
    res.statusCode = 200;
    const query = `UPDATE customeraddress set isDeleted = 1, isActive = 0 where id = ${req.query.id}`;

    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(e);
        }
        else {
            res.send(
                { textMsg: "Record deleted" }
            );
        }
    })
}

const updateAddress = (req,res) =>{
    res.statusCode = 200;
    const currentDate = Math.floor(new Date() / 1000.0);
    const query = `UPDATE customeraddress SET addressLine1 = '${req.body.addressLine1}', addressLine2 = '${req.body.addressLine2}', city = '${req.body.city}', state = '${req.body.state}', pincode = ${req.body.pincode}, updatedDttm = ${currentDate} WHERE id = ${req.body.id} and isDeleted = 0 and isActive = 1 `;
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        else {
            res.send("ok");
        }
    })
}

module.exports = {getAddress, insertAddress, deleteAddress, showAddress, updateAddress};