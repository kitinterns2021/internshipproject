const db = require("../config/db");
// const sendMail = require("../emailControllers/sendMail");
// const paymentSuccess = require("../emailControllers/paymentSuccess");

// getting email controllers from email direcotry
const sendMail = require("../../email/emailControllers/sendMail");
const paymentSuccess = require("../../email/emailControllers/payment");


// getting all required data
const getData = (req, res) => {

    // getting products from query
    const products = JSON.parse(req.query.products);

    // getting user id from query
    const userId = req.query.userId;

    //initializing object
    let result = {
        products: [],
        address: { contact: 0 },
        grandTotal: 0
    }

    // getting all products
    for (let key in products) {
        let query = `select products.id, brands.name as brandName, products.name as productName, tagtable.name as tag, products.price, (products.price + (products.price * 0.12)) as deletedPrice, (products.price * ${products[key]}) as total from products JOIN brands on products.brandId = brands.id JOIN tagtable ON products.tag = tagtable.id where products.id = ${key} and products.isActive = 1 and products.isDeleted = 0;select imageURL from imagetable where productId = ${key} and isActive = 1 and isDeleted = 0 LIMIT 1`;
        db.query(query, (err, rows, fields) => {
            if (err) {
                console.log(err);
            }
            else {
                // pushing product details in array
                result.products.push({ ...rows[0][0], qty: products[key], img: rows[1][0].imageURL });

                // adding total amount of product in grand total
                result.grandTotal += rows[0][0].total;

            }//end if

        });//end db callback function
    }//end for loop

    // checking if the grandtoal is less than 1000 or not
    // result.grandTotal <= 1000 ? result.grandTotal += 50 : result.grandTotal;

    // getting user address
    const query = `select addressLine1, addressLine2, city, state, pincode, users.name, users.contact, users.emailId from customeraddress JOIN users on customeraddress.userId = users.id where customeraddress.userId = ${userId} and customeraddress.isActive = 1 and customeraddress.isDeleted = 0 LIMIT 1;`;
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        else {
            result["address"] = { ...result.address, ...rows[0] };
        }//end if
    })//end db callback function

    // sending responst after 500 milliseconds. This process is synchronous and takes some time. SO we have to wait till the process gets completed.
    // ==> If any logical problems happen to get proper data, just try to increase the response time of the setTimeout function
    setTimeout(() => { res.send(result) }, 500);//end setTimeout



}//end getData

// handling payment and order
const makePayment = async (req, res) => {
    const data = req.body;
    // console.log(data);
    const epoch = Math.floor(new Date().getTime() / 1000);

    // getting values from data object
    const products = data.products;
    const userId = data.userId;
    const address = data.address;
    // console.log(products);


    // inserting data into orders table
    const query = `INSERT INTO orders (id, userId, contact, price, addressLine1, addressLine2, city, state, pincode, orderStatus, createdBy, updatedBy, createdDttm, updatedDttm, isActive, isDeleted) VALUES (NULL, '${userId}', '${address.contact}', '${parseInt(data.grandTotal) + 50}', '${address.addressLine1}', '${address.addressLine2}', '${address.city}', '${address.state}', '${address.pincode}', 'BOK', '${userId}', '${userId}', '${epoch}', '${epoch}', '1', '0');`
    db.query(query, async (err, rows, fields) => {
        if (err) {
            console.log(err);
            db.rollback(() => {
                res.send({ err: err.toString() })
            })
        }
        else {
            const orderId = rows.insertId; //==> Id of the currently inserted record

            // inserting data in order items table
            products.forEach(item => {
                const query1 = `INSERT INTO orderitems (id,orderId, productId, productPrise, quantity, createdBy, updatedBy, createdDttm, updatedDttm, isActive, isDeleted) VALUES (NULL, '${orderId}', '${item.id}', '${item.price}', '${item.qty}', '${userId}', '${userId}', '${epoch}', '${epoch}', '1', '0');`;
                db.query(query1, (err1, rows1, fields1) => {

                    if (err1) {
                        console.log(err1)
                        db.rollback(() => {
                            res.send({ err: err.toString() })
                        })
                    }

                })//end db callback function

            });//end foreach
        }//end if


        // sending response
        // res.send({ success: true })

        
        // sending email
        const result = await mail({
            to: address.emailId,
            name: address.name,
            totalPrice: (parseInt(data.grandTotal) - 50) <= 1000 ? (parseInt(data.grandTotal) - 50) : data.grandTotal, // ==> if grand total is less than or equal to Rs 1000, then it coming with extra 50rs as delivery charges.
            deliveryCharges: (parseInt(data.grandTotal) - 50) <= 1000 ? "50" : "0",
            grandTotal: data.grandTotal,
            orderItems: [...products],
            address: address,
            subject: "Order Placed Successfully!",
            title:"Order Placed Successfully!",
            message:"your order has been placed successfully."
        })

        if (result === "OK") {
            res.send({ success: true })
        }
        else {
            res.send({ success: false })
        }

    })//end callback function of db

}//end makePayment

// adding user contact number
const addContact = (req, res) => {
    const body = req.body;

    const query = `update users set contact = '${body.contact}' where id = ${body.userId} and isActive = 1 and isDeleted = 0 and roleId = 3`;
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
            res.send({ err: err.toString() });
        }
        else {
            res.send({ success: true });
        }
    })//end db callback function
}//end addContact

// cancelling the order
const cancelOrder = (req, res) =>{
    const body = req.body;
    // console.log(body)

    const query =  `update orderitems, orders set orderitems.isActive = 0, orderitems.updatedBy = ${body.userId}, orderitems.updatedDttm = ${Math.floor(new Date() / 1000)}, orders.orderStatus='CNL' where orderitems.id = ${body.orderItemId} and orders.id = (select orderitems.orderId from orderitems where orderitems.id = ${body.orderItemId})`;
    db.query(query, async (err, rows, fields) => {
        if(err)
        {
            console.log(err);
            res.status(500).send({err:err.toString()});
        }
        else{

            // sending mail to the associated user
            const result = await mail({
                to: [body.emailId],
                orderItems: [{...body,img:body.imageURL,total:(body.price / body.qty)}],
                subject: "Order Cancelled!",
                name: body.userName,
                totalPrice: (parseInt(body.price) - 50) <= 1000 ? (parseInt(body.price) - 50) : body.price,
                deliveryCharges: (parseInt(body.price) - 50) <= 1000 ? "50" : "0",
                grandTotal: body.price,
                address:{addressLine1:body.addressLine1, addressLine2:body.addressLine2, city:body.city,state:body.state,pincode:body.pincode},
                title:"Order has been cancelled!",
                message: "Your order has been cancelled."
            });

            if(result === "OK"){
                res.status(200).send({success:true});
            }
            else{
                res.status(500).send({success:false});
            }

        }
    })//end db callback function
}//end cancelOrder

// trying to send email
const mail = async (data) => {

    const attachments = [];
    data.orderItems.map(element => {
        return attachments.push({
            filename: element.img,
            path: `http://localhost:3020/images/${element.img}`,
            cid: element.img
        })
    })

    const html = await paymentSuccess(data);

    // creating options object
    const emailOptions = {
        from: "Fathom Services <fathomable@gmail.com>",
        to: data.to,
        subject: data.subject,
        html: html,
        attachments: attachments
    }

    const result = await sendMail(emailOptions);
    // console.log(result);
    if (result.accepted.length !== 0) {
        return "OK"
    }
}


// exporting all API controller functions as an object
module.exports = { getData, makePayment, addContact, cancelOrder };