const db = require("../config/db");

// setting function for repeated query
const productQuery = (limit) => {
    return `select products.id, brands.name as brandName, products.name as productName, tagtable.name as tag, products.price, (products.price + (products.price * 0.12)) as deletedPrice, imagetable.imageURL from products JOIN brands on products.brandId = brands.id JOIN tagtable ON products.tag = tagtable.id JOIN imagetable ON products.id = imagetable.productId where RAND() and imagetable.id = (select id from imagetable where isActive = 1 and isDeleted = 0 and productId = products.id limit 1) and products.isActive = 1 and products.isDeleted = 0 and products.currentStock > 0 ORDER BY products.name LIMIT ${limit};`;
}//end productQuery

// getting single product record
const getSingleProduct = (req,res) =>{

    const id = req.query.id;

    const query = `select category.name as category, subCategory.name as subcategory, brands.name as brand, products.name, products.model, products.price,(price + ((price/100)*12)) as delPrice, products.productDescription as description, tagtable.name as tag from products join category on products.categoryId = category.id join subCategory on products.subCategoryId = subCategory.id join brands on products.brandId = brands.id JOIN tagtable on products.tag = tagtable.id where products.isDeleted = 0 and products.isActive = 1 and products.currentStock > 0 and products.id = ${id};select specKey, value from productspecification where productId=${id} and isDeleted=0 and isActive =1 order by id;select imageURL from imagetable where isActive = 1 and isDeleted= 0 and productId=${id} order by id;${productQuery(15)}`;
    db.query(query, (err, rows, fields)=>{
        if(err)
        {
            res.send({err: err.toStrint()});
        }
        else{
            res.send({
                data: rows[0][0],
                specifications: rows[1],
                images: rows[2],
                relatedProducts: rows[3]
            });
        }
    })

}//end getSingleProduct

// getting all necessary data for frontend
const getDashboardData = (req,res) => {

    res.status = 200;

    // setting query
    const query =  `select * from category where isActive = 1 and isDeleted = 0 ORDER BY id LIMIT 6;${productQuery(20)}`;

    db.query(query, (err, rows, fields)=>{
        if(err)
        {
            console.log(err);
            res.send({err:err.toString()});
        }
        else{
            res.send({
                category: rows[0],
                products: rows[1]
            })
        }
    })

}//end getDashboardData

// getting data for categories
const getCategoryData = (req, res) => {

    let query = "";

    if(req.query.categoryId)
    {
        query = `select  products.id, brands.name as brandName, products.name as productName, tagtable.name as tag, products.price, (products.price + (products.price * 0.12)) as deletedPrice, imagetable.imageURL from products JOIN brands on products.brandId = brands.id JOIN tagtable ON products.tag = tagtable.id JOIN imagetable ON products.id = imagetable.productId JOIN category ON products.categoryId = category.id where imagetable.id = (select id from imagetable where isActive = 1 and isDeleted = 0 and productId = products.id limit 1) and products.isActive = 1 and products.isDeleted = 0 and category.id =${req.query.categoryId} ORDER BY products.name;select count(products.id) as totalRecords, category.name from products JOIN category ON products.categoryId = category.id where products.isActive = 1 and products.isDeleted = 0 and category.id = ${req.query.categoryId};${productQuery(10)}`;
    }
    else if(req.query.subCategoryId)
    {
        query = `select  products.id, brands.name as brandName, products.name as productName, tagtable.name as tag, products.price, (products.price + (products.price * 0.12)) as deletedPrice, imagetable.imageURL from products JOIN brands on products.brandId = brands.id JOIN tagtable ON products.tag = tagtable.id JOIN imagetable ON products.id = imagetable.productId JOIN category ON products.categoryId = category.id JOIN subcategory ON products.subCategoryId = subcategory.id where imagetable.id = (select id from imagetable where isActive = 1 and isDeleted = 0 and productId = products.id limit 1) and products.isActive = 1 and products.isDeleted = 0 and subcategory.id =${req.query.subCategoryId} ORDER BY products.name;select count(products.id) as totalRecords, subcategory.name from products JOIN subcategory ON products.subCategoryId = subcategory.id where products.isActive = 1 and products.isDeleted = 0 and subcategory.id = ${req.query.subCategoryId};${productQuery(10)}`;
    }

    db.query(query,(err, rows, fields) => {
        if(err)
        {
            console.log(err);
            res.send({
                err: err.toString()
            });
        }
        else{
            res.status(200).send({
                products: rows[0],
                category: rows[1][0],
                relatedProducts: rows[2]
            })
        }
    })//end db callback function
}//end getCategoryData


// getting categories and sub categories for navbar
const navbarData = (req, res) => {

    const query = `
        select id, name from category where isActive = 1 and isDeleted = 0;
        select id, name from subcategory where isActive = 1 and isDeleted = 0;
    `;

    db.query(query, (err, rows, fields)=>{
        if(err)
        {
            console.log(err);
        }else{
            // console.log(rows);
            res.status(200).send({
                categories: [...rows[0]],
                subCategories: [...rows[1]],
            })
        }
    })//end db callback function

}//end navBarData

// exporting API controller function as a object
module.exports = { getDashboardData, getCategoryData, getSingleProduct, navbarData };