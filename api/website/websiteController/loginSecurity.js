const db = require("../Config/db");

const loginInfo = (req, res) => {
    const query = `SELECT emailId from users where id = ${req.query.id} and roleId = 3 and isDeleted = 0 and isActive = 1`;
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        res.send(rows);
    })
}

const changePassword = (req, res) => {
    // console.log(req.body.emailId + "\n" + req.body.password)
    const query = `SELECT emailId, password from users WHERE emailId = "${req.body.emailId}" and password = "${req.body.password}" and roleId = 3 and isActive = 1 and isDeleted = 0`;
    // const query = `SELECT * from users where emailId = "${req.query.emailId}" and password = "${req.query.password}" and roleId = 3 and isDeleted = 0 and isActive = 1`;
    // const query = `SELECT * FROM users WHERE isActive = 1 and isDeleted = 0 and emailId = "${req.query.emailId}" and roleId = 3`;
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        else if (rows.length === 0) {
            res.send(false);
        }
        else {
            res.send({res:true});
        }
    })
}



const updatePassword = (req,res) =>{
    const query = `UPDATE users SET password = "${req.body.password}" WHERE id = ${req.body.userId} and isDeleted = 0 and isActive = 1`;
    db.query(query, (err,rows,fields)=>{
        if(err){
            console.log(err);
        }
        else{
            res.send("ok");
        }
    })
}

module.exports = { loginInfo, changePassword, updatePassword };