const db = require("../config/db");

const getProduct = (req,res) => {

    const productId = req.query.productId;

    const query = `
    select products.id, products.name, products.price, imagetable.imageURL from products JOIN imagetable ON products.id = imagetable.productId where products.id = ${productId} and imagetable.productId = ${productId} and products.isDeleted = 0 and imagetable.isDeleted = 0 LIMIT 1`;
    db.query(query, (err, rows, fields) => {
        if(err)
        {
            res.send({err:err.toString()});
        }
        else{
            res.send(rows);
        }
    })
}//end getProduct


// exporting API controller functions as object
module.exports = { getProduct };