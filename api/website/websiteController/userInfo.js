const db = require("../Config/db");

const userInformation = (req, res) => {
    const query = `SELECT *, FROM_UNIXTIME(dob,"%d-%m-%y") as dob FROM users WHERE isActive = 1 and isDeleted = 0 and id = ${req.query.userId} and roleId = 3`;
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        res.send(rows);
    })
}

const updateUser = (req, res) => {
    res.statusCode = 200;
    const currentDate = Math.floor(new Date() / 1000.0);
    const query = `UPDATE users SET name='${req.body.name}', contact=${req.body.contact}, gender='${req.body.gender}', updatedDttm = ${currentDate}, dob = ${Math.floor(new Date(req.body.dob) / 1000.0)} WHERE id = ${req.body.userId} and isDeleted = 0 and isActive = 1`;
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        else {
            res.send("ok");
        }
    })
}

module.exports = {userInformation, updateUser};
