const db = require("../config/db");

const getOrderHistory = (req, res) => {
    const query = `SELECT orders.id as orderId, orderitems.id as orderItemId, orders.orderStatus as status, orders.city, tagtable.name as tagName, brands.name as brandName, products.name as productname, imagetable.imageURL from orders JOIN users ON orders.userId = users.id JOIN orderitems ON orders.id = orderitems.orderId JOIN products ON products.id = orderitems.productId JOIN tagtable on products.tag = tagtable.id JOIN brands on brands.id = products.brandId JOIN imagetable ON imagetable.productId = products.id WHERE users.id = ${req.query.userId} and  imagetable.id = (SELECT id from imagetable where imagetable.productId = products.id and imagetable.isActive = 1 LIMIT 1) ORDER BY orderitems.id desc`;
    db.query(query, (err, rows, fields) => {
        if (err) {
            console.log(err);
        }
        res.status(200).send(rows);
    })
};

// getting order item of perticular order
const getOrderItemHistory = (req,res) =>{

    const query = `SELECT products.id as productId, products.name as productName, orderitems.id as orderItemId, (orderitems.productPrise * orderitems.quantity) as price, ((orderitems.productPrise * orderitems.quantity) + (orderitems.productPrise * orderitems.quantity * 0.12)) as deletedPrice, ((orderitems.productPrise * orderitems.quantity) + (orderitems.productPrise * orderitems.quantity * 0.12) - (orderitems.productPrise * orderitems.quantity )) as youSaved, users.name as userName, users.emailId, orders.contact, brands.name as brandName, imagetable.imageURL, orderitems.quantity as qty, orders.orderStatus as status, FROM_UNIXTIME( orders.updatedDttm, '%D %M %Y' ) as updatedDate, orders.addressLine1, orders.addressLine2, orders.city, orders.state, orders.pincode FROM orders JOIN orderitems ON orderitems.orderId = orders.id JOIN products ON orderitems.productId = products.id JOIN users ON orders.userId = users.id JOIN brands ON products.brandId = brands.id JOIN imagetable ON imagetable.productId = products.id WHERE imagetable.id = (SELECT id FROM imagetable WHERE imagetable.productId = products.id and imagetable.isActive = 1 LIMIT 1)  and users.id = ${req.query.userId} and orderitems.id = ${req.query.orderItemId}`;
    db.query(query, (err, rows, fields) =>{
        if(err)
        {
            console.log(err);
            // req.status(500).send({err:err.toString()});
        }
        else{
            res.status(200).send(rows[0]);
        }
    })//end db callback function
}//end getOrderItemHistory

module.exports = {getOrderHistory, getOrderItemHistory};