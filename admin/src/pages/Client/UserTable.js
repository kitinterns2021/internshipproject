import React, { useState, useEffect } from 'react'
import { Link } from "react-router-dom";
import { Tag, Table, Divider, Typography } from "antd";
import "antd/dist/antd.css";
// import TableButton from "../../components/TableButton/TableButton";
import url from "../../components/httpRequest/http";
import "./UserTable.css";

const { Title } = Typography;

export default function UserTable() {
    // defining state for data
    const [data, setData] = useState(false);

    // getting all users data from backend
    const getData = async () => {
        const response = await fetch(url + "/all-users");
        const rows = await response.json();
        setData(rows);
    }//end getData()

    useEffect(() => {
        getData();

        return () => {
            setData(false);
        }
    }, [])

    //Defining all required columns of the table
    const columns = [
        // {
        //     title: "ID",
        //     dataIndex: "id",
        //     key: "id",
        //     render: id => <Link to={`/users/user-details?id=${id}`}>
        //         {id}
        //     </Link>

        // },
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: "Contact",
            dataIndex: "contact",
            key: "contact",
        },
        {
            title: 'Email',
            dataIndex: 'emailId',
            key: 'emailId',
        },
        // {
        //     title: 'Gender',
        //     dataIndex: 'gender',
        //     key: 'gender',
        //     render: gender => {
        //         if (gender === "M") {
        //             return "Male";
        //         }
        //         else if (gender === "F") {
        //             return "Female";
        //         }
        //         else {
        //             return "Others"
        //         }
        //     }
        // },
        // {
        //     title: 'Birth Date',
        //     dataIndex: 'dob',
        //     key: 'dob',
        // },
        {
            title: 'Status',
            key: 'isActive',
            dataIndex: 'isActive',
            render: Status => {
                let color = "";
                switch (Status) {
                    case 1:
                        Status = "ACTIVE"
                        color = "green";
                        break;
                    case 0:
                        Status = "INACTIVE";
                        color = "volcano";
                        break;
                    default:
                        break;
                }
                return (
                    <Tag color={color} key={Status}>
                        {Status}
                    </Tag>
                );
            }
        },
        // {
        //     title: 'Verified',
        //     key: 'isVerified',
        //     dataIndex: 'isVerified',
        //     render: verify => {
        //         let color = "";
        //         switch (verify) {
        //             case 1:
        //                 verify = "YES"
        //                 color = "green";
        //                 break;
        //             case 0:
        //                 verify = "NO";
        //                 color = "volcano";
        //                 break;
        //             default:
        //                 break;
        //         }
        //         return (
        //             <Tag color={color} key={verify}>
        //                 {verify}
        //             </Tag>
        //         );
        //     }
        // },
        {
            title: "Joining Date",
            key: "createdDttm",
            dataIndex: "createdDttm"
        },
        {
            title: "Last Modified",
            key: "updatedDttm",
            dataIndex: "updatedDttm"
        },
        // {
        //     title: "Actions",
        //     key: "key",
        //     dataIndex: "action",
        //     render: (text, record) => {
        //         return <>

        //             {
        //                 record.orderStatus === "BOK" &&
        //                 /* Dispatch Order */
        //                 < TableButton
        //                     id={record.id}
        //                     title="Do you want to dispatch this order?"
        //                     onConfirm={"confirmHandler"}
        //                     onCancel={"cancel"}
        //                     type="primary"
        //                     buttonText="Dispatch"
        //                     status="DIS"

        //                 />
        //             }

        //             {
        //                 record.orderStatus === "DIS" &&
        //                 /* Deliver Order  */
        //                 < TableButton
        //                     id={record.id}
        //                     title="Is this order delivered to the customer?"
        //                     onConfirm={"confirmHandler"}
        //                     onCancel={"cancel"}
        //                     type="primary"
        //                     buttonText="Delivered?"
        //                     status="DLI"

        //                 />
        //             }

        //             {
        //                 (record.orderStatus === "BOK" || record.orderStatus === "DIS") &&
        //                 /* Cancel Order  */
        //                 < TableButton
        //                     id={record.id}
        //                     title="Are you sure to delete this order?"
        //                     onConfirm={"confirmHandler"}
        //                     onCancel={"cancel"}
        //                     type="danger"
        //                     buttonText="Cancel"
        //                     status="CNL"

        //                 />
        //             }

        //             {
        //                 (record.orderStatus === "CNL" || record.orderStatus === "DLI") &&
        //                 /* Delete Order  */
        //                 < TableButton
        //                     id={record.id}
        //                     title="Are you sure you want to delete this order from the records?"
        //                     onConfirm={"deleteHandler"}
        //                     onCancel={"cancel"}
        //                     type="danger"
        //                     buttonText="Delete?"
        //                 />
        //             }
        //         </>
        //     }
        // }
    ];

    return (
        <div className='container4'>
            {data &&
                <>
                    <Title >All Users</Title>
                    <Divider />
                    <p>Showing 1-5 out of 100 results</p>
                    <Table columns={columns} dataSource={data} />
                </>
            }
        </div>
    )
}
