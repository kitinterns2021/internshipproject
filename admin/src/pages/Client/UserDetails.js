import React from 'react'
import { Row } from "antd";
import "antd/dist/antd.css";
import "./UserDetails.css";
import { useHistory, useLocation } from 'react-router-dom'
import AccountCard from './AccountCard/AccountCard';
import { CreditCardFilled, ShoppingFilled, IdcardFilled, LockFilled, EnvironmentFilled } from "@ant-design/icons";

export default function UserDetails() {
    // getting history object
    const history = useHistory();

    // getting location object
    const location = useLocation();

    // getting query parameters from location object
    const searchParams = new URLSearchParams(location.search);
    return (
        <>
            <div className="site-card-wrapper">
                <Row gutter={[16, 50]}>
                    <AccountCard
                        icon={<ShoppingFilled style={{ fontSize: "50px", width: "50px", color: "#bae637" }} />}
                        title="Purchase History"
                        content="Return or buy things again"
                        link="#"
                    />

                    <AccountCard
                        icon={<IdcardFilled style={{ fontSize: "50px", width: "50px", color: "#b37feb" }} />}
                        title="Manage Account"
                        content="Edit personal profile"
                        link="/account/personal-profile"
                    />

                    <AccountCard
                        icon={<LockFilled style={{ fontSize: "50px", width: "50px", color: "gray" }} />}
                        title="Login and Security"
                        content="Edit mail and password"
                        link="/account/login-security"
                    />
                </Row>
            </div>
            <div className="site-card-wrapper">
                <Row gutter={[16, 50]}>
                    <AccountCard
                        icon={<EnvironmentFilled style={{ fontSize: "50px", width: "50px", color: "orange" }} />}
                        title="Addresses"
                        content="Edit addresses for orders and gifts"
                        link="/account/your-addresses"
                    />

                    <AccountCard
                        icon={<CreditCardFilled style={{ fontSize: "50px", width: "50px", color: "#40a9ff" }} />}
                        title="Payment Options"
                        content="Edit or add payment options"
                        link="#"
                    />
                </Row>
            </div>
        </>
    )
}
