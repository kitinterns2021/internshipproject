import React,{ useState } from "react";
import { useHistory } from 'react-router-dom';
import {
  Card,
  Divider,
  Typography,
  Row,
  Col,
  Button,
  Popconfirm,
  message,
} from "antd";
import "./Account.css";
import Signin from "../Login/Signin"
import {
  CreditCardFilled,
  SolutionOutlined,
  LockFilled,
  TeamOutlined,
  IdcardFilled,
  UserOutlined,
  LogoutOutlined,
} from "@ant-design/icons";
import AccountCard from "../../components/AccountCard/AccountCard";

const { Title } = Typography;

  
  
export default function Account(props) {
   const [loggedIn, setLoggedIn] = useState(() => {
    // if local storage has a state of isLoggedIn = true, this function will return true otherwise false
    const state = localStorage.getItem("isLoggedIn");
    if (state === "true") {
      return true;
    } else {
      return false;
    }
  });


  const logoutConfirmHandler = (e) => {

    if (localStorage.getItem("loggedIn") || localStorage.getItem("userId")) {
        // removing isLoggedIn state from local storage
        localStorage.removeItem("loggedIn");
  
        // removing userId from local storage
        localStorage.removeItem("userId");
    }
  
    // setting setLoggedIn state to false
    setLoggedIn(false);

    message.success("Sign out successfully!");
  }
  
  return (
    <>
       {loggedIn ? (
      <Card style={{ width: "100%" }}>
        <Row loginStatus={setLoggedIn} >
          <Col>
            <UserOutlined
              style={{
                marginLeft: "2rem",
                fontSize: "80px",
                width: "80px",
                color: "#780650",
              }}
            />
          </Col>
          <Col style={{ marginLeft: "2rem" }}>
            <Title level={2}>Hi Javed !</Title>
          </Col>
        </Row>
        <Divider />
        <div className="site-card-wrapper">
          <Row gutter={[16, 50]}>
            <AccountCard
              icon={
                <TeamOutlined
                  style={{ fontSize: "50px", width: "50px", color: "#bae637" }}
                />
              }
              title="ADMIN INFO"
              content=""
              link="/account/admin"
            />

            <AccountCard
              icon={
                <IdcardFilled
                  style={{ fontSize: "50px", width: "50px", color: "#b37feb" }}
                />
              }
              title="Manage Personal Account"
              content=""
              link="/account/spersonalinfo"
            />

            <AccountCard
              icon={
                <LockFilled
                  style={{ fontSize: "50px", width: "50px", color: "gray" }}
                />
              }
              title="Change Password"
              content=""
              link="/account/security"
            />
          </Row>
        </div>
        <div className="site-card-wrapper">
          <Row gutter={[16, 50]}>
            <AccountCard
              icon={
                <SolutionOutlined
                  style={{ fontSize: "50px", width: "50px", color: "orange" }}
                />
              }
              title="Admin report"
              content=""
              link="/account/adminreport"
            />

            <AccountCard
              icon={
                <CreditCardFilled
                  style={{ fontSize: "50px", width: "50px", color: "#40a9ff" }}
                />
              }
              title="Feedback Report"
              content=""
              link="/account/feedbacktable"
            />
          </Row>
        
            <Popconfirm
              placement="topRight"
              title="Do you want to Log Out from the account?"
              onConfirm={logoutConfirmHandler}
              onCancel={() => message.error("Log out aborted!")}
              okText="Yes"
              cancelText="No"
            >
              <Button
                style={{
                  Size: "50px",
                  width: "50p x",
                  color: "black",
                  height: "80px",
                  borderBlockColor: "black",
                  marginTop: "100px",
                  marginLeft: "65rem" 
                }}
              >
                <LogoutOutlined
                  style={{ fontSize: "50px", width: "50px", color: "black" }}
                />
                Logout
              </Button>
            </Popconfirm>
         
        </div>
      </Card>
       ) : (
        <Signin loginStatus={setLoggedIn} />
      )}
    </>
  );
}
