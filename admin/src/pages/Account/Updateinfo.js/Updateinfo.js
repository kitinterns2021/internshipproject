import React, { useState, useEffect } from "react";
import {
  Form,
  Button,
  Radio,
  Card,
  Select,
  Typography,
  Row,
  Col,
  Divider,
  notification,
  Spin,
} from "antd";
import { Content } from "antd/lib/layout/layout";
import "antd/dist/antd.css";
import "./Updateinfo.css"
import moment from "moment";

import { useHistory, useLocation } from "react-router-dom";
import InputField from "../../../components/InputField/InputField";
import url from "../../../components/httpRequest/http";
import EmailComponent from "../../../components/EmailComponent/EmailComponent";
import Password from "../../../components/Password/Password";
const { Title } = Typography;
const dateFormat = "YYYY/MM/DD";
// const customFormat = value => `custom format: ${value.format(dateFormat)}`;

export default function Updateinfo() {
  const [form] = Form.useForm();

  // setting state for the data
  //   const [data, setData] = useState({
  //     id: "",
  //     name: "",
  //     role: "",
  //     emailId:"",
  //     password: "",
  //     createdBy: localStorage.getItem("userId"),
  //     createdDttm: date,
  //     status: "",
  //     userName: "",
  //   });

  //   // getting history object
  const history = useHistory();

  const [data, setData] = useState(false);

  const getData = async () => {
      const response = await fetch(url + `/account/spersonalinfo?userId=${localStorage.getItem("userId")}`)
      const rows = await response.json();
      setData({
          ...data,
          userId: localStorage.getItem("userId") ,
          name: rows[0].name,
          emailID:rows[0].emailId,
          status:rows[0].status,
          
      });
      // console.log(rows[0]);
      form.setFieldsValue({ ...rows[0] });
  }

  // getting location object of the url and getting the query parameters from it
  // const location = useLocation();

  // //searchParams are query parameters
  // const searchParams = new URLSearchParams(location.search);

  useEffect(() => {
      getData();

  }, []
  )

  const onChangeHandler = (name, e) => {
      setData({
          ...data,
          [name]: e.target.value
      })
  }

 
     // notification for confirmation
     const openNotificationWithIcon = (msg) => {
         notification.success({
             message: "Success!",
             description: msg
         });
     };
 
 
     const onFinish = async () => {
         const dataObj = {
             method: "PUT",
             headers: {
                 "Content-Type": "application/json",
                 "Accept": "application/json"
             },
             body: JSON.stringify(data)
         }
         const response = await fetch(url + `/updateinfo`, dataObj);
         const result = await response.text();
         if (result) {
             openNotificationWithIcon("Record Updated Successfully...");
             history.goBack();
             console.log(result);
         }
         else {
             openNotificationWithIcon("Something went wrong. Please try again...");
         }
 
         // console.log(data);
     };
 
    const formItemLayout = {
      labelCol: {
        span:8,
      },
      wrapperCol: {
        span: 16,
      },
    
    };

  return (
    <Content className="container5">
      <Form
        {...formItemLayout}
        form={form}
        onFinish={onFinish}
        name="PERSONAL INFORMATION"
        scrollToFirstError
        style={{ marginTop: "2rem",width:"400px" }}
      >
        
        <Card hoverable="true" className="card5">
          <h1 className="title3">PERSONAL INFORMATION</h1>
          <Divider />
          <InputField
            label="Name"
            name="name"
            style={{ width: "70%",marginLeft:"10px"  }}
            message="Please enter admin name..."
            onChange={onChangeHandler}
            value={data.name}
          />
         
          <EmailComponent
            name="emailId"
            label="Email"
           
            onChange={onChangeHandler}
            value={data.emailId ? data.emailId : ""}
          />

          {/* <Form.Item
            rules={[
              { required: true, message: "Please Select Status to Continue." },
            ]}
            name="status"
          
            label="Status"
          >
            <RadioComponent.Group >
              <RadioComponent value={1}>ACTIVE</RadioComponent>
              <RadioComponent value={0}>INACTIVE</RadioComponent>
            </RadioComponent.Group>
          </Form.Item> */}
          
          {/* <DateFun name="dob" value={data.dob ? data.dob : ""} onChange={date => setData({ ...data, [data.dob]: date })}  /> */}
          <Form.Item
            rules={[
              {
                required: true,
                message: "Please Select Status to Continue.",
              },
            ]}
            
            name="status"
            onChange={(value) => {
              setData({ ...data, status: value.target.value });
            }}
            label="Status"
          >
            <Radio.Group defaultValue={data.status || 1} style={{ width: "70%",marginLeft:"20px"  }}>
              <Radio value={1}>ACTIVE</Radio>
              <Radio value={0}>INACTIVE</Radio>
            </Radio.Group>
          </Form.Item>
          {/* <Form.Item
                        name="dob"
                        label="Date Of Birth"
                        rules={[
                            {
                                required: false,
                                message: 'Please input your birth date!',
                            },
                        ]}
                    >
                        {/* <Row>
                            <Col>
                                <InputComponents name="dob" readOnly={true} style={{width:'300px'}} />
                            </Col>
                            <Col> */}
          {/* <DatePicker
                                    defaultValue={data.dob}
                                    onChange={date => setData({ ...data, dob: date['_d'] })}
                                    style={{width:"325px"}}
                                    
                                /> */}
          {/* </Col>
                        </Row> */}

          <br />
          <Form.Item>
            <Row className="row5">
              <Col className="col5">
                <Button type="primary" htmlType="submit" className="btn51">
                   UPDATE
                </Button>
              </Col>
              <Col className="col5">
                <Button
                  type="primary"
                  className="btn52"
                  onClick={() => history.goBack()}
                >
                  CANCEL
                </Button>
              </Col>
            </Row>
          </Form.Item>
        </Card>
      
      </Form>
    </Content>
  );
}
