import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Table, Button, Divider, Typography, Tag, Spin, message } from "antd";
import "./feedback.css";
import url from "../../../components/httpRequest/http";
// import { PieChart, Pie } from "recharts";
import { BackwardOutlined } from "@ant-design/icons";
import { useHistory } from "react-router-dom";

const { Title } = Typography;
//Defining all required columns of the table
const columns = [
  {
    title: "Name ",
    dataIndex: "Name",
    key: "Name",
  },
  {
    title: " EMAILID ",
    dataIndex: "emailId",
    key: "emailId",
  },
  {
    title: "Rate",
    dataIndex: "rate",
    key: "rate",
  },
  {
    title: "Feedback",
    dataIndex: "feedback",
    key: "feedback",
  },
];

export default function Feedback(props) {
  const [data, setData] = useState("");

  // state for spinner

  const ratehander = [
    { name: "1 st rating", students: 400 },
    { name: "2 st rating", students: 700 },
    { name: "3 st rating", students: 200 },
    { name: "4  st rating", students: 1000 },
  ];

  // getting data from table
  const getData = async () => {
    //setting the state to spin
    // setIsLoading(true);

    const response = await fetch(url + "/account/feedbacktable");
    
    const rows = await response.json();
    
    setData(rows);
   
  };
  useEffect(() => {
    getData();
  }, [data]);
  
  // function for deleting the current sub category

  //using params object
  const history = useHistory();

  //handling back button click event
  const backClickHandler = (event) => {
    return history.goBack();
  };

  return (
    <>
      <Title level={2} style={{ textAlign: "center", marginTop: "10px" }}>
        Feedback Report 
      </Title>
     
      {/* <Title level={2}>Order Items</Title> */}
      <Divider type="horizontal" />
      {/* <PieChart width={700} height={700}>
        <Pie
          data={rate}
          dataKey="students"
          outerRadius={250}
          fill="green"
        />
      </PieChart> */}
      <Table columns={columns} dataSource={data} />
      <Button type="primary" className="btn4" onClick={backClickHandler}>
        <BackwardOutlined />
        Back
      </Button>
    </>
  );
}
