import React, { useState } from 'react'

import { Typography, Input, Row, Col, Select, DatePicker, Divider, Checkbox } from "antd";
import { TagOutlined, BranchesOutlined } from "@ant-design/icons";

import Admintable from './Admintable';

const { Title } = Typography;
const { Search } = Input;
const { Option } = Select;

export default function Admin() {

    // state for order status search
    const [status, setStatus] = useState("");

    const statusChangeHandler = (value, e) => {
        setStatus(value);
        console.log(value);
    }

    return (
        <>
            {/* <Title level={2}>Orders</Title>
            <Divider type="horizontal" />


            <Title level={4} style={{ marginLeft: "2rem" }}>
                <BranchesOutlined className="icons4" /> Filters
            </Title>

            <Row className='row4'>

                <Col className="col">
                    <Select defaultValue="status" style={{ width: 200, marginRight: "15px" }} onChange={(value) => statusChangeHandler(value)}>
                        <Option value="status" selected disabled><TagOutlined className="icons4" />Status</Option>
                        <Option value="">All</Option>
                        <Option value="ACT">ACTIVE</Option>
                        <Option value="INA">INACTIVE</Option>
                       </Select>
                </Col>
                <Col className="col">
                    <DatePicker placeholder="Date" style={{ width: "200px", marginRight: "15px" }} />
                </Col>
                <Col className="col">
                    <Search placeholder="search order..." enterButton style={{ width: "300px", marginRight: "15px" }} />
                </Col>
            </Row>

            <Divider type="horizontal" /> */}

            <Admintable status={status} />

        </>
    )
}
