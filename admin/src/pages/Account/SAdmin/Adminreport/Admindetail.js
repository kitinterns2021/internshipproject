import React, { useState, useEffect } from "react";
import { Divider, Typography, Row, Card, Col } from "antd";
import "antd/dist/antd.css";
import Adetailtable from "./Adetailtable";
import Feedback from './Feedback'
import url from "../../../../components/httpRequest/http";
// import Input from '../../../components/OrderItemsInput/InputField';
import "./Admindetail.css";
import { useLocation } from "react-router-dom";
// import { dataItems, data } from '../ArrayData';

const { Title } = Typography;

export default function Admindetail() {
  // defining state for data
  const [data, setUserData] = useState(false);
  const [orderItems, setOrderItems] = useState(false);

  //using location object
  const location = useLocation();

  //URLSearchParams is a Default JavaScript constructor which gets a search object of location and returns a object which contains all query parameters
  const obj = new URLSearchParams(location.search);
  // console.log(obj.get("id"));
  const id = obj.get("id"); //getting id parameter from object

  // getting data of order items
  const getData = async () => {
    const response = await fetch(url+`/adminreport/admindetail?id=${id}`);
    const rows = await response.json();
    console.log(rows);
    setUserData(rows.userData[0]);
    //setOrderItems(rows.orderItems);
  }; //end getData()

  useEffect(() => {
    getData();
  }, []);

  return (
    <div className="container4">
      <Title level={2}>Admin Details</Title>
      
      {/* <Title level={2}>Order Items</Title> */}
      <Divider type="horizontal" />

      <Feedback />
    </div>
  );
}
