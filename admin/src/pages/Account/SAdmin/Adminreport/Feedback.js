import { Content, Header } from "antd/lib/layout/layout";
import React, { useState } from "react";
import { Route, Switch, Link, useHistory } from "react-router-dom";
import url from "../../../../components/httpRequest/http";
//import Admindetail from "./Admindetail";
import "antd/dist/antd.css";
import { render } from "react-dom";
import { Form,Button,Checkbox,Card,notification,Rate} from 'antd';
import "./Feedback.css";
import TextAreaCom from "../../../../components/TextAreaCom/TextAreaCom";
const Feedback = (props) => {
  const [form] = Form.useForm();
  const [currentValue, setCurrentValue] = useState();
  const [data, setData] = useState({
   
    feedback: "",
  });

  const openNotificationWithIcon = (type,msg) => {
    if(type === 'success')
    {
        notification.success({
            message: "Success!",
            description: msg
        });
    }
    else if(type === 'error')
    {
        notification.error({
            message: "Error",
            description: msg
        });
    }
};// setting state to check email

  const onChangeHandler = (name, e) => {
    setData({
      ...data,
      [name]: e.target.value,
    });
  };
  const clickHandler = async (e) => {
    console.log(data);
    const obj = {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
      body: JSON.stringify(data),
    };

    const response = await fetch(url + `/adminreport/admindetail`, obj);
    const result = await response.json();
    console.log(result);
    if (result.id) {
      openNotificationWithIcon("success", "Record Inserted Successfully...");

      // setting id to the local storage
      // localStorage.setItem("userId",result.id);

      // props.setIsLoggedIn();

      // // redirecting the user to the home page
      // window.location.href="/"
  
    } else {
      openNotificationWithIcon(
       
        "Thank you"
      );
    }
  };

  const ratehandler = (value) => {
    setCurrentValue(value);
    let u = { currentValue };
    if ((u = 1)) {
      console.log("bad");
    } else {
      console.log("good");
    }
  };
  const formItemLayout = {
    labelCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 8,
      },
    },
    wrapperCol: {
      xs: {
        span: 24,
      },
      sm: {
        span: 16,
      },
    },
  };
  const tailFormatLayout = {
    wrapperCol: {
      xs: {
        span: 15,
        offset: 0,
      },
      sm: {
        span: 10,
        offset: 0,
      },
    },
  };
  return (
    <Content className="container5">
      <Form
        {...formItemLayout}
        form={form}
        name="register"
       
        scrollToFirstError
      >
        <Card hoverable="true" className="card5">
          <Header
            style={{
              marginLeft: "160px",
              marginTop: "8px",
              textDecoration: "underline",
              backgroundColor: "transparent",
              width: "480px",
              fontSize: "xx-large",
              fontWeight: "bolder",
              paddingBottom: "80px",
            }}
          >
            <h2>Feedback</h2>
          </Header>
          <span className="feedback1">Your feedback matters!</span>
          <br />
          <span className="feedback2">Help us improve our Website </span>

          <Form.Item
            style={{
              marginLeft: "240px",
              marginTop: "8px",
              width: "480px",
              fontSize: "x-large",
              textDecoration: "underline",
              fontWeight: "600px",
            }}
          >
            <span style={{ marginLeft: "30px" }}>Rating</span>
            <br />
            <Rate
              name="rate"
              defaultValue={5}
            //   value={data.rate ? data.rate : ""}
            //   onChange={onChangeHandler}
            />
            <br /> 
          </Form.Item>

          <TextAreaCom
            name="feedback"
            value={data.feedback}
            onChange={onChangeHandler}
            style={{
              marginLeft: "140px",
              marginTop: "0px",
              width: "350px",
              reSize: "none",
            }}
            placeholder="write your comments"
          />
          <Form.Item>
            <Button
              block
              type="primary"
              htmlType="submit"
              className="login-form-button"
              onClick={clickHandler}
            >
              share with us
            </Button>
          </Form.Item>
        </Card>
      </Form>
    </Content>
  );
};

export default Feedback;
