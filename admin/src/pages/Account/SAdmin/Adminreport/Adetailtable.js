import { Table, Button, Statistic } from "antd";
import React from "react";
import { BackwardOutlined } from "@ant-design/icons";
import { useHistory } from "react-router-dom";

//Defining all required columns of the table
const columns = [
  {
    title: "Category ",
    dataIndex: "productName",
    key: "productName",
  },
  {
    title: " SubCategory ",
    dataIndex: "productPrise",
    key: "productPrise",
    render: (price) => {
      return (
        <Statistic
          value={price}
          precision={2}
          prefix="Rs. "
          valueStyle={{ fontSize: "15px" }}
        />
      );
    },
  },
  {
    title: "Brand",
    dataIndex: "quantity",
    key: "quantity",
  },
  {
    title: "Product",
    dataIndex: "updatedDttm",
    key: "updatedDttm",
  },
];

export default function Adetailtable(props) {
  //using params object
  const history = useHistory();

  //handling back button click event
  const backClickHandler = (event) => {
    return history.goBack();
  };

  return (
    <>
      
      <Table columns={columns} dataSource={props.data} />
      <Button type="primary" className="btn4" onClick={backClickHandler}>
        <BackwardOutlined />
        Back
      </Button>
    </>
  );
}
