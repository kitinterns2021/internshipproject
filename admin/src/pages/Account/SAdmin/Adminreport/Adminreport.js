import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { Table, Button, Typography, Tag, Spin, message } from "antd";
import "./Adminreport.css";
import url from "../../../../components/httpRequest/http";
//import Admindetail from "./Admindetail";
const { Title } = Typography;

const Adminreporttable = (props) => {
  const [data, setData] = useState(false);

  // state for spinner
  const [isLoading, setIsLoading] = useState(false);

  // getting data from table
  const getData = async () => {
    //setting the state to spin
    // setIsLoading(true);

    const response = await fetch(
      url + "/account/adminreport?status=" + props.status || ""
    );
    const rows = await response.json();
    setData(rows);
  };

  useEffect(() => {
    getData();
  }, [data]);

  // function for deleting the current sub category

  //Defining all required columns of the table
  const columns = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
    },

    {
      title: "Date Joined",
      dataIndex: "createdDttm",
      key: "createdDttm",
    },
    {
      title: "Last Modified",
      dataIndex: "updatedDttm",
      key: "updatedDttm",
    },
    // {
    //   title: 'Modified By',
    //   dataIndex: 'updatedBy',
    //   key: 'updatedBy',
    // },
    {
      title: "status",
      key: "status",
      dataIndex: "status",
      render: (status) => {
        let color = "";
        if (status === 1) {
          status = "ACTIVE";
          color = "green";
        }
        else {
          status = "INACTIVE";
          color = "volcano";
        }
        // switch (status) {
        //     case "1":
        //       status = "ACTIVE";
        //        color = "green" ;
        //         break;
        //      case "0":
        //       status = "INACTIVE";
        //        color = "red";
        //        break;
        //       default:
        //         break;
        // }

        return (
          <Tag color={color} key={status}>
            {status.toUpperCase()}
          </Tag>
        );
      },
    },
    {
      title: "Actions",
      key: "key",
      dataIndex: "action",
      render: (text, record) => {
        return (
          <>
            {/* Button to view the orders */}
            <Link to={`/adminreport/admindetail`}>
              <Button
                type="secondary"
                style={{
                  width: 100,
                  marginBottom: 5,
                  marginTop: 5,
                  marginRight: 10,
                }}
              >
                View
              </Button>
            </Link>
          </>
        );
      },
    },
  ];

  return (
    <div className="container3">
      <Title level={2} style={{ textAlign: "center", marginTop: "10px" }}>
        Admin Report
      </Title>

      <>
        {/* <p level={2} style={{ textAlign: "left", fontWeight: "bold" }}>
          {" "}
          Total Available- {data.length}{" "}
        </p>
        <br />
        <Table columns={columns} dataSource={data} />
        <br /> */}
        {data && <p>Showing 1-5 out of 100 results</p> && (
          <Table columns={columns} dataSource={data} />
        )}
      </>
    </div>
  );
};
export default Adminreporttable;
