import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom';
import { Form, Button, Card,Row,  Col, Divider, Typography, notification } from 'antd';
import "antd/dist/antd.css";
import { Content } from 'antd/lib/layout/layout';
import "./Password.css";
import EmailComponent from "../../../../components/EmailComponent/EmailComponent";
import OnlyPassword from '../../../../components/OnlyPassword/OnlyPassword';
import url from '../../../../components/httpRequest/http';
import ChangePassword from './Passwordchange';

const { Title } = Typography;


export default function LoginSecurity() {

    const [form] = Form.useForm()

    const formItemLayout = {

       
        labelCol: {
            span:8,
          },
          wrapperCol: {
            span: 16,
          },
        

    };

    const history = useHistory();
    const [data, setData] = useState(false);

    // setting state for changing the password
    const [isTrue, setIsTrue] = useState(false);

    //setting state for showing error
    const [isError, setIsError] = useState(false);

    const getData = async () => {
        const response = await fetch(url + `/logininfo?id=${localStorage.getItem("userId")}`);
        const rows = await response.json();
        setData({
            ...data,
            emailId: rows[0].emailId
            // password: rows[0].password
        });
        // console.log(rows[0]);
        form.setFieldsValue({ emailId: rows[0].emailId });

    }

    useEffect(() => {
        getData();

    }, []
    )

    const onChangeHandler = (name, e) => {
        setData({
            ...data,
            [name]: e.target.value
        })
    }

    
    const onFinish = async () => {
        const dataObj = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(data)
        }
        const response = await fetch(url + `/changepassword`, dataObj);
        const result = await response.json();
        console.log(result);

        if (result.res === true) {
            setIsTrue(true);
        }
        else {
            setIsError(true);
            setTimeout(()=>{
                setIsError(false);
            }, 5000);
        }

        // console.log(data.emailId);
        // console.log(data.password);
    
    }


    return (
        <>
            {
                isTrue ?

                    <ChangePassword />
                    :
                    <Content className="container5">
                        < Form {...formItemLayout} form={form} name="register" onFinish={onFinish} scrollToFirstError style={{ marginTop: "2rem" }
                     }>
                            

                            <Card hoverable="true" className="card5">
                            <Title level={2} style={{ textAlign: "left" }}><u>Login & Security</u></Title><br/><br/>
                                <Divider/>
                    
    
                                <EmailComponent name="emailId"  label="Email" onChange={onChangeHandler} value={data.emailId ? data.emailId : ""} />
                                <OnlyPassword name="password" label="Password" onChange={onChangeHandler} />
                                {
                                    isError && <p style={{color: "red", marginLeft: "140px"}}>Entered password does'nt match, please enter correct password.</p>
                                }
                                <Form.Item
                                    wrapperCol={{
                                        offset: 4,
                                        span: 16,
                                    }}
                                ><br />
                                <Row className="row5">
                                  <Col className="col5">
                                    <Button type="primary" htmlType="submit"  className="btn51" >
                                        Edit Password
                                    </Button>
                                    </Col>
                                    <Col className="col5">
                                    <Button type="primary" onClick={() => { history.goBack() }} className="btn52">
                                        Back
                                    </Button><br /><br />
                                    </Col>
                                </Row>
                                </Form.Item>

                            </Card>
                        </Form >
                    </Content >
            }
        </>
    )
}
