import React, { useState, useEffect } from "react";
import {
  Form,
  Button,
  Radio,
  Card,
  Select,
  Typography,
  Row,
  Col,
  Divider,
  notification,
  Spin,
} from "antd";
import { Content } from "antd/lib/layout/layout";
import "antd/dist/antd.css";
import "./Admininfo.css";
import moment from "moment";
import "./Admininfo.css"
import { useHistory, useLocation } from "react-router-dom";
import InputField from "../../../components/InputField/InputField";
import url from "../../../components/httpRequest/http";
import EmailComponent from "../../../components/EmailComponent/EmailComponent";
import Password from "../../../components/Password/Password";
const { Title } = Typography;
const dateFormat = "YYYY/MM/DD";
// const customFormat = value => `custom format: ${value.format(dateFormat)}`;

export default function Admininfo() {
  const [form] = Form.useForm();
  const [spin, setSpin] = useState(false);

  // creating current date
  const d = new Date();
  const date = d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear();

  // setting state for the data
  const [data, setData] = useState({
    id: "",
    name: "",
    emailId:"",
    password: "",
    createdBy: localStorage.getItem("userId"),
    createdDttm: date,
    status: "",
    userName: "",
  });

  // getting history object
  const history = useHistory();

  // getting current user from the backend
  const getUser = async () => {
    const response = await fetch(
      url + `/admin-information?userId=${localStorage.getItem("userId")}`
    );
    const result = await response.json();
    setData({
      ...data,
      userName: result[0].userName,
    });
    form.setFieldsValue({ createdBy: result[0].userName, createdDttm: date });
  }; //end getUser()

  // getting sub category data with its id from backend
  const getAllData = async (id) => {
    // console.log(id)
    setSpin(true);
    const response = await fetch( url + `/account/admin?id=${id}&userId=${localStorage.getItem("userId")}`
    );
    const rows = await response.json();
    setData({
      ...data,
      id: rows[0][0].id,
      name: rows[0][0].name,
      emailId: rows[0][0]. emailId,
      password: rows[0][0].password,
      status: rows[0][0].status,
      userName: rows[1][0].userName,
    });
    form.setFieldsValue({
      ...rows[0][0],
      createdBy: rows[1][0].userName,
      createdDttm: date,
    });
    // setData({ ...data,...rows[0]});
    setSpin(false);
  }; //end getData

  // getting location object of the url and getting the query parameters from it
  const location = useLocation();

  const searchParams = new URLSearchParams(location.search);
  useEffect(() => {
    if (searchParams.get("id")) {
      getAllData(searchParams.get("id"));
      console.log(data);
    } else {
      getUser();
    }

    return () => {
      // Component unmount function
      setData({
        id: "",
        name: "",
        emailId: "",
        password: "",
        createdBy: localStorage.getItem("userId"),
        createdDttm: "",
        status: "",
        userName: "",
      });
    };
  }, []);

  // handling on change event
  const onChangeHandler = (name, e) => {
    setData({
      ...data,
      [name]: e.target.value,
    });
  }; //end on change handler

  // notification for confirmation
  const openNotificationWithIcon = (msg) => {
    notification.success({
      message: "Success!",
      description: msg,
    });
  };

  // handling button click event
  const clickHandler = async () => {
    if (searchParams.get("id")) {
      const dataObj = {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
        body: JSON.stringify({
          ...data,
          date: Math.floor((data.createdDttm, "DD-MM-YYYY") / 1000.0),
          id: searchParams.get("id"),
        }),
      };
      const response = await fetch(url + "/account/admin/updateadmin", dataObj);
      const result = await response.text();
      if (result) {
        openNotificationWithIcon("Record Updated Successfully...");
        history.goBack();
        console.log(result);
      } else {
        openNotificationWithIcon("Something went wrong. Please try again...");
      }
    } else {
      const dataObj = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
        body: JSON.stringify({
          ...data,
          date: Math.floor(moment(data.createdDttm, "DD-MM-YYYY") / 1000.0),
        }),
      };
      const response = await fetch(url + "/account/admin/addadmin", dataObj);
      const result = await response.text();
      openNotificationWithIcon("Record Inserted Successfully...");
      history.goBack();
      console.log(result);
    }
  }; //end clickHandler

  const formItemLayout = {
   
      labelCol: {
        span:8,
      },
      wrapperCol: {
        span: 16,
      },
    
    // wrapperCol: {
    //   xs: {
    //     span: 24,
    //   },
    //   sm: {
    //     span: 16,
    //   },
    // },
  };

  return (
    <Content className="container5">
      <Form
        {...formItemLayout}
        form={form}
        onFinish={clickHandler}
        name="ADMIN INFORMATION"
        scrollToFirstError
        style={{ marginTop: "2rem" }}
      >
        {spin ? (
          <Spin
            tip="Loading Data..."
            style={{
              marginLeft: "550px",
              marginTop: "50px",
              marginBottom: "50px",
              width: "100px",
            }}
          />
        ) : (
          <Card hoverable="true" className="card5">
            <h1 className="title3">
              {!searchParams.get("id") ? "ADD ADMIN" : "UPDATE ADMIN"}
            </h1>
            <Divider />
            <InputField
              label="Name"
              name="name"
              style={{ width: "70%",marginLeft:"10px"  }}
              message="Please enter admin name..."
              onChange={onChangeHandler}
              value={data.name}
            />
           
            <EmailComponent
              name="emailId"
              label="Email"
              onChange={onChangeHandler}
              value={data.emailId?data.emailId:""}
            />

            {/* <Form.Item
            rules={[
              { required: true, message: "Please Select Status to Continue." },
            ]}
            name="status"
          
            label="Status"
          >
            <RadioComponent.Group >
              <RadioComponent value={1}>ACTIVE</RadioComponent>
              <RadioComponent value={0}>INACTIVE</RadioComponent>
            </RadioComponent.Group>
          </Form.Item> */}
            <Password
              label="Password"
              name="password"
              onChange={onChangeHandler}
              value={data.password}
            />
            {/* <DateFun name="dob" value={data.dob ? data.dob : ""} onChange={date => setData({ ...data, [data.dob]: date })}  /> */}
            <Form.Item
              rules={[
                {
                  required: true,
                  message: "Please Select Status to Continue.",
                },
              ]}
              name="status"
              onChange={(value) => {
                setData({ ...data, status: value.target.value });
              }}
              label="Status"
            >
              <Radio.Group defaultValue={data.status || 1}  style={{ width: "70%",marginLeft:"50px"  }}>
                <Radio value={1}>ACTIVE</Radio>
                <Radio value={0}>INACTIVE</Radio>
              </Radio.Group>
            </Form.Item>
            {/* <Form.Item
                        name="dob"
                        label="Date Of Birth"
                        rules={[
                            {
                                required: false,
                                message: 'Please input your birth date!',
                            },
                        ]}
                    >
                        {/* <Row>
                            <Col>
                                <InputComponents name="dob" readOnly={true} style={{width:'300px'}} />
                            </Col>
                            <Col> */}
            {/* <DatePicker
                                    defaultValue={data.dob}
                                    onChange={date => setData({ ...data, dob: date['_d'] })}
                                    style={{width:"325px"}}
                                    
                                /> */}
            {/* </Col>
                        </Row> */}

            <br />
            <Form.Item>
              <Row className="row5">
                <Col className="col5">
                  <Button type="primary" htmlType="submit" className="btn511">
                    {!searchParams.get("id") ? "SAVE" : "UPDATE"}
                  </Button>
                </Col>
                <Col className="col5">
                  <Button
                    type="primary"
                    className="btn512"
                    onClick={() => history.goBack()}
                  >
                    CANCEL
                  </Button>
                </Col>
              </Row>
            </Form.Item>
          </Card>
        )}
      </Form>
    </Content>
  );
}
