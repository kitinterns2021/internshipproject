import { Table, Tag, message, Button, Statistic } from 'antd';
import React, { useState, useEffect } from 'react';
// import { data } from '../ArrayData';
import { Link } from "react-router-dom";
import "./Orders.css";
// import ConfirmBox from './ConfirmBox';

// importing url
import url from '../../../components/httpRequest/http';
import TableButton from '../../../components/TableButton/TableButton';

export default function ContentTable(props) {

    // state for orders data
    const [orders, setOrders] = useState(false);

    // getting orders data from backend
    const data = async () => {
        const response = await fetch(url + "/orders?status=" + props.status || "");
        const rows = await response.json();
        setOrders(rows);
    }

    useEffect(
        () => {
            data();
        }, [orders]
    )

    // function for updating the current order status
    async function confirmHandler(obj, e) {
        // updating the order
        const response = await fetch(url + `/order/update-order-status?id=${obj.id}&status=${obj.status}`, { "method": "PUT" });
        const msg = await response.json();
        console.log(e);
        message.success(msg.textMsg);
        setOrders(false);
    }


    function cancel(e) {
        console.log(e);
        message.error('Action aborted.');
    }

    //Defining all required columns of the table
    const columns = [
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: "Contact",
            dataIndex: "contact",
            key: "contact",
        },
        {
            title: 'Total Amount',
            dataIndex: 'price',
            key: 'price',
            render: price => {
                return <Statistic value={price} precision={2} prefix="Rs. " valueStyle={{fontSize:"15px"}}/>
            }
        },
        {
            title: 'Status',
            key: 'orderStatus',
            dataIndex: 'orderStatus',
            render: Status => {
                let color = "";
                switch (Status) {
                    case "BOK":
                        Status = "BOOKED"
                        color = "blue";
                        break;
                    case "DIS":
                        Status = "DISPATCHED";
                        color = "geekblue";
                        break;
                    case "DLI":
                        Status = "DELIVERED"
                        color = "green";
                        break;
                    case "CNL":
                        Status = "CANCELLED"
                        color = "volcano";
                        break;
                    default:
                        break;
                }
                return (
                    <Tag color={color} key={Status}>
                        {Status.toUpperCase()}
                    </Tag>
                );
            }
        },
        {
            title: "Date",
            key: "createdDttm",
            dataIndex: "createdDttm"
        },
        {
            title: "Last Modified",
            key: "updatedDttm",
            dataIndex: "updatedDttm"
        },
        {
            title: "Actions",
            key: "key",
            dataIndex: "action",
            render: (text, record) => {
                return <>

                    {/* Button to view the orders */}
                    <Link to={`/order/order-items?id=${record.id}`}>
                        <Button type="secondary" style={{width:100,marginBottom:5,marginTop:5, marginRight:10}}>View</Button>
                    </Link>

                    {
                        record.orderStatus === "BOK" &&
                        /* Dispatch Order */
                        < TableButton
                            id={record.id}
                            title="Do you want to dispatch this order?"
                            onConfirm={confirmHandler}
                            onCancel={cancel}
                            type="primary"
                            buttonText="Dispatch"
                            status="DIS"
                            style={{width:100,marginBottom:5,marginTop:5, marginRight:10}}

                        />
                    }

                    {
                        record.orderStatus === "DIS" &&
                        /* Deliver Order  */
                        < TableButton
                            id={record.id}
                            title="Is this order delivered to the customer?"
                            onConfirm={confirmHandler}
                            onCancel={cancel}
                            type="primary"
                            buttonText="Delivered?"
                            status="DLI"

                            // className ="button41"
                            style={{ width: '100px', marginBottom:5,marginTop:5, marginRight:10 }}
                        />
                    }

                    {
                        (record.orderStatus === "BOK" || record.orderStatus === "DIS") &&
                        /* Cancel Order  */
                        < TableButton
                            id={record.id}
                            title="Are you sure to cancel this order?"
                            onConfirm={confirmHandler}
                            onCancel={cancel}
                            type="danger"
                            buttonText="Cancel"
                            status="CNL"
                            style={{ width: '100px',marginBottom:5,marginTop:5, marginRight:10 }}

                        />
                    }

                </>
            }
        }
    ];

    return (
        <>
            {orders &&
                <p>Showing 1-5 out of 100 results</p> &&
                <Table columns={columns} dataSource={orders} />
            }
        </>
    )
}
