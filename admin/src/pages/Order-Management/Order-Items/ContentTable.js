import { Table, Button, Statistic, Image, Row, Col } from 'antd';
import React from 'react';
import { BackwardOutlined } from "@ant-design/icons";
import { useHistory } from "react-router-dom";
import url from '../../../components/httpRequest/http';

//Defining all required columns of the table
const columns = [
    {
        title: 'Product Name',
        dataIndex: 'productName',
        key: 'productName',
        render: (productName, record) => {    //==> this returns current column value and then full row/record
            return (
                <Row>
                    <Col>
                        <Image
                            src={url + `/images/${record.imageURL}`}
                            preview={false}
                            height={50}
                            width={50}
                        />
                    </Col>
                    <Col style={{ marginTop:"15px",marginLeft:"15px" }}>
                        {productName}
                    </Col>
                </Row>
            )
        }
    },
    {
        title: "Product Price",
        dataIndex: "productPrise",
        key: "productPrise",
        render: price => {
            return <Statistic value={price} precision={2} prefix="Rs. " valueStyle={{ fontSize: "15px" }} />
        }
    },
    {
        title: 'Quantity',
        dataIndex: 'quantity',
        key: 'quantity',
    },
    {
        title: 'Last Modified',
        dataIndex: 'updatedDttm',
        key: 'updatedDttm',
    }
];




export default function ContentTable(props) {

    //using params object
    const history = useHistory();

    //handling back button click event
    const backClickHandler = event => {
        return (
            history.goBack()
        )
    }

    return (
        <>
            <p>Order Items Available - {props.length} </p>
            <Table columns={columns} dataSource={props.data} />
            <Button type='primary' className='btn4' onClick={backClickHandler}><BackwardOutlined />Back</Button>
        </>
    )
}
