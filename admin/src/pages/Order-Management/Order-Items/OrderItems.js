import React, { useState, useEffect } from 'react';
import { Divider, Typography, Row, Card, Col } from 'antd';
import "antd/dist/antd.css";
import ContentTable from './ContentTable';
import url from '../../../components/httpRequest/http';
// import Input from '../../../components/OrderItemsInput/InputField';
import "./OrderItems.css";
import { useLocation } from "react-router-dom";
// import { dataItems, data } from '../ArrayData';


const { Title } = Typography;

export default function OrderItems() {

    // defining state for data
    const [data,setUserData] = useState(false);
    const [orderItems,setOrderItems] = useState(false);

    //using location object
    const location = useLocation();

    //URLSearchParams is a Default JavaScript constructor which gets a search object of location and returns a object which contains all query parameters
    const obj = new URLSearchParams(location.search);
    // console.log(obj.get("id"));
    const id = obj.get("id"); //getting id parameter from object

    // getting data of order items
    const getData = async () =>{
        const response = await fetch(url + `/orders/order-items?id=${id}`);
        const rows = await response.json();
        console.log(rows);
        setUserData(rows.userData[0]);
        setOrderItems(rows.orderItems);
    }//end getData()

    useEffect(
        ()=>{
            getData();
        },[]
    );

    
    return (
        <div className='container4'>
            
            <Title level={2}>Order Details</Title>
            <Divider type='horizontal' />
            <Card bordered={true} className='innerContainer4'>

                <Row>
                    <Col className='col42'>
                        {/* <Row >
                            <Col className='col41'><h3>Order ID:</h3></Col>
                            <Col className='col41'><h3>{id}</h3></Col>
                        </Row> */}
                        <Row>
                            <Col className='col41'><h3>Name:</h3></Col>
                            <Col className='col41'><h3>{data.userName}</h3></Col>
                        </Row>
                        <Row  >
                            <Col className='col41'><h3>Date:</h3></Col>
                            <Col className='col41'><h3>{data.createdDttm}</h3></Col>
                        </Row>
                        <Row  >
                            <Col className='col41'><h3>Last Modified:</h3></Col>
                            <Col className='col41'><h3>{data.updatedDttm}</h3></Col>
                        </Row>
                    </Col>
                    <Col >
                        {/* <Row>
                            <Col className='col41'><h2 style={{ color: "orange" }}>Address Details</h2></Col>
                        </Row> */}
                        <Row>
                            <Col className='col41'><h3>Address Line 1:</h3></Col>
                            <Col className='col42'><h3>{data.addressLine1}</h3></Col>
                        </Row>
                        <Row>
                            <Col className='col41'><h3>Address Line 2:</h3></Col>
                            <Col className='col42'><h3>{data.addressLine2}</h3></Col>
                        </Row>
                        <Row>
                            <Col className='col41'><h3>City:</h3></Col>
                            <Col className='col42'><h3>{data.city}</h3></Col>
                        </Row>
                        <Row>
                            <Col className='col41'><h3>State:</h3></Col>
                            <Col className='col42'><h3>{data.state}</h3></Col>
                        </Row>
                        <Row>
                            <Col className='col41'><h3>Pincode:</h3></Col>
                            <Col className='col42'><h3>{data.pincode}</h3></Col>
                        </Row>
                    </Col>
                </Row>

            </Card>

            <Title level={2}>Order Items</Title>
            <Divider type='horizontal' />

            <ContentTable data={orderItems} length={orderItems.length} />
        </div>
    )
}