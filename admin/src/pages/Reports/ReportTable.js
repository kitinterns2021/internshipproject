import React from 'react';
import { Table, Typography, Divider } from "antd";
import "antd/dist/antd.css"
import "./ReportTable.css";

const { Title } = Typography;

export default function ReportTable(props) {

  return (
    <div className='container4223233'>
        <Title level={2}>
            {props.title}
        </Title>
        <Divider />

        <Title level={5}>
          {props.totalLength} Records Found
        </Title>

        <Table dataSource={props.data} columns={props.columns}/>
    </div>
  )
}
