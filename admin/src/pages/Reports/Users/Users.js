import React, { useState, useEffect } from 'react';
import ReportTable from "../ReportTable";
import { Tag } from "antd";
import url from '../../../components/httpRequest/http';

export default function Users() {

    // setting state for data
    const [data,setData] = useState([]);

    // getting all data
    const getData = async () => {
        const response = await fetch(url + "/reports/users");
        const result = await response.json();

        // setting data
        setData([
            ...result
        ])
    }//end getData

    useEffect(() => {
        getData();
    },[data]);

    // table columns
    const columns = [
        {
            title:"Name",
            dataIndex:"name",
            key:"name"
        },
        {
            title:"Contact",
            dataIndex:"contact",
            key:"contact"
        },
        {
            title:"Email",
            dataIndex:"emailId",
            key:"emailId"
        },
        {
            title:"Date Joined",
            dataIndex:"joiningDate",
            key:"joiningDate"
        },
        {
            title:"Gender",
            dataIndex:"gender",
            key:"gender",
            render: gender => {
                switch (gender) {
                    case "M":
                        return "Male";
                    case "F":
                        return "Female";
                    case "O":
                        return "Others";
                    default:
                        return "Not Yet Updated";
                }
            }
        },
        {
            title:"Date of Birth",
            dataIndex:"dob",
            key:"dob",
            render: dob => {
                if(dob === null || "")
                {
                    return "Not Yet Updated"
                }
                else{
                    return dob
                }
            }
        },
        {
            title:"Status",
            dataIndex:"status",
            key:"status",
            render: status => {
                if(status === 1)
                {
                    return <Tag color="green">ACTIVE</Tag>
                }
                else if(status === 0)
                {
                    return <Tag color="volcano">INACTIVE</Tag>
                }
            }
        }
    ];

    // getting all users data

  return (
    <ReportTable title="Website Users" data={data} columns={columns} totalLength={data.length} />
  )
}
