import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { Table, Button, Typography, Tag, Spin, message } from 'antd';
import "./SubCatTable.css";
import url from "../../../components/httpRequest/http";
import TableButton from '../../../components/TableButton/TableButton';

const { Title } = Typography;

const SubCatTable = () => {


  const [data, setData] = useState(false);

  // state for spinner
  const [isLoading, setIsLoading] = useState(false);

  // getting data from table
  const getData = async () => {
    //setting the state to spin
    // setIsLoading(true);

    const response = await fetch(url + "/sub-categories");
    const rows = await response.json();
    setData(rows);

    //setting the state of spin to false
    setIsLoading(false);
  }

  useEffect(
    () => {
      getData();
    }, [data]
  )

  // function for deleting the current sub category
  async function confirmHandler(obj, e) {
    // updating the order
    const response = await fetch(url + `/sub-categories/delete-subCategory?id=${obj.id}`, { "method": "DELETE" });
    const msg = await response.json();
    console.log(e);
    message.success(msg.textMsg);
    setData(false);
  }


  function cancel(e) {
    console.log(e);
    message.error('Action aborted.');
  }

  //Defining all required columns of the table
  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Created By',
      dataIndex: 'createdBy',
      key: 'createdBy',
    },
    {
      title: 'Date',
      dataIndex: 'createdDttm',
      key: 'createdDttm',
    },
    {
      title: 'Last Modified',
      dataIndex: 'updatedDttm',
      key: 'updatedDttm',
    },
    // {
    //   title: 'Modified By',
    //   dataIndex: 'updatedBy',
    //   key: 'updatedBy',
    // },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
      render: status => {
        let color = "";
        if (status === 1) {
          status = "ACTIVE";
          color = "green";
        }
        else {
          status = "INACTIVE";
          color = "volcano";
        }
        return (
          <Tag color={color} key={status}>
            {status.toUpperCase()}
          </Tag>
        )
      }
    },
    {
      title: "Actions",
      key: "key",
      dataIndex: "action",
      render: (text, record) => {
        return <>
          <Button type='primary' className='button3'>
            <Link to={`/update-subCategory?id=${record.id}`}>
              Edit
            </Link>
          </Button>

          {/* Delete button will only for those records which are in-active */}
          {
          
            <TableButton
              type="danger"
              id={record.id}
              className="button3"
              title="Do you want to delete this sub-category?"
              buttonText="Delete"
              onConfirm={ confirmHandler }
              onCancel={ cancel }
            />
          }
        </>
      }
    }

  ];


  return (
    <div className="container3">
      <Title level={2} style={{ textAlign: "left" }}>Sub Categories</Title>
      {
        isLoading ?
          <Spin style={{ marginLeft: "550px", marginTop: "50px", marginBottom: "50px" }} tip="Loading Data..." />
          :
          <>
            <div style={{ display: "flex" }}>
              <Button type="default" htmlType="submit" style={{ marginLeft: "auto" }}>
                <Link to="/add-subCategory">Add Sub Category</Link>
              </Button>
            </div>
            <br />
            <Table columns={columns} dataSource={data} />
            <br />
          </>
      }
    </div>
  );
};
export default SubCatTable;
