import React, { useState, useEffect } from "react";
import {
  Form,
  Button,
  Radio,
  Card,
  Select,
  Row,
  Col,
  Divider,
  notification,
  Spin,
} from "antd";
import "antd/dist/antd.css";
import "./Category.css";
import { useHistory, useLocation } from "react-router-dom";
import InputField from "../../../components/InputField/InputField";
import url from "../../../components/httpRequest/http";

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};

const Category = () => {
  const [form] = Form.useForm();

  // seting state for spinner
  const [spin, setSpin] = useState(false);
 
  // creating current date
  const d = new Date();
  const date = d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear();

// setting state for the data
const [data, setData] = useState({
  id: "",
  name: "",
  createdBy: localStorage.getItem("userId"),
  createdDttm: date,
  status: "",
  userName: "",
});

// getting history object
const history = useHistory();

  // getting current user from the backend
  const getUser = async () => {
    const response = await fetch(url + `/admin-information?userId=${localStorage.getItem("userId")}`);
    const result = await response.json();
    setData({
      ...data,
      userName: result[0].userName,
    });
    form.setFieldsValue({ createdBy: result[0].userName, createdDttm: date });
  }; //end getUser()


  // getting category data with its id from backend
  const getAllData = async (id) => {
    // console.log(id)
    setSpin(true);
    const response = await fetch(url + `/categories?id=${id}&userId=${localStorage.getItem("userId")}`);
    const rows = await response.json();
    setData({
      ...data,
      id: rows[0][0].id,
      name: rows[0][0].name,
      status: rows[0][0].status,
      userName: rows[1][0].userName,
    });
    form.setFieldsValue({
      ...rows[0][0],
      createdBy: rows[1][0].userName,
      createdDttm: date,
    });
    // setData({ ...data,...rows[0]});
    setSpin(false);
  }; //end getData

  // getting location object of the url and getting the query parameters from it
  const location = useLocation();

  const searchParams = new URLSearchParams(location.search);
  useEffect(() => {
    if (searchParams.get("id")) {
      getAllData(searchParams.get("id"));
      console.log(data);
    } else {
      getUser();
    }
    return () => {
      // Component unmount function
      setData({
        id: "",
        name: "",
        createdBy: localStorage.getItem("userId"),
        createdDttm: "",
        status: "",
        userName: "",
      });
    };
  }, []);

  //handiling onchange event
  // handling on change event
  const onChangeHandler = (name, e) => {
    setData({
      ...data,
      [name]: e.target.value,
    });
  }; //end on change handler

  // notification for confirmation
  const openNotificationWithIcon = (msg) => {
    notification.success({
      message: "Success!",
      description: msg,
    });
  };

  // handling button click event

    const clickHandler = async () => {
      if (searchParams.get("id")) {
        const dataObj = {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Accept: "application/json",
          },
          body: JSON.stringify({
            ...data,
            date: Math.floor((data.createdDttm, "DD-MM-YYYY") / 1000.0),
            id: searchParams.get("id"),
          }),
        };
      const response = await fetch(url + "/categories/update-category",dataObj);
      const result = await response.text();
      if (result) {
        openNotificationWithIcon("Record Updated Successfully...");
        history.goBack();
        console.log(result);
      } else {
        openNotificationWithIcon("Something went wrong. Please try again...");
      }
    }
    //add data
    else {
      const dataObj = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
        body: JSON.stringify({
          ...data,
          date: Math.floor(data.createdDttm._d / 1000.0),
        }),
      };
      const response = await fetch(url + "/categories/add-category", dataObj);
      const result = await response.text();
      openNotificationWithIcon("Record Inserted Successfully...");
      history.goBack();
      console.log(result);
    }
  }; //end clickHandler

  return (
    <Form
      {...layout}
      form={form}
      onFinish={clickHandler}
      className="register-form3"
      style={{ width: "500px" }}
    >
      {spin ? (
        <Spin
          tip="Loading Data..."
          style={{
            marginLeft: "550px",
            marginTop: "50px",
            marginBottom: "50px",
            width: "100px",
          }}
        />
      ) : (
        <Card className="card3" style={{ width: "500px",height:"400px", marginLeft: "380px" }}>
          <h1 className="title3">
            {!searchParams.get("id") ? "CATEGORY" : "UPDATE CATEGORY"}
          </h1>
          <Divider />

          <InputField
            label="Name"
            name="name"
            type="text"
            message="Please enter category name..."
            placeholder="Enter category name here"
            onChange={onChangeHandler}
            value={data.name}
          />

          {/* <InputField
            label={searchParams.get("id") ? "Updated By" : "Created By"}
            name="createdBy"
            type="text"
            readOnly={true}
            value={data.userName}
          /> */}

          {/* <InputField
            label="Date"
            name="createdDttm"
            type="text"
            value={data.createdDttm}
          /> */}

        <Form.Item
            
            name="status"
            onChange={(value) => {
              setData({ ...data, status: value.target.value });
            }}
            label="Status"
          >
            <Radio.Group defaultValue={data.status || 1}>
              <Radio value={1}>ACTIVE</Radio>
              <Radio value={0}>INACTIVE</Radio>
            </Radio.Group>
          </Form.Item>

          <Form.Item>
            <Row className="row3">
              <Col className="col3">
                <Button type="primary" htmlType="submit" className="btnA31">
                  {!searchParams.get("id") ? "SAVE" : "UPDATE"}
                </Button>
              </Col>
              <Col className="col3">
                <Button
                  type="primary"
                  className="btnC32"
                  onClick={() => history.goBack()}
                >
                  CANCEL
                </Button>
              </Col>
            </Row>
          </Form.Item>
        </Card>
      )}
    </Form>
  );
};
export default Category;
