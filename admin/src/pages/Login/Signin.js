import { Form, Input, Button, Checkbox, Card,notification,message} from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import "./Signin.css";
import { Header } from 'antd/lib/layout/layout';
import url from '../../components/httpRequest/http';
import React, { useState } from 'react';

const Signin = (props) => {

  // defining state for the error message
  const [isError, setIsError] = useState(false);
  const openNotificationWithIcon = (msg) => {
    notification.success({
        message: "Success!",
        description: msg
    });
  };

  const onFinish = async (values) => {
    // checking login validity
    // console.log(values)
    const response = await fetch(url + `/login?email=${values.email}&password=${values.password}`);
    const result = await response.json();

    if (result.loggedIn === true) {

      // setting logged in state to local storage
      localStorage.setItem("isLoggedIn","true");
      openNotificationWithIcon("Login Successfully...");
      // setting id of current user to the local storage
      localStorage.setItem("userId",result.id);

      props.loginStatus(true);
    }
    else {
      setIsError(true);
      setTimeout(() => {
        setIsError(false);
      }, 5000);
    }

  };
    // function for updating the current order status
   
      


  return (
    <Form
      className="form51"
      name="normal_login"

      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
    >
      <Card hoverable="true" className="card51">
        <Header style={{ marginLeft: "0px", marginRight: "0px", textDecoration: "underline", backgroundColor: "transparent", width: "350px", fontSize: "x-large", fontWeight: "bolder", paddingBottom: "80px" }}>Signin</Header>

        <Form.Item
          name="email"
          rules={[
            {
              required: true,
              message: 'Please enter your Email!',
            },
          ]}
        >
          <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Email" />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: 'Please enter your Password!',
            },
          ]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Password"
          />
        </Form.Item>
        {
          isError &&
          <p style={{ color: "red" }}>Email or Password is invalid. Please check and try again!</p>
        }
        <Form.Item>
          <Form.Item name="remember" valuePropName="checked" noStyle>
            <Checkbox>Remember me</Checkbox>
          </Form.Item>

          {/* <a className="login-form-forgot" href="">
            Forgot password
          </a> */}
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit" className="button51">
            Sign in
          </Button>

        </Form.Item>
      </Card>
    </Form>

  );
};

export default Signin;