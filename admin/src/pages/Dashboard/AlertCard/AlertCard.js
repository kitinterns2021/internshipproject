import React from 'react';
import { Layout, Card, Col, Row, Divider } from "antd";
import "antd/dist/antd.css";
import "./AlertCard.css";

const { Content, Header} = Layout;

export default function AlertCard() {
    return (
        <>
            <Content style={{ margin: '16px 16px' }}>
                <div className="alert-content4">
                    <Header style={{ paddingLeft: 1, backgroundColor: "transparent", fontSize: "x-large", fontWeight: "bolder" }}>Alert</Header>
                    <div className="site-card-wrapper">
                        <Row gutter={16}>
                            <Col span={8}>
                                <Card title="Digital Multimeter" hoverable="true" className="alert-card4">
                                    <h5>20</h5> remaining
                                </Card>
                            </Col>
                            <Col span={8}>
                                <Card title="Vernier Calliper" hoverable="true" className="alert-card4">
                                    <h5>15</h5> remaining
                                </Card>
                            </Col>
                            <Col span={8}>
                                <Card title="Screw Gauge (Micrometer)" hoverable="true" className="alert-card4">
                                    <h5>10</h5> remaining
                                </Card>
                            </Col>
                        </Row>
                    </div>
                </div>
            <Divider type="horizontal" />
            </Content>
        </>
    )
}
