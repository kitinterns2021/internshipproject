import React, { useState } from 'react';
import { Link, useLocation } from "react-router-dom";
import { Menu, Layout, Popconfirm, message } from "antd";
import {
    AppstoreOutlined,
    AppstoreAddOutlined,
    HomeOutlined,
    ShoppingCartOutlined,
    UserOutlined,
    TagsOutlined,
    BoldOutlined,
    CodeSandboxOutlined,
    BarChartOutlined,
    SettingOutlined,
    LogoutOutlined
} from "@ant-design/icons";


const { SubMenu } = Menu;
const { Sider } = Layout;

export default function Navigation(props) {
    const [state, setState] = useState(false);
    const location = useLocation();

    const onCollapse = () => {
        setState(true);
    };

    const logoutConfirmHandler = (e) => {

        if (localStorage.getItem("isLoggedIn") || localStorage.getItem("userId")) {
            // removing isLoggedIn state from local storage
            localStorage.removeItem("isLoggedIn");

            // removing userId from local storage
            localStorage.removeItem("userId");
        }

        // setting setLoggedIn state to false
        props.loginStatus(false);

        message.success("Sign out successfully!");
    }

    return (
        <Sider collapsible state={state} onCollapse={onCollapse}>
            <Menu theme="dark" defaultSelectedKeys={location.pathname} mode="inline">

                <Menu.Item key="/home" icon={<HomeOutlined />}>
                    <Link to="/home">
                        Dashboard
                    </Link>
                </Menu.Item>

                <Menu.Item key="/users" icon={<UserOutlined />}>
                    <Link to="/users">
                        Users
                    </Link>
                </Menu.Item>

                <Menu.Item key="/brands" icon={<BoldOutlined/>}>
                    <Link to="/brands">
                        Brands
                    </Link>
                </Menu.Item>

                <Menu.Item key="/tags" icon={<TagsOutlined/>}>
                    <Link to="/tags">
                        Tags
                    </Link>
                </Menu.Item>

                <SubMenu key="sub1" icon={<AppstoreOutlined />} title="Categories">
                    <Menu.Item key="/categories" icon={<AppstoreAddOutlined />}>
                        <Link to="/categories">
                            Category
                        </Link>
                    </Menu.Item>

                    <Menu.Item key="/subCategory" icon={<AppstoreAddOutlined />}>
                        <Link to="/sub-category">
                            Subategory
                        </Link>
                    </Menu.Item>
                </SubMenu>

                <Menu.Item key="/product" icon={<CodeSandboxOutlined />}>
                    <Link to="/product">
                        Products
                    </Link>
                </Menu.Item>

                <Menu.Item key="/orders" icon={<ShoppingCartOutlined />}>
                    <Link to="/orders">
                        Orders
                    </Link>
                </Menu.Item>

                <Menu.Item key="/reports" icon={<BarChartOutlined />}>
                    <Link to="/reports">
                        Reports
                    </Link>
                </Menu.Item>

                <Menu.Item key="/settings" icon={<SettingOutlined />}>
                    <Link to="/account">
                        Settings
                    </Link>
                </Menu.Item>

                <Menu.Item key="/logout" icon={<LogoutOutlined />}>
                    {/* <Link to="/logout"> */}
                    <Popconfirm
                        placement="topLeft"
                        title="Do you want to Sign Out from the account?"
                        onConfirm={logoutConfirmHandler}
                        onCancel={() => message.error("Sign out aborted!")}
                        okText="Yes"
                        cancelText="No"
                    >
                        Logout
                    </Popconfirm>
                    {/* </Link> */}
                </Menu.Item>
            </Menu>
        </Sider>

    )
}
