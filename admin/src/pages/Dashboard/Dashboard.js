import React, { useState } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { Layout, Typography } from "antd";
import { CopyrightOutlined } from "@ant-design/icons";
import "antd/dist/antd.css";
import "./Dashboard.css";
import DashboardContent from "./DashboardContent/DashboardContent";
import Navigation from "./Navigation";
import NotFound from "../../components/NotFound/NotFound";
import Adminreporttable from "../Account/SAdmin/Adminreport/Adminreport";
import OrderManagement from "../Order-Management/OrderManagement";
import OrderItems from "../Order-Management/Order-Items/OrderItems";
import Account from "../Account/Account";
import Adminmanagement from "../Account/SAdmin/Adminmngmt"
import Admininfo from "../Account/SAdmin/Admininfo";
import Admintable from "../Account/SAdmin/Admintable";
import Brand from "../Brands/Brand";
import Category from "../Category/Category/Category";
import Signin from "../Login/Signin";
import BrandTable from "../Brands/BrandTable";
import CategoryTable from "../Category/Category/CategoryTable";
import SubCatTable from "../Category/SubCategory/SubCatTable";
import SubCategory from "../Category/SubCategory/SubCategory";
import UserTable from "../Client/UserTable";
import UserDetails from "../Client/UserDetails";
import ProductTable from "../Products/Product/ProductTable";
import Product from "../Products/Product/Product";
import ProductImages from "../Products/Product-images/ProductImages/ProductImages";
import AddProductImage from "../Products/Product-images/AddProductImages/AddProductImage";
import Updateinfo from "../Account/Updateinfo.js/Updateinfo";
import LoginSecurity from "../Account/SAdmin/passwordchange/Security";
import ChangePassword from "../Account/SAdmin/passwordchange/Passwordchange";
import TagTable from "../Tag/TagTable";
import Tag from "../Tag/Tag";
import Feedback from "../Account/Feedback/feedback";
import ProductSpecification from "../Products/Product-specification/ProductSpecification";
import Users from "../Reports/Users/Users";
import Admindetail from "../Account/SAdmin/Adminreport/Admindetail";
import AReport from "../Account/SAdmin/Adminreport/AReport";
const { Header, Footer } = Layout;
const { Title } = Typography;

export default function Dashboard() {
  //const params = useParams();
  // setting state for logged in
  const [loggedIn, setLoggedIn] = useState(() => {
    // if local storage has a state of isLoggedIn = true, this function will return true otherwise false
    const state = localStorage.getItem("isLoggedIn");
    if (state === "true") {
      return true;
    } else {
      return false;
    }
  });

  return (
    <>
      <Header style={{ paddingTop: "0.6rem" }}>
        <Title
          level={1}
          style={{
            color: "white",
            fontWeight: "bold",
            fontFamily: "Times New Roman",
          }}
        >
          Fathom
        </Title>
      </Header>
      {loggedIn ? (
        <Layout style={{ minHeight: "100vh" }}>
          {/* Navigation Component */}
          <Navigation loginStatus={setLoggedIn} />

          {/* Dashboard Content */}
          <Layout className="site-layout">
            <Switch>
              <Route exact path="/">
                <Redirect to="/home" />
              </Route>
              <Route exact path="/home">
                {/* <AlertCard /> */}
                <DashboardContent />
              </Route>
              <Route exact path="/users">
                <UserTable />
              </Route>
              <Route exact path="/users/user-details">
                <UserDetails />
              </Route>

              <Route exact path="/product">
                <ProductTable />
              </Route>
              <Route exact path="/add-product">
                <Product />
              </Route>
              <Route exact path="/update-product">
                <Product />
              </Route>

              <Route exact path="/product/product-images">
                <ProductImages />
              </Route>

              <Route exact path="/product/product-images/add-image">
                <AddProductImage />
              </Route>

              <Route exact path="/productspecs">
                <ProductSpecification />
              </Route>

              <Route exact path="/orders">
                <OrderManagement />
              </Route>

              <Route exact path={`/order/order-items`}>
                <OrderItems />
              </Route>
              <Route exact path="/brands">
                <BrandTable />
              </Route>

              <Route exact path="/add-brand">
                <Brand />
              </Route>

              <Route exact path="/update-brand">
                <Brand />
              </Route>

              {/* Routes for Tag */}
              <Route exact path="/tags">
                <TagTable />
              </Route>

              <Route exact path="/tags/add-tag">
                <Tag />
              </Route>

              <Route exact path="/tags/update-tag">
                <Tag />
              </Route>

              <Route exact path="/categories">
                <CategoryTable />
              </Route>

              <Route exact path="/add-category">
                <Category />
              </Route>

              <Route exact path="/update-category">
                <Category />
              </Route>

              <Route exact path="/sub-category">
                <SubCatTable />
              </Route>

              <Route exact path="/add-subCategory">
                <SubCategory />
              </Route>
              <Route exact path="/update-subCategory">
                <SubCategory />
              </Route>

              {/* Reports */}
              <Route path="/reports/users">
                <Users />
              </Route>

              <Route exact path="/account">
                <Account />
              </Route>
              <Route exact path="/account/admin">
                <Adminmanagement />
              </Route>

              <Route exact path="/addadmin">
                <Admininfo />
              </Route>
              <Route exact path="/updateadmin">
                <Admininfo />
              </Route>

              <Route exact path="/account/spersonalinfo">
                <Updateinfo />
              </Route>
              <Route exact path="/account/security">
                <LoginSecurity />
              </Route>
              <Route exact path="/passwordchange">
                <ChangePassword />
              </Route>
              <Route exact path="/account/adminreport">
                <AReport />
              </Route>

              <Route exact path={`/adminreport/admindetail`}>
                <Admindetail />
              </Route>
              <Route exact path={`/account/feedbacktable`}>
                <Feedback />
              </Route>
              
              {/* Route for 
              {/* Route for no page or path found */}
              <Route path="*">
                <NotFound />
              </Route>
            </Switch>
            <Footer style={{ textAlign: "center" }}>
              <h4>
                <CopyrightOutlined />
                2021 Made By Fathom Software Pvt. Ltd.
              </h4>
            </Footer>
          </Layout>
        </Layout>
      ) : (
        <Signin loginStatus={setLoggedIn} />
      )}
    </>
  );
}
