import React, { useState, useEffect } from 'react';
import { Layout, Card, Col, Row, Divider, Typography, } from "antd";
import { UserOutlined, TagsOutlined, ShoppingCartOutlined } from '@ant-design/icons';
import url from "../../../components/httpRequest/http";
import { Link } from "react-router-dom";
import { Pie } from "@ant-design/plots"
import "antd/dist/antd.css";
import "./DashboardContent.css";

const { Content, Header } = Layout;
const { Title } = Typography;

const chartData = data => {
    // data for pie chart
    // const newData = [
    //     {
    //         type: 'New Orders',
    //         value: 27,
    //     },
    //     {
    //         type: 'Dispatched Orders',
    //         value: 25,
    //     },
    //     {
    //         type: 'Delivered Orders',
    //         value: 18,
    //     },
    //     {
    //         type: 'Cancelled Orders',
    //         value: 15,
    //     },
    // ];
    return {
        appendPadding: 10,
        data: data,
        angleField: 'value',
        colorField: 'type',
        radius: 0.9,
        label: {
            type: 'inner',
            offset: '-30%',
            content: ({ percent }) => `${(percent * 100).toFixed(0)}%`,
            style: {
                fontSize: 14,
                textAlign: 'center',
            },
        },
        interactions: [
            {
                type: 'element-active',
            },
        ],
    };
}//end chartData

export default function DashboardContent() {

    // setting state for data
    const [data, setData] = useState(false);

    const getData = async () => {
        // getting data for users, brands and new orders
        const response = await fetch(url + "/dashboard");
        const result = await response.json();
        setData(result);
    }//end getData

    useEffect(() => {
      getData();
        return () => {
            setData(false);
        }
    }, [])
    return (
        <>
            <Content style={{ margin: '16px 16px' }}>
                <div className="dashboard-content4">
                    <Header style={{ paddingLeft: 1, backgroundColor: "transparent", fontSize: "x-large", fontWeight: "bolder" }}>Dashboard</Header>
                    <div className="site-card-wrapper">
                        <Row gutter={16}>
                            <Col span={8}>
                                <Card hoverable="true" style={{ backgroundColor: "rgb(0, 160, 235)" }} className="content-card4">
                                    <Row gutter={[16, 16]}>
                                        <Col>
                                            <h2>
                                                {data.users}
                                            </h2>
                                        </Col>
                                        <Col span={12}></Col>
                                        <Col>
                                            <UserOutlined style={{ fontSize: "50px" }} />

                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <h4>
                                                Total Users
                                            </h4>
                                        </Col>
                                    </Row>
                                </Card>
                            </Col>
                            <Col span={8}>
                                <Card hoverable="true" style={{ backgroundColor: "rgb(243, 240, 55)" }} className="content-card4">
                                    <Row gutter={[16, 16]}>
                                        <Col>
                                            <h2>
                                                {data.brands}
                                            </h2>
                                        </Col>
                                        <Col span={12}></Col>
                                        <Col>
                                            <TagsOutlined style={{ fontSize: "50px" }} />

                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col>
                                            <h4>
                                                Total Brands
                                            </h4>
                                        </Col>
                                    </Row>
                                </Card>
                            </Col>
                            <Col span={8}>
                                <Link to="/orders">
                                    <Card hoverable="true" style={{ backgroundColor: "rgb(34, 202, 34)" }} className="content-card4">
                                        <Row gutter={[16, 16]}>
                                            <Col>
                                                <h2>
                                                    {data.orders}
                                                </h2>
                                            </Col>
                                            <Col span={12}></Col>
                                            <Col>
                                                <ShoppingCartOutlined style={{ fontSize: "50px" }} />

                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col>
                                                <h4>
                                                    New Orders
                                                </h4>
                                            </Col>
                                        </Row>
                                    </Card>
                                </Link>
                            </Col>
                        </Row>
                    </div>
                    <Divider />

                    <div>
                        <Title level={3} style={{ fontWeight: "bold" }}>
                            Charts
                        </Title>
                        <Divider />
                        <Row>
                            <Col span={10} offset={1}>
                                <Title level={4} style={{ fontWeight: "bold" }}>Orders</Title>
                                <Pie {...chartData([
                                    {
                                        type:"Booked Orders",
                                        value: data.booked
                                    },
                                    {
                                        type:"Dispatched Orders",
                                        value: data.dispatched
                                    },
                                    {
                                        type:"Delivered Orders",
                                        value: data.delivered
                                    },
                                    {
                                        type:"Cancelled Orders",
                                        value: data.cancelled
                                    }
                                ])} />
                            </Col>
                            <Col span={10} offset={1}>
                                <Title level={4} style={{ fontWeight: "bold" }}>Customers</Title>
                                <Pie {...chartData([
                                    {
                                        type:"Active Users",
                                        value:data.activeUsers
                                    },
                                    {
                                        type:"Inactive Users",
                                        value:data.inactiveUsers
                                    }
                                ])} />
                            </Col>
                        </Row>
                        {/* <Row>
                            <Col span={10} offset={1}>
                                <Title level={4} style={{ fontWeight: "bold" }}>Products</Title>
                                <Pie {...config} />
                            </Col>
                            <Col span={10} offset={1}>
                                <Title level={4} style={{ fontWeight: "bold" }}>Brands</Title>
                                <Pie {...config} />
                            </Col>
                        </Row> */}
                    </div>
                </div>
                <Divider type="horizontal" />
            </Content>
        </>
    )
}
