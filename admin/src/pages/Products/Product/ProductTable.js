import React, { useState, useEffect } from 'react'
import { Table, Button, Typography, Tag, Spin, message, Tooltip } from 'antd';
import { Link } from 'react-router-dom';
import "./ProductTablecss.css"
import url from '../../../components/httpRequest/http';
import TableButton from '../../../components/TableButton/TableButton';
import { EditOutlined, DeleteOutlined, FileTextOutlined, PictureOutlined } from "@ant-design/icons";


export default function ProductTable() {
  const { Title } = Typography;

  const [data, setData] = useState(false);
  const [isLoding, setIsLoading] = useState(false);

  const getData = async () => {
    const resp = await fetch(url + '/product-table');
    const result = await resp.json();
    setData(result);
  }
  useEffect(() => {
    getData();
  }, [data])


  // function for deleting the current products in table which is inactive
  async function confirmHandler(obj, e) {

    const response = await fetch(url + `/product-delete?id=${obj.id}`, { "method": "DELETE" });
    const msg = await response.json();
    console.log(e);

    message.success(msg.textMsg);
    setData(false);
  }


  function cancel(e) {
    console.log(e);
    message.error('Action aborted.');
  }

  const columns = [

    {
      title: 'Category',
      dataIndex: 'category',
      key: 'category',
    },
    {
      title: 'Sub Category',
      dataIndex: 'subcategory',
      key: 'subcategory',
    },
    {
      title: 'BrandName',
      dataIndex: 'brand',
      key: 'brand',
    },
    {
      title: 'Product Name',
      dataIndex: 'name',
      key: 'name',
    },

    {
      title: 'Model',
      dataIndex: 'model',
      key: 'model',
    },
    {
      title: 'Stock',
      dataIndex: 'currentStock',
      key: 'currentStock',
    },
    {
      title: 'Barcode',
      dataIndex: 'barcode',
      key: 'barcode',
    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
    },
    {
      title: 'Tag',
      dataIndex: 'tagName',
      key: 'tagName',
    },
    {
      title: 'Status',
      dataIndex: 'isActive',
      key: 'isActive',
      render: status => {
        let color = "";
        if (status === 1) {
          status = "ACTIVE";
          color = "green";
        }
        else {
          status = "INACTIVE";
          color = "volcano";
        }
        return (
          <Tag color={color} key={status}>
            {status.toUpperCase()}
          </Tag>
        )
      }
    },

    {
      title: "Actions",
      key: "key",
      dataIndex: "action",
      render: (id, record) => {
        return <>
          <Tooltip title="Edit" color="green" key="edit">
            <Button type='secondary' className="button13">
              <Link to={`/update-product?id=${record.id}`}>
                <EditOutlined />
              </Link>
            </Button>
          </Tooltip>

          <Tooltip title="Specification" color="green" key="specification">
            <Button className="button13" type="secondary"
              style={{
                marginTop: '0 8px',
              }}

            >
              <Link to={`/productspecs/${record.id}`}>
                <FileTextOutlined />
              </Link>
            </Button>
          </Tooltip>

          <Tooltip title="Images" color="green" key="images">
            <Button className="button13" type="secondary"
              style={{
                marginTop: '0 8px',
              }}

            >
              <Link to={`/product/product-images?id=${record.id}`}>
                <PictureOutlined />
              </Link>
            </Button>
          </Tooltip>


          <TableButton
            placement="top"
            color="red"
            tooltipTitle="Delete"
            type="danger"
            id={record.id}
            className="button13"
            title="Do you want to delete this product?"
            buttonText={<DeleteOutlined />}
            onConfirm={confirmHandler}
            onCancel={cancel}
          />

        </>
      }
    }

  ];

  return (
    <div className="container11">


      <Title level={2} style={{ textAlign: "left" }}>Product Table</Title>
      {
        isLoding ?
          <Spin style={{ marginLeft: "550px", marginTop: "50px", marginBottom: "50px" }} tip="Loading Data..." />
          :
          <>
            <div style={{ display: "flex" }}>
              <Button type="default" htmlType="submit" style={{ marginLeft: "auto" }}>
                <Link to="/add-product">Add product </Link>
              </Button>
            </div>
            <br />
            <Table columns={columns} dataSource={data} />
            <br />
          </>
      }

    </div>
  )
}
