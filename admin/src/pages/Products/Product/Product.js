import React, { useState, useEffect } from 'react';
import { Form, Button, Card, Select, notification } from 'antd';
import "antd/dist/antd.css";
import "./Formcss.css"
import { Header } from 'antd/lib/layout/layout';
import url from '../../../components/httpRequest/http';
// import TextAreaCom from '../../../components/Textarea/TextAreaCom';
import TextAreaCom from '../../../components/TextAreaCom/TextAreaCom';
import InputField from '../../../components/InputField/InputField';
import SelectComponent from '../../../components/SelectComponent/SelectComponent';
import { useHistory, useLocation } from 'react-router-dom';

const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};

const Product = () => {
    // seting state for spinner
    const [spin, setSpin] = useState(false);

    const [data, setData] = useState({
        id: [],
        Name: "",
        Category: "",
        CategoryCB: [],
        BrandName: "",
        BrandCB: [],
        SubCategoryName: "",
        SubCategoryCB: [],
        discription: "",
        status: "",
        model: "",
        Stock: "",
        barcode: "",
        price: "",
        tag: "",
        TagCB: [],
        // categoryId: -1,
        userId: localStorage.getItem("userId")
    });


    const history = useHistory();
    const location = useLocation();

    const params = new URLSearchParams(location.search);
    const paramId = params.get("id");


    const getAllData = async (id) => {

        const response = await fetch(url + `/get-product${id ? `?id=${id}` : ''}`);
        const rows = await response.json();
        // console.log(rows)

        if (id) {

            // if id exists, then this record is for updation. So, setting form field values

            setData({
                ...data,
                CategoryCB: rows[0],
                SubCategoryCB: rows[1],
                BrandCB: rows[2],
                TagCB: rows[3],
                categoryId: rows[4][0].categoryId,
                ...rows[4][0]
            });
            console.log(rows[4][0])
            form.setFieldsValue({ ...rows[4][0] })
            setSpin(false);
        }
        else {
            // if id does not exists, this reocrd is to insert.

            setData({
                ...data,
                CategoryCB: rows[0],
                SubCategoryCB: rows[1],
                BrandCB: rows[2],
                TagCB: rows[3],
            });
            // console.log(rows)
            setSpin(false);
        }

    }

    useEffect(() => {
        if (paramId) {
            getAllData(paramId);
        }
        else {
            getAllData()
        }


        // return () => {
        //     // Component unmount function
        //     setData({
        //         Name: "",
        //         model: "",
        //         barcode: "",
        //         Stock: "",
        //         price: ""
        //     })
        // }

    }, [paramId]);
    const onChangeHandler = (name, e) => {
        setData({
            ...data,
            [name]: e.target.value,
        });
    }//end on change handler

    // notification for confirmation
    const openNotificationWithIcon = (msg) => {
        notification.success({
            message: "Success!",
            description: msg
        });
    };



    const [form] = Form.useForm()

    const formItemLayout = {
        labelCol: {
            xs: {
                span: 24,
            },

            sm: {
                span: 8,
                wrapperCol: {
                    xs: {
                        // span: 24,
                    },
                    sm: {
                        span: 16,
                    },

                },
            },


            wrapperCol: {
                xs: {
                    span: 24,

                    labelCol: {
                        xs: {
                            span: 24,
                        },
                        sm: {
                            span: 8,
                        },

                    },
                    wrapperCol: {
                        xs: {
                            span: 24,
                        },
                        sm: {
                            span: 16,
                        },
                    },
                }
            }
        }
    }



    const clickHandler = async (e) => {
        if (params.get("id")) {
            const dataObj = {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
                body: JSON.stringify({ ...data, id: params.get("id") })
            }
            const response = await fetch(url + "/update-product", dataObj);
            const result = await response.text();
            if (result) {
                openNotificationWithIcon("Record update successsfully..");
                history.goBack();
                console.log(result);
            }
            else {
                openNotificationWithIcon("Something went wrong try again");
            }
        }
        else {
            console.log(data);
            const obj = {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"

                },
                body: JSON.stringify(data)
            }
            const response = await fetch(url + "/add-product", obj);
            const result = await response.json();
            if (result.status === true) {
                notification.success({
                    message: "Success!",
                    description: "Product added successfully."
                });
                history.goBack();
            }
        }
    }

    return (

        <Form {...layout} form={form} onFinish={clickHandler} className="form1" >
            {
                <Card hoverable="true" className="card19">
                    <Header style={{ marginLeft: "270px", marginTop: "10px", textAlign: 'center', textDecoration: "bold", backgroundColor: "transparent", width: "480px", fontSize: "x-large", fontWeight: "bolder", paddingBottom: "80px" }}><h2>{!paramId ? "Add Product" : "Update Product"}</h2></Header>

                    <table className="table11">
                        <tbody className="tbody1"><tr><td>
                            <InputField label="Name" onChange={onChangeHandler} style={{ width: "335", marginLeft: "6px" }} name="Name" message="Please enter name..." placeholder="Enter name here" value={data.name ? data.name : ""} />
                        </td><td>

                                < InputField label="Model" onChange={onChangeHandler} style={{ width: "335px", marginLeft: "6px" }} name="model" message="Please enter model..." placeholder="Enter model here" value={data.model ? data.model : ""} />


                            </td></tr><tr><td>
                                <Form.Item
                                    name="Category"
                                    label="Category"
                                    rules={[
                                        {
                                            required: data.categoryId ? false : true,
                                            message: 'Please select category!',
                                        },
                                    ]}
                                >
                                    <Select name="Category" style={{ width: "335px", marginLeft: "6px" }} placeholder="select your Category" defaultValue={data.categoryId} key={data.categoryId} onChange={(value) => { setData({ ...data, "Category": value }) }}>
                                        {
                                            data.CategoryCB.map(record => {
                                                return <option key={record.id} value={record.id}>{record.name}</option>
                                            })
                                        }
                                    </Select>
                                </Form.Item>
                            </td>
                                <td>
                                    < InputField label="Stock" onChange={onChangeHandler} style={{ width: "335px", marginLeft: "6px" }} name="Stock" message="Please enter Stock..." placeholder="Enter Stock here" value={data.currentstock ? data.currentstock : ""} />

                                </td></tr>
                            <tr>
                                <td>
                                    <Form.Item
                                        name="SubCategoryName"
                                        label="Sub Category" style={{ width: "500px", marginLeft: "10px" }}
                                        rules={[
                                            {
                                                required: data.subCategoryId ? false : true,
                                                message: 'Please select SubCategory!',
                                            },
                                        ]}
                                    >

                                        <Select name="SubCategoryName"
                                            placeholder="select your SubCategory"
                                            defaultValue={data.subCategoryId}
                                            key={data.subCategoryId}
                                            onChange={(value) => { setData({ ...data, "SubCategoryName": value }) }}>
                                            {
                                                data.SubCategoryCB.map(record => {
                                                    return <option key={record.id} value={record.id}>{record.name}</option>
                                                })
                                            }
                                        </Select>
                                    </Form.Item>
                                </td> <td>
                                    < InputField label="Barcode" onChange={onChangeHandler} style={{ width: "335px", marginLeft: "6px" }} name="barcode" message="Please enter barcode..." placeholder="Enter barcode here" value={data.barcode ? data.barcode : ""} />


                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <Form.Item
                                        name="tag"
                                        label="Tag" style={{ width: "500px", marginLeft: "6px" }}
                                        rules={[
                                            {
                                                required: data.tagId ? false : true,
                                                message: 'Please select tag!',
                                            },
                                        ]}
                                    >

                                        <Select name="tag"
                                            placeholder="select tag"
                                            defaultValue={parseInt(data.tagId) || null}
                                            key={parseInt(data.tagId)}
                                            onChange={(value) => { setData({ ...data, "SubCategoryName": value }) }}>
                                            {
                                                data.TagCB.map(record => {
                                                    return <option key={record.id} value={record.id}>{record.name}</option>
                                                })
                                            }
                                        </Select>
                                    </Form.Item>

                                </td>
                                <td>
                                    < InputField label="Price" onChange={onChangeHandler} style={{ width: "335px", marginLeft: "6px" }} name="price" message="Please enter price..." placeholder="Enter price here" value={data.prise ? data.prise : ""} />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <Form.Item
                                        name="BrandName"
                                        label="Brand" style={{ width: "500px", marginLeft: "6px" }}
                                        rules={[
                                            {
                                                required: data.brandId ? false : true,
                                                message: 'Please select brand!',
                                            },
                                        ]}
                                    >
                                        <Select name="BrandName" placeholder="select your brand" key={data.brandId} defaultValue={data.brandId} onChange={(value) => { setData({ ...data, "BrandName": value }) }}>
                                            {
                                                data.BrandCB.map(record => {
                                                    return <option key={record.id} value={record.id}>{record.name}</option>
                                                })
                                            }
                                        </Select>
                                    </Form.Item>
                                </td>
                                <td>
                                    < SelectComponent label="Status" name="status" message="Please select status..." onChange={(value) => { setData({ ...data, status: value }) }} value={data.status ? (data.status) : ""} />

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    < TextAreaCom
                                        name="discription"
                                        label="Discription"
                                        message="Please enter discription"
                                        placeholder="Enter discription here..."
                                        onChange={onChangeHandler} />

                                </td>




                                <td>
                                    <Button className='btn19' type="primary" htmlType="submit" style={{ margin: '50 0px' }}>
                                        {!paramId ? "Add" : "Update"}
                                    </Button>

                                    <Button className='btn19' type="primary"
                                        style={{
                                            margin: '0 8px',
                                        }}
                                        // onClick={() => {
                                        //     form.resetFields();
                                        // }}
                                        onClick={() => history.goBack()}
                                    >
                                        Back
                                    </Button>
                                </td> </tr>
                        </tbody></table>
                </Card>
            }
        </Form>
    );
};



export default Product;