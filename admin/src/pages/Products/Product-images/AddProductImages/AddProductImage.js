import React, { useState, useEffect } from 'react';
import { Typography, Divider, Row, Col, Form, Button, notification, Upload, Image, Spin } from "antd";
import "antd/dist/antd.css";
import "./AddProductImage.css";
import { useLocation, useHistory } from "react-router-dom";
import url from '../../../../components/httpRequest/http';
import { PlusCircleOutlined } from "@ant-design/icons";


const { Title } = Typography;

export default function AddProductImage() {

    // creating location object
    const location = useLocation();
    const searchParams = new URLSearchParams(location.search);

    const [form] = Form.useForm();

    // setting state for spinner
    const [spin, setSpin] = useState(true);


    // setting state for data
    const [data, setData] = useState({
        imgId: searchParams.get("id") || "",
        productId: searchParams.get("productId"),
        name: "",
        createdBy: localStorage.getItem("userId"),
        status: 1
    });

    // creating array for files
    const files = [];


    // creating history object
    const history = useHistory();

    // getting admin or super admin data from backend
    const getData = async () => {
        const response = await fetch(url + `/product/product-images?id=${searchParams.get("productId")}`);
        const result = await response.json();
        setData({ ...data, name: result.productDetails.name });
        console.log(result);
    }

    // calling getData in useEffect
    useEffect(() => {
        getData();
    }, [])


    // showing notification
    const showNotification = (type, msg) => {
        if (type === "success") {
            notification.success({
                message: "Success!",
                description: msg
            });
        }
        else if (type === "error") {
            notification.error({
                message: "Error",
                description: msg
            });
        }
    }

    // creating an event to load files in the list
    const onActionHandler = file => {
        files.push(file);
        console.log(files);
    }//end onActionHandler

    // removing a file from state
    const onRemoveHandler = file => {

        for (let i = 0; i < files.length; i++) {
            if (files[i].uid === file.uid)  // ==> Ckecking if the uid of both files matches
            {
                files.splice(i, 1);  // ==> if uid matches, the file will be removed from array using array.splice(index,filesCount) function
                break;
            }
        }
        console.log(files);
    }//end onRemoveHandler

    // listning onFinish event
    const onFinishHandler = async () => {
        setSpin(true);
        // console.log(data.files);
        const formData = new FormData();

        // apppending all files in formdata
        for (let i = 0; i < files.length; i++) {
            formData.append(`${i}`, files[i]);
        }

        // formData.append('files',files);//appending all files
        formData.append("body", JSON.stringify(data));//appending all data state

        const response = await fetch(url + "/products/product-images/add-product-image", { method: "POST", body: formData });

        const result = await response.json();
        console.log(result)
        if (result.status === true) {
            showNotification("success", "Image Inserted Successfully!...");
            history.goBack();
        }
        else {
            showNotification("error", "Something went wrong")
        }
    }

    const onPreview = async file => {
        let src = file.url;
        if (!src) {
            src = await new Promise(resolve => {
                const reader = new FileReader();
                reader.readAsDataURL(file.originFileObj);
                reader.onload = () => resolve(reader.result);
            });
        }
        const image = new Image();
        image.src = src;
        const imgWindow = window.open(src);
        imgWindow.document.write(image.outerHTML);
    };

    return (
        <div className='container41'>
            <div className="header4">
                <Title>
                    {
                        !searchParams.get("id") ? "Add Images" : "Update Image"
                    }
                </Title>
            </div>
            <Divider />

            <Row gutter={[20, 20]}>
                <Col>
                    <h3>Product Name:</h3>
                </Col>
                <Col>
                    <h3>{data.name}</h3>
                </Col>
            </Row>
            {/* <div className="container42"> */}
            <Form onFinish={onFinishHandler} form={form}>


                <div className="container42" style={{ overflowY: "scroll" }}>

                    {/* <input type="file" accept='image/*' name="images" onChange={e => setFileList({"file":e.target.files[0]})}/> */}
                    <Upload
                        listType="picture-card"
                        action={onActionHandler}//action returns a file which will be sent to the backend
                        onPreview={onPreview}
                        onRemove={onRemoveHandler}
                        accept="image/*"
                        multiple={true}
                    >
                        <PlusCircleOutlined style={{fontSize:"50px"}}/>
                    </Upload>
                </div>
                <div style={{ textAlign: "center" }}>
                    
                    <Button disabled={spin === false && true} type="primary" htmlType='submit' className='button41' style={{ marginLeft: 30 }}>SAVE</Button>
                    
                    <Button type="primary" className='button41' style={{ marginLeft: 10 }} onClick={() => history.goBack()}>BACK</Button>
                </div>

            </Form>

        </div>
        // </div>
    )
}
