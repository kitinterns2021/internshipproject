import React, { useState, useEffect } from 'react';
import "./ProductImages.css"
import { Typography, Card, Row, Col, Button, message } from "antd";
import "antd/dist/antd.css";
import { PlusCircleOutlined, BackwardOutlined, TableOutlined, PictureOutlined } from '@ant-design/icons';
import ImageCard from '../../../../components/ImageCard/ImageCard';
import url from "../../../../components/httpRequest/http";
import { Link, useLocation } from 'react-router-dom';
import ImageTable from './ImageTable';

const { Title } = Typography;
export default function ProductImages() {

    // setting state for images
    const [data, setData] = useState([]);

    // setting state for product details
    const [productSepcs, setProductSpecs] = useState({});

    // setting state to show data in row or in table
    const [showImages, setShowImages] = useState(true);

    // getting location object
    const location = useLocation();

    // getting query parameters from location object
    const params = new URLSearchParams(location.search);

    // getting all images
    const getImages = async (id) => {
        const response = await fetch(url + `/product/product-images?id=${id}`);
        const result = await response.json();
        // console.log(result)
        setProductSpecs(result.productDetails);
        setData(result.productImages);
    }//end getImages

    useEffect(() => {
        if (params.get("id")) {
            getImages(params.get("id"));
        }
    }, [data]);

    // deleting the record
    const confirmDeleteHandler = async (id, imageUrl) => {
        const response = await fetch(url + `/products/product-images/delete-product-image?id=${id}&imageUrl=${imageUrl}`, { method: "DELETE" });
        const result = await response.text();
        if (result === "OK") {
            message.success("Record deleted successfully!");
        }
    }

    return (
        <div className='container1'>
            <Title>Product Images</Title>

            {/* setting card for product specification */}
            <Card className='card1'>
                <Title level={2}>Product Details</Title>
                <Row>
                    <Col className='col42'>
                        <Row>
                            <Col className='col41'><h3>Product Name:</h3></Col>
                            <Col className='col41'><h3>{productSepcs.name}</h3></Col>
                        </Row>
                        <Row  >
                            <Col className='col41'><h3>Brand:</h3></Col>
                            <Col className='col41'><h3>{productSepcs.brand}</h3></Col>
                        </Row>
                    </Col>
                    <Col >
                        <Row>
                            <Col className='col41'><h3>Category:</h3></Col>
                            <Col className='col42'><h3>{productSepcs.category}</h3></Col>
                        </Row>
                        <Row>
                            <Col className='col41'><h3>Sub Category:</h3></Col>
                            <Col className='col42'><h3>{productSepcs.subCategory}</h3></Col>
                        </Row>
                    </Col>
                </Row>
                <Button type='primary' style={{ width: "200px", marginTop: "30px" }}><Link to="/product"><BackwardOutlined />Back</Link></Button>
            </Card>


            {/* rendering product images */}
            <Card className='card1'>
                <Row>
                    <Col>
                        <Title level={2}>Available Product Images</Title>
                    </Col>
                    <Col style={{ marginLeft: "auto" }}>
                        <Row gutter={[0, 16]}>
                            <Col>
                                <Button type='secondary' onClick={() => setShowImages(true)}>
                                    <PictureOutlined />
                                </Button>
                            </Col>
                            <Col>
                                <Button type='secondary' onClick={() => setShowImages(false)}>
                                    <TableOutlined />
                                </Button>
                            </Col>
                        </Row>
                    </Col>
                </Row>
                {

                        showImages ?
                        <Row gutter={[16, 24]} style={{ marginLeft: "85px", marginTop: "40px" }}>
                            {
                                data && data.map(element => {
                                    return <ImageCard key={element.id} imgId={element.id} name={element.imageURL} seqId={element.imageSequence} status={element.isActive === 1 ? "ACTIVE" : "INACTIVE"} src={url + `/images/${element.imageURL}`} onConfirm={confirmDeleteHandler} />
                                })
                            }
                            <Col span={8}>
                                <Link to={`/product/product-images/add-image?productId=${params.get("id")}`}>
                                    <Card
                                        key="2"
                                        hoverable
                                        style={{ width: 300, height: 500, padding: "8rem 1.5rem 1rem 1rem", backgroundColor: "whitesmoke" }}
                                        cover={<PlusCircleOutlined className='addImg' />}
                                    >
                                        <Title level={5} style={{ marginTop: "8rem", marginLeft: "4rem" }}>Add Image</Title>
                                    </Card>
                                </Link>
                            </Col>
                        </Row>

                        :
                        <ImageTable data={data} id = {params.get("id")} onConfirm={confirmDeleteHandler} />

                }
            </Card>
        </div>
    )
}
