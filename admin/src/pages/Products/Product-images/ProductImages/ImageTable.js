import React from 'react';
import { Table, Tag, Button, message, Image } from "antd";
import "antd/dist/antd.css";
import { Link } from "react-router-dom";
import TableButton from '../../../../components/TableButton/TableButton';
import "./ProductImages.css";
import url from '../../../../components/httpRequest/http';

export default function ImageTable(props) {
    // handling switch click event to change the status of the product image
    const onClickHandler = async (id,status) => {
console.log(id+","+status)
        // creating object to send with put request
        const obj = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify({
                status: !status,
                imgId: id,
                userId: localStorage.getItem("userId")
            })
        };

        const response = await fetch(url + `/products/product-images/edit-product-image`, obj);
        const result = await response.json();
        if (result.status === true) {
            message.success("Status Updated.")
        }
        else {
            message.error("Something went wrong. Please try again.");
        }
    }//end onClickHandler

    const columns = [
        {
            title: 'Image',
            dataIndex: 'imageURL',
            key: 'imageURL',
            render: (element) => {
                return <Image
                    src={url + `/images/${element}`}
                    style={{ widht: "100px", height: "50px", marginLeft: "25px" }}
                />
            }
        },
        {
            title: 'Updated By',
            dataIndex: 'updatedBy',
            key: 'updatedBy',
        },
        {
            title: 'Created Date',
            dataIndex: 'createdDttm',
            key: 'createdDttm',
        },
        {
            title: 'Last Modified',
            dataIndex: 'updatedDttm',
            key: 'updatedDttm',
        },
        {
            title: 'Status',
            dataIndex: 'isActive',
            key: 'isActive',
            render: status => {
                let color = "";
                switch (status) {
                    case 1:
                        status = "ACTIVE";
                        color = "green";
                        break;
                    case 0:
                        status = "INACTIVE";
                        color = "volcano";
                        break;

                    default:
                        break;
                }
                return <Tag color={color}>{status}</Tag>
            }
        },
        {
            title: "Actions",
            key: "key",
            dataIndex: "action",
            render: (text, record) => {
                return <>
                    {/* <Button type='primary' className='button4' onClick={() => onClickHandler(record.id,record.isActive)}>
                            Edit
                    </Button> */}
                    <TableButton
                            type="primary"
                            id={record.id}
                            className="button4"
                            title="Do you want to edit status of this image?"
                            buttonText="Edit"
                            onConfirm={() => onClickHandler(record.id,record.isActive)}
                            onCancel={() => message.error("Action Aborted!")}
                        />

                    {/* Delete button will only for those records which are in-active */}
                    {
                        // record.isActive === 0 &&
                        <TableButton
                            type="danger"
                            id={record.id}
                            className="button4"
                            title="Do you want to delete this image?"
                            buttonText="Delete"
                            onConfirm={() => props.onConfirm(record.id)}
                            onCancel={() => message.error("Action Aborted!")}
                        />
                    }
                </>
            }
        }

    ];

    return (
        <div>
            <div style={{ display: "flex" }}>
                <Button type="default" htmlType="submit" style={{ marginLeft: "auto", marginBottom: 20 }}>
                    <Link to={`/product/product-images/add-image?productId=${props.id}`}>Add Image</Link>
                </Button>
            </div>
            <Table className="table12"
                columns={columns}
                dataSource={props.data}
                bordered

            />

        </div>
    )
}
