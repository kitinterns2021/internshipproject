import { Table, Tag, message, Button, Typography, Divider } from 'antd';
import React, { useState, useEffect } from 'react';
import { Link, useHistory, useParams } from "react-router-dom";
import "./ProductSpecificationTable.css";

// importing url
import url from '../../../components/httpRequest/http';
import TableButton from '../../../components/TableButton/TableButton';

const { Title } = Typography;

export default function ProductSpecificationTable() {

    // history object
    const history = useHistory();

    // gettting params
    const { productId } = useParams();

    // state for orders data
    const [data, setData] = useState({
        productName: "",
        allSpecifications: []
    });

    // getting orders data from backend
    const getData = async () => {
        const response = await fetch(url + `/product-specification?productId=${productId}`);
        const rows = await response.json();
        setData(rows);
    }

    useEffect(
        () => {
            getData();
        }
    )

    // function for deleting the record
    async function confirmHandler(obj, e) {
        const dataObj = {
            method: "DELETE",
        }
        // updating the order
        const response = await fetch(url + `/product-specification/delete-specification?specId=${obj.id}&userId=${localStorage.getItem("userId")}`, dataObj);
        const result = await response.json();
        console.log(e);
        if (result.success === true) {
            message.success("Record Deleted Successfully!");
            setData({
                productName: "",
                allSpecifications: []
            });
        }
    }


    function cancel(e) {
        console.log(e);
        message.error('Action aborted.');
    }

    //Defining all required columns of the table
    const columns = [
        {
            title: 'Key',
            dataIndex: 'specKey',
            key: 'specKey',
        },
        {
            title: "Value",
            dataIndex: "value",
            key: "value",
        },
        {
            title: "Created By",
            dataIndex: "createdBy",
            key: "createdBy",
        },
        {
            title: "Date",
            dataIndex: "createdDttm",
            key: "createdDttm",
        },
        {
            title: "Last Modified",
            dataIndex: "updatedDttm",
            key: "updatedDttm",
        },
        {
            title: 'Status',
            key: 'status',
            dataIndex: 'status',
            render: Status => {
                let color = "";
                switch (Status) {
                    case 1:
                        Status = "ACTIVE"
                        color = "green";
                        break;
                    case 0:
                        Status = "INACTIVE"
                        color = "volcano";
                        break;
                    default:
                        break;
                }
                return (
                    <Tag color={color} key={Status}>
                        {Status}
                    </Tag>
                );
            }
        },
        {
            title: "Date",
            key: "createdDttm",
            dataIndex: "createdDttm"
        },
        {
            title: "Last Modified",
            key: "updatedDttm",
            dataIndex: "updatedDttm"
        },
        {
            title: "Actions",
            key: "key",
            dataIndex: "action",
            render: (text, record) => {
                return <>

                    <Link to={`/productspecs/edit-specification/${productId}/${record.id}`}>
                        <Button type='primary' className='button4jdh82'>
                            Edit
                        </Button>
                    </Link>

                    <TableButton
                        placement="top"
                        color="red"
                        tooltipTitle="Delete"
                        type="danger"
                        id={record.id}
                        className="button4jdh82"
                        title="Confirm deleting this record?"
                        buttonText="Delete"
                        onConfirm={confirmHandler}
                        onCancel={cancel}
                    />
                </>
            }
        }
    ];

    return (
        <div className='container4asdfj37r8'>
            <Title level={2}>
                Product Specification Table
            </Title>
            <Divider />

            <Title level={3}>
                Product - {data.productName}
            </Title>

            <div style={{ display: "flex" }}>
                <Button type="default" htmlType="submit" style={{ marginLeft: "auto" }}>
                    <Link to={`/productspecs/add-specification/${productId}`}>Add Specification </Link>
                </Button>
            </div>

            {data &&
                <p>Showing 1-5 out of 100 results</p> &&
                <Table columns={columns} dataSource={data.allSpecifications} />
            }

            <Button type='primary' className='button4jdh82' onClick={() => history.goBack()}>
                Back
            </Button>
        </div>
    )
}
