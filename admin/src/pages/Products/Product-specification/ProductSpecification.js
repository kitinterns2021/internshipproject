import React, { useState, useEffect } from 'react';
import { Form, Button, Card, Radio, notification } from 'antd';
import "antd/dist/antd.css";
import "./ProductSpecscss.css"
import { Header } from 'antd/lib/layout/layout';
import InputField from '../../../components/InputField/InputField';
import { useHistory, useParams } from 'react-router-dom';
import url from '../../../components/httpRequest/http';

const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};



const ProductSpecification = () => {

    const history = useHistory();
    // const location = useLocation();

    // const params = new URLSearchParams(location.search);

    const { productId, specId } = useParams();

    const [data, setData] = useState({
        productId: productId,
        productName: "",
        specKey: "",
        value: "",
        createdBy: localStorage.getItem("userId"),
        status: 1
    })

    const [form] = Form.useForm()



    const getData = async () => {

        const response = await fetch(url + `/product-specification/get-product?productId=${productId}${specId ? `&specId=${specId}` : ''}`);
        const result = await response.json();
        setData(
            {
                ...data,
                ...result
            }
        );
        form.setFieldsValue({ productName: result.name, specKey: result.specKey, value: result.value, status: result.isActive })

    }

    useEffect(() => {
        getData();
    }, [])



    const formItemLayout = {

        labelCol: {
            xs: {
                span: 8,
            },
            sm: {
                span: 8,
            },
        },
        wrapperCol: {
            xs: {
                span: 14,
            },
            sm: {
                span: 10,
            },
        },
    };

    const onChangeHandler = (name, e) => {
        setData({
            ...data,
            [name]: e.target.value

        })
    }
    const onFinish = async (e) => {
        console.log(data);
        if (specId) {
            //if specId exists, then the record is to update
            const obj = {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
    
                },
                body: JSON.stringify({...data,specId})
            }
            const response = await fetch(url + "/productspecs/update-specification", obj);
            const result = await response.json();
            if (result.status === true) {
                notification.success({
                    message: "Success!",
                    description: "Specification Updated!"
                });
            }
            else {
                notification.error({
                    message: "Error!",
                    description: "Something went wrong. Please try again!"
                });
                
            }
            history.goBack();

        }
        else {
            //if specId does not exists, then the record is to add
            const obj = {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"

                },
                body: JSON.stringify({...data})
            }
            const response = await fetch(url + "/productspecs", obj);
            const result = await response.json();
            if (result.status === true) {
                notification.success({
                    message: "Success!",
                    description: "Specification Added!"
                });
            }
            else {
                notification.error({
                    message: "Error!",
                    description: "Something went wrong. Please try again!"
                });
            }
        }
    }


    return (
        <Form {...layout} form={form} onFinish={onFinish} className="form12" >

            <Card hoverable="true" className="card18">
                <Header style={{ marginLeft: "90px", marginTop: "10px", textAlign: 'center', textDecoration: "bold", backgroundColor: "transparent", width: "480px", fontSize: "x-large", fontWeight: "bolder", paddingBottom: "80px" }}><h2>Product Specification</h2></Header>



                <InputField label="Product Name" name="productName" readOnly={true} style={{ width: "400px", marginLeft: "6px" }} value={data.productName} />
                <InputField label="Key" required="true" style={{ width: "400px", marginLeft: "6px" }} onChange={onChangeHandler} name="specKey" message="Please enter product key..." placeholder="Enter pruduct key here" value={data.key ? data.key : ""} />
                <InputField label="Value" required="true" style={{ width: "400px", marginLeft: "6px" }} onChange={onChangeHandler} name="value" message="Please enter product value..." placeholder="Enter pruduct value here" value={data.value ? data.value : ""} />

                <Form.Item name="status" label="Status">
                    <Radio.Group defaultValue={data.status} onChange={(e) => onChangeHandler("status", e)}>
                        <Radio value={1}>Active</Radio>
                        <Radio value={0}>Inactive</Radio>

                    </Radio.Group>
                </Form.Item>
                <Button className='btn14' type="primary" htmlType="submit" style={{ margin: '50 0px' }}>
                    {
                        specId ? "Update" : "Save"
                    }
                </Button>
                <Button className='btn14' type="primary"
                    style={{
                        margin: '0 8px',
                    }}
                    onClick={() => history.goBack()}
                >
                    Back
                </Button>
            </Card>
        </Form>
    )
}
export default ProductSpecification;