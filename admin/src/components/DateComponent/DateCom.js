import { Form, DatePicker } from 'antd'
import moment from "moment"


export default function DateCom(props) {
    const dateFormat = 'DD-MM-YYYY';
    return (
        <Form.Item
            name={props.name}
            label="Date Of Birth"
            rules={[
                {
                    required: true,
                    message: 'Please input your birth date!',
                },
            ]}
        >
            <DatePicker
                defaultValue={moment(props.date, dateFormat)}
                format={dateFormat}
                style={
                    { width: "50%" }
                }
                onChange={date=>props.onChange(date)}
            />
        </Form.Item>
    )
}
