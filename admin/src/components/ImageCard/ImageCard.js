import React from 'react';
import { Card, Col, message, Row, Switch } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';
import TableButton from '../TableButton/TableButton';
import "./ImageCard.css";
import url from '../httpRequest/http';

export default function ImageCard(props) {

    // handling switch click event to change the status of the product image
    const onClickHandler = async (status,e,id) => {

        // creating object to send with put request
        const obj = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify({
                status: status === true ? 1 : 0,
                imgId: id,
                userId: localStorage.getItem("userId")                
            })
        };

        const response = await fetch(url + `/products/product-images/edit-product-image`, obj);
        const result = await response.json();
        if(result.status === true)
        {
            message.success("Status Updated.")
        }
        else{
            message.error("Something went wrong. Please try again.");
        }
    }
    return (
        <Col span={8}>
            <Card
                key={props.key}
                hoverable
                style={{ width: 300, height: 500, }}
                cover={<img alt="example" src={props.src} style={{ width: 300, height: 300 }} />}
                className={props.className || ""}
            >
                <Row>
                    <Col><h4>Status:</h4></Col>
                    <Col style={{ marginLeft: "5px" }}><h4 style={{ color: props.status === "ACTIVE" ? "green" : "red" }}>{props.status}
                    <Switch
                            style={{marginLeft:"97px"}}
                            defaultChecked={ props.status === "ACTIVE" && true}
                            onClick={(status,e) => onClickHandler(status,e,props.imgId)}
                        />
                    </h4></Col>
                </Row>
                <Row>
                    <Col>
                        {/* <TableButton
                            type="secondary"
                            style={{width:250}}
                            id={props.id}
                            className="button41"
                            title="Do you want to Edit this image?"
                            buttonText={<EditOutlined />}
                            onConfirm={() => props.onConfirm(props.id)}
                            onCancel={() => props.error("Action Aborted!")}
                            placement="bottom"
                            color="green"
                            tooltipTitle="Edit"
                        /> */}
                        
                    </Col>
                    {
                        // props.status === "INACTIVE" &&
                        <Col>
                            <TableButton
                                type="danger"
                                id={props.id}
                                className="button41"
                                title="Do you want to delete this image?"
                                style={{width:250}}
                                buttonText={<DeleteOutlined />}
                                onConfirm={() => props.onConfirm(props.imgId,props.name)}
                                onCancel={() => message.error("Action Aborted!")}
                                placement="bottom"
                                color="red"
                                tooltipTitle="delete"
                            />
                        </Col>
                    }
                </Row>
            </Card>
        </Col>
    )
}
