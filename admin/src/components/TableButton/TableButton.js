import React from 'react'
import { Popconfirm, Button, Tooltip } from "antd";

export default function TableButton(props) {
    return (
        <Popconfirm
            placement="topRight"
            title={props.title}
            onConfirm={() => props.onConfirm({ id: props.id, status: props.status })}
            onCancel={() => props.onCancel()}
            okText="Yes"
            cancelText="No"
        >
            <Tooltip placement={props.placement} color={props.color} title={props.tooltipTitle}>
                <Button type={props.type} style={props.style} className={props.className} shape={props.shape || null}>
                    {props.buttonText}
                </Button>
            </Tooltip>
        </Popconfirm>
    )
}
