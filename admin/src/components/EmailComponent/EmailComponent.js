import React from 'react'
import { Form, Input } from 'antd'
import { keyNumberHandler, keyNameHandler, keyAplhaNumHandler,keyEmailHandler } from '../../validationAdmin/validationAdmin';


export default function EmailComponent(props) {
  return (
    <Form.Item
      name={props.name}
      label={props.label}
      rules={[
        {
          type: 'email',
          message: 'The input is not valid E-mail!',
        },
        {
          required: true,
          message: 'Please input your E-mail!',
        },
      ]}
    >
      <Input
        style={{ width: "70%",marginLeft:"10px" }}
        value={props.value}
        minLength="15"
        maxLength="100"
        whitespace="false"
        readOnly={props.readOnly}
        onChange={(e) => props.onChange(props.name, e)}
        onKeyPress={(e) => { keyEmailHandler(e) }}
        
      />
    </Form.Item>
  )
    return (
        <Form.Item
        name={props.name}
        label={props.label}
        rules={[
          {
            type: 'email',
            message: 'The input is not valid E-mail!',
          },
          {
            required: true,
            message: 'Please input your E-mail!',
          },
        ]}
      >
        <Input style={{width:"70%"}} value={props.value} onChange={(e)=>props.onChange(props.name,e)} placeholder='Enter your email here'/>
      </Form.Item>
    )
  return (
    <Form.Item
      name={props.name}
      label={props.label}
      rules={[
        {
          type: 'email',
          message: 'The input is not valid E-mail!',
        },
        {
          required: true,
          message: 'Please input your E-mail!',
        },
      ]}
    >
      <Input
        style={{ width: "70%" }}
        value={props.value}
        minLength="15"
        maxLength="100"
        whitespace="false"
        readOnly={props.readOnly}
        onChange={(e) => props.onChange(props.name, e)}
        onKeyPress={(e) => { keyEmailHandler(e) }}
        placeholder='Enter your email here'
      />
    </Form.Item>
  )
}
