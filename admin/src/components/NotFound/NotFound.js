import React from 'react';
import { Card } from "antd";
import "./NotFound.css";
// import img from "./NotFound.PNG";
import "antd/dist/antd.css";

export default function NotFound() {
    return (
        <Card bordered={false} className='card4'>
            <h1>Page Not Found.</h1>
            {/* <img src={img}/> */}
        </Card>
    )
}
