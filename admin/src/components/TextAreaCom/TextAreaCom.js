import React from 'react'
import { Form, Input } from 'antd'
import { keyAddressHandler, keyAplhaNumHandler } from '../../validationAdmin/validationAdmin';


export default function TextAreaCom(props) {
  return (
    <Form.Item
      id={props.id}
      name={props.name}
      label={props.label}
      rules={[
        {
          required: true,
          message: props.message,
        },
      ]}
    >
      <Input.TextArea
        showCount
        maxLength={props.maxLength}
        value={props.value}
        onChange={(e) => props.onChange(props.name, e)} 
        placeholder={props.placeholder}
        onKeyPress={(e) => { props.type === "text" && keyAddressHandler(e) || props.type === "alphanum" && keyAplhaNumHandler(e) }}
        readOnly={props.readOnly || false}
        style={props.style}
      />
    </Form.Item>
  )
}
