import React from 'react'
import { Form, Input } from 'antd';
import "antd/dist/antd.css";
import { keyNumberHandler, keyNameHandler, keyAplhaNumHandler } from '../../validationAdmin/validationAdmin';

export default function InputField(props) {

    return (
        <>
            <Form.Item
                label={props.label}
                name={props.name}
                style={props.style}

                rules={[
                    {
                        required: props.required,
                        message: props.message,
                    }
                ]}
            >
                <InputField
                    readOnly={props.readOnly}
                    placeholder={props.placeholder}
                    onKeyPress={(e) => { props.type === "text" && keyNameHandler(e) || props.type === "number" && keyNumberHandler(e) || props.type === "alphanum" && keyAplhaNumHandler(e) }}
                    minLength={props.minLength}
                    maxLength={props.maxLength}
                    whitespace={props.whitespace}
                    value={props.value}
                    style={props.style}
                   // readOnly={props.readOnly}
                    onChange={(e) => props.onChange(props.name, e)}
                />
            </Form.Item>
        </>
    )

}
