import React from 'react'
import {Form, Select } from 'antd';
const { Option } = Select;
export default function SelectComponent(props) {
    return (
        <>
            <Form.Item
                name={props.name}
                label={props.label}
                style={props.style}
                rules={[
                    {
                        required: props.value ? false : true,
                        message: props.message,
                    },
                ]}
            >
             <Select placeholder="select your status" select={props.value} onChange={value => props.onChange(value)}>
                    <Option value="Active">Active</Option>
                    <Option value="inactive">Inactive</Option>
                </Select>  
            </Form.Item>
        </>
    );
}

