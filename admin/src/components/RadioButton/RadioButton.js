import React from 'react'
import {Form, Select } from 'antd';
const { Option } = Select;
export default function RadioButton(props) {
    return (
        <>
            <Form.Item
                name={props.name}
                label={props.label}
                rules={[
                    {
                        required: true,
                        message: props.message,
                    },
                ]}
                
            >
                <Select placeholder="select your gender" select={props.value} style={{width:"50%" ,marginLeft:"50px"}} onChange={(value)=>{props.onChange(value)}}>
                    <Option value="M">Male</Option>
                    <Option value="F">Female</Option>
                    <Option value="O">Other</Option>
                </Select>
            </Form.Item>
        </>
    );
}

