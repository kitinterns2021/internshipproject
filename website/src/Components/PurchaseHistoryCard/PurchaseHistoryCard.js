import React from 'react'
import "antd/dist/antd.css";
import { Tag, Card, Col, Row, Typography } from 'antd';
import "../../pages/Accounts/PurchaseHistory/PurchaseHistory.css";

const { Title } = Typography;

const PurchaseHistoryCard = (props) => {
    return (
        <div>
            {/* <Col span={8} > */}
                <Card bordered={true} className="col2" hoverable={true} >
                   
                        <Row>
                            <Col span={40}>
                                <img src={props.src} alt="image" width="150px" height="100px" />
                            </Col>
                            <Col style={{ marginLeft: "2rem" }} span={40}>
                                <Row>
                                <Col><Tag color="#f50">{props.tag}</Tag></Col>
                                    <Col><h3><b>{props.brand}</b></h3></Col>
                                </Row>
                                <Row><h2>{props.productName}</h2></Row>
                                <Row>Order Status : {props.orderStatus}</Row>
                                <Row>Deleivery address :{props.city}</Row>
                            </Col>
                        </Row>
                </Card>
            {/* </Col > */}
        </div>
    )
}

export default PurchaseHistoryCard