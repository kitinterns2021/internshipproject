import React from 'react'
import { Popconfirm, Button } from "antd";

export default function ButtonTable(props) {
    return (
        <Popconfirm
            placement="topRight"
            title= {props.title}
            onConfirm={() => props.onConfirm({id: props.id, status: props.status})}
            onCancel={() =>props.onCancel()}
            okText="Yes"
            cancelText="No"
        >
            <Button type={props.type} className={props.className || 'button2'}>
                {props.buttonText}
            </Button>
        </Popconfirm>
    )
}
