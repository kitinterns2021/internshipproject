import React from 'react'
import { Form, Input } from 'antd'

export default function OnlyPassword(props) {
    return (
        <>
            <Form.Item
                name={props.name}
                label={props.label}
                rules={[
                    {
                        required: true,
                        message: 'Please input your current password!',
                    },
                ]}
                hasFeedback
            >
                <Input.Password style={{width:"70%"}} onChange={(e)=>props.onChange(props.name, e)} value={props.value} placeholder='Enter your current password here'/>
            </Form.Item>
        </>
    )
}
