import React from 'react'
import { Form, Input } from 'antd';
import "antd/dist/antd.css";
import { keyNumberHandler, keyNameHandler, keyAplhaNumHandler } from '../../validation/validation';

export default function InputComponents(props) {

    return (
        <>
            <Form.Item
                label={props.label}
                name={props.name}
                rules={[
                    {
                        required: true,
                        message: props.message,
                    }
                ]}
            >
                <Input
                    placeholder={props.placeholder}
                    onKeyPress={(e) => { (props.type === "text" && keyNameHandler(e)) || (props.type === "number" && keyNumberHandler(e)) || (props.type === "alphanum" && keyAplhaNumHandler(e)) }}
                    minLength={props.minLength}
                    maxLength={props.maxLength}
                    whitespace={props.whitespace}
                    value={props.value}
                    style={props.style}
                    onChange={(e) => props.onChange(props.name, e)}
                    readOnly={props.readOnly}
                />
            </Form.Item>
        </>
    )

}
