import React from 'react'
import "antd/dist/antd.css";
import { Card, Col, Row } from 'antd';
import { Link } from "react-router-dom";


export default function AccountCard(props) {
    return (

        <Col span={8} >
            <Card bordered={true} className="col" hoverable={true} style={{boxShadow: "0 6px 10px 0 rgba(0,0,0,0.2), 0 8px 20px 0 rgba(0,0,0,0.19)"}}>
                <Link to={props.link} style={{color:"black"}}>
                    <Row>
                        <Col>
                            {props.icon}
                        </Col>
                        <Col style={{ marginLeft: "2rem" }}>
                            <h2>{props.title}</h2>
                            {props.content}
                        </Col>

                    </Row>
                </Link>
            </Card>
        </Col >
    )
}
