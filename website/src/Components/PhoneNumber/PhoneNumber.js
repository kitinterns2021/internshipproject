import React from 'react'
import { Form, Input } from 'antd'
import { keyNumberHandler } from '../../validation/validation';
// const { Option } = Select;

export default function PhoneNumber(props) {

    // const prefixSelector = (
    //     <Form.Item name="prefix" noStyle>
    //         <Select
    //             style={{
    //                 width: 70,
    //             }}
    //         >
    //             <Option value="86">+86</Option>
    //             <Option value="87">+87</Option>
    //         </Select>
    //     </Form.Item>
    // );
    return (
        <>
            <Form.Item
                name={props.name}
                label={props.label}
                initialValue={props.value}
                rules={[
                    {
                        required: true,
                        message: props.message,
                    },
                ]}
            >
                <Input
                    // addonBefore={prefixSelector}
                    style={{
                        width: '50%'
                    }}
                    placeholder='Enter contact number here...'

                    // placeholder={props.placeholder}
                    onKeyPress={(e) => {props.type === "number" && keyNumberHandler(e)}}
                    minLength={props.minLength}
                    maxLength={props.maxLength}
                    whitespace={props.whitespace}
                    value={props.value}
                    style={props.style}
                    onChange={(e) => props.onChange(props.name, e)}
                />
            </Form.Item>
        </>
    )
}
