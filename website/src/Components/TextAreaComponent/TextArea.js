import React from 'react'
import { Form, Input } from 'antd'
import { keyAddressHandler, keyAplhaNumHandler } from '../../validation/validation';


export default function TextArea(props) {
  return (
    <Form.Item
      id={props.id}
      name={props.name}
      label={props.label}
      rules={[
        {
          required: true,
          message: props.message,
        },
      ]}
    >
      <Input.TextArea
        showCount
        maxLength={props.maxLength}
        value={props.value}
        onChange={(e) => props.onChange(props.name, e)} 
        placeholder={props.placeholder}
        onKeyPress={(e) => { (props.type === "text" && keyAddressHandler(e)) || (props.type === "alphanum" && keyAplhaNumHandler(e)) }}
      />
    </Form.Item>
  )
}
