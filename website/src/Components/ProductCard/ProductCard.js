import React from 'react'
import { Tag, Card, Row, Statistic } from 'antd';
import "../../pages/Dashboard/Dashboard.css";
import { Link } from 'react-router-dom';

const ProductCard = (props) => {
    return (
        <Link to={`/product/${props.id}`}>
            <Card bordered={false} style={{ backgroundColor: "white", width: '300px', height: '580px' }} className='productCard22'>
                <Row align='center'>
                    <img src={props.img} alt='Laptop' width="300px" height="320px" />
                </Row>
                <Row align='center' >
                    <div className='div232'>
                        <h3><b>{props.brand}</b></h3>
                        <Tag color="#f50">{props.tag}</Tag>

                        <h2><b>{props.title}</b></h2>

                        <Statistic value={props.deletedPrice} precision={2} prefix="Rs. " valueStyle={{ color: "orange", fontSize: "13px", fontWeight: "bold" }} />
                        <Statistic value={props.price} precision={2} prefix="Rs. " valueStyle={{ color: "green", fontSize: "medium", fontWeight: "bold" }} />

                    </div>
                </Row>
            </Card>
        </Link>
    )
}

export default ProductCard