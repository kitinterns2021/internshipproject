import React from 'react'
import "../../pages/Dashboard/Dashboard.css";
import ProductCard from '../ProductCard/ProductCard';
import url from '../httpRequests/httpRequest';
import { Typography } from 'antd';


const { Title } = Typography;

const PeopleSearch = (props) => {
    return (
        <div className='division2'><br />
            <Title className='title2' level={2}>
                {props.title}
            </Title>
            <div className='wrapper'>
                {
                    props.products.map(product => {
                        return <div className='item2'>
                          <ProductCard
                            id = {product.id}
                            img={url + `/images/${product.imageURL}`}
                            tag={product.tag}
                            brand={product.brandName}
                            title={product.productName}
                            price={product.price}
                            deletedPrice={product.deletedPrice}
                            // description='Enter Product description here'
                          />
                        </div>
                    })
                }
            </div>
        </div>
    )
}

export default PeopleSearch