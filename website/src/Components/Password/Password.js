import React from 'react'
import { Form, Input } from 'antd'

export default function Password(props) {
  return (
    <>
      <Form.Item
        name="password"
        label="New Password"
        rules={[
          {
            required: true,
            message: 'Please input your new password!',
          },
        ]}
        hasFeedback
      >
        <Input.Password style={{ width: "70%" }} value={props.value} onChange={(e)=>props.onChange(props.name,e)} placeholder='Enter new password'/>
      </Form.Item>
      <Form.Item
        name="confirm"
        label="Confirm Password"
        dependencies={['password']}
        hasFeedback
        rules={[
          {
            required: true,
            message: 'Please confirm your password!',
          },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue('password') === value) {
                return Promise.resolve();
              }

              return Promise.reject(new Error('The two passwords that you entered do not match!'));
            },
          }),
        ]}
      >
        <Input.Password style={{ width: "70%" }} placeholder='Re-enter above password'/>
      </Form.Item>
    </>
  )
}
