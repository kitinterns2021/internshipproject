import React from 'react'
import { Card, Col, Row } from 'antd';
import "antd/dist/antd.css";
import "../../pages/Accounts/PurchaseHistory/PurchaseHistory.css";

const style1 = { background: 'white', padding: '8px 8px' };


const CategoryCard = (props) => {
    return (<>
        <Col className="gutter-row" span={25}>

            <div style={style1}>

                <div className="site-card-border-less-wrapper">
                    <Card bordered={false} hoverable={true} style={{ width: 280, boxShadow: "0 2px 1px 0 rgba(0,0,0,0.2), 0 8px 20px 0 rgba(0,0,0,0.19)" }}>
                        <Row align='center'>
                            <img src={props.img} alt={props.alt} width="150px" height="100px" />
                        </Row>
                        <Row align='center' >
                            <div className='div231'>
                                <h3 style={{color:"Teal"}}><b>{props.category}</b></h3>
                            </div>
                        </Row>
                    </Card>

                </div>
            </div>
        </Col>
    </>)
}

export default CategoryCard