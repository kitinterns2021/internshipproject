import React, { useState, useEffect } from 'react';
import "./RelatedProducts.css";
import { Divider, Layout, Skeleton, Typography } from 'antd';
import "antd/dist/antd.css";
import { useLocation } from 'react-router-dom';
import ProductCard from '../../../Components/ProductCard/ProductCard';
import url from '../../../Components/httpRequests/httpRequest';
import PeopleSearch from '../../../Components/PeopleSearch/PeopleSearch';

const { Title } = Typography;

export default function RelatedProducts() {

    // getting category id from search parameter
    const location = useLocation();
    console.log(location)
    const params = new URLSearchParams(location.search);


    // setting state for data
    const [data, setData] = useState({});

    // setting state for skelaton
    const [isLoading, setIsLoading] = useState(true);

    // getting id
    const id = params.get("id");

    // getting records related with category
    const getData = async () => {
        // const response = await fetch(url + `/productSearch/category${(location.pathname === '/products/category' && `?categoryId=${id}`) || (location.pathname === '/products/subcategory' && `?subCategoryId=${id}`)}`);
        const response = await fetch(url + `/productSearch/category${location.pathname === '/products/category'? `?categoryId=${id}`: `?subCategoryId=${id}`}`);
        const result = await response.json();

        // setting data in a state
        setData({ ...result });

        setIsLoading(false);
    }//end getData

    useEffect(()=>{
        getData();
        return () => {
            // scrolling smoothly to the top of the page when props or id changes, using component didUnmount function.
            window.scrollTo({
              top: 0, 
              behavior: 'smooth'
            });
          };
    },[data,id])

    return <Layout>
        {
            isLoading === true ?

                <Skeleton />
                :

                <div className='container41afsf'>
                    <Title level={3}>
                      Results For { data.category.name }
                    </Title>
                    <Divider/>

                    <Title level={5} style={{color:"grey"}}>
                        {data.category.totalRecords} Products Pound.
                    </Title>

                    <div className='products2'>
                        {
                            data.products.map(product => {
                                return <ProductCard
                                    id={product.id}
                                    img={url + `/images/${product.imageURL}`}
                                    tag={product.tag}
                                    brand={product.brandName}
                                    title={product.productName}
                                    price={product.price}
                                    deletedPrice={product.deletedPrice}
                                />
                            })
                        }
                    </div>

                    <PeopleSearch title="People also search for" products={data.relatedProducts} />
                </div>
        }
    </Layout>
}