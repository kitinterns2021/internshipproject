import React, { useState, useEffect } from 'react'
import "./SingleProduct.css";
import { Breadcrumb, Row, Col, Image, Typography, Button, notification, Divider, Statistic } from "antd";
import { useParams, useHistory } from 'react-router-dom';
import "antd/dist/antd.css";
import { CarOutlined, RollbackOutlined } from "@ant-design/icons";
import url from "../../../Components/httpRequests/httpRequest";
import cartFunction from "../../Cart/cartFunction";
import PeopleSearch from "../../../Components/PeopleSearch/PeopleSearch";

const { Title } = Typography;

export default function SingleProduct() {

    // getting product id
    const { productId } = useParams();

    // getting history object
    const history = useHistory();

    // setting state for cart button. Initial quantity of this state will be the quantity present in the local storage, 0 otherwise.
    const cart = "cart" in localStorage ? JSON.parse(localStorage.getItem("cart")) : {};

    const [cartQuantity, setCartQuantity] = useState(0);

    // setting state for all data of product
    const [data, setData] = useState({
        productDetails: {},
        specifications: [],
        images: [],
        relatedProducts: []
    });


    // setting state for image
    const [image, setImage] = useState("");

    // function to get all data of the product
    const getProduct = async () => {

        const response = await fetch(url + `/single-product?id=${productId}`);
        const result = await response.json();

        // getting all images url in an array
        const images = [];
        result.images.map(element => {
            return images.push(element.imageURL)
        })

        setData({
            ...data,
            productDetails: result.data,
            specifications: result.specifications,
            images: [...images],
            relatedProducts: result.relatedProducts
        })

        setImage(images[0])

    }

    useEffect(() => {
        getProduct();
        setCartQuantity(Object.keys(cart).length !== 0 && productId in cart ? cart[productId] : 0);
        return () => {
            // scrolling smoothly to the top of the page when props or id changes, using component didUnmount function.
            window.scrollTo({
              top: 0, 
              behavior: 'smooth'
            });
          };
    }, [productId]);


    const onClickHandler = imageURL => {
        setImage(imageURL);
    }

    // changing cart quantity
    const addQuantityHandler = val => {
        // checking if the quantity is greater than 5, return if true
        if (cartQuantity === 5) {
            notification.info({
                description: "Can add 5 items only"
            });

            return;
        }
        setCartQuantity(cartQuantity + val);

        // adding to the cart
        cartFunction(productId, (cartQuantity + val))
    }

    // changing cart quantity
    const substractQuantityHandler = val => {
        setCartQuantity(cartQuantity - val);
        // removing from the cart
        cartFunction(productId, (cartQuantity - val))
    }

    // handling buy now event
    const buyNowHandler = () => {
        if (Object.keys(cart).length !== 0 && productId in cart) {
            history.push("/payment");
        }
        else {
            // adding to the cart
            cartFunction(productId, (cartQuantity + 1)); // ==> If the quantity of the product in cart is 0, then it will be set to 1
            // redirecting user to the payment form
            // <Redirect to="/payment/"/>
            history.push("/payment");
        }

    }

    return (
        <div className='container41'>

            <Breadcrumb>
                <Breadcrumb.Item className='breadcrumb41'>
                    {data.productDetails.category}
                </Breadcrumb.Item>
                <Breadcrumb.Item className='breadcrumb41'>
                    {data.productDetails.subcategory}
                </Breadcrumb.Item>
                <Breadcrumb.Item className='breadcrumb41'>
                    {data.productDetails.brand}
                </Breadcrumb.Item>
                {/* <Breadcrumb.Item className='breadcrumb41'>
                {data.productDetails.subcategory}
                </Breadcrumb.Item> */}
            </Breadcrumb>

            <Row gutter={[10, 0]}>

                {/* Product images array */}
                <Col span={2}>
                    <div className='container42'>

                        {/* getting all images */}
                        {
                            data.images.map(img => {
                                return <Image src={url + `/images/${img}`} preview={false} className="img41" onClick={() => onClickHandler(img)} />
                            })
                        }
                    </div>
                </Col>

                {/* Product main image */}
                <Col span={12} className="col41">
                    <Image
                        className='img42'
                        src={url + `/images/${image}`}
                        preview={true}
                    />
                </Col>

                {/* Product details and price */}
                <Col span={10} className="col42">
                    <div className='container43'>
                        <p style={{ fontWeight: "bold" }}>{data.productDetails.brand}</p>

                        <Title
                            level={3}
                            style={{ marginBottom: "30px" }}
                        >{data.productDetails.name}</Title>

                        <del>
                            <Statistic valueStyle={{ fontSize: "12px", color: "red", marginBottom: 2 }} value={data.productDetails.delPrice} precision={2} prefix={'RS'} />
                        </del>

                        <Statistic valueStyle={{ fontSize: "18px", fontWeight: "bold", color: "green", marginBottom: 20 }} value={data.productDetails.price} precision={2} prefix={'RS'} />

                        <Row>
                            <Col span={12}>
                                <Button shape='round' type='primary' className='btn41' onClick={buyNowHandler}>Buy Now</Button>
                            </Col>
                            <Col span={7}>
                                {
                                    cartQuantity === 0 ?
                                        <Button shape='round' type='primary' className='btn41' onClick={() => addQuantityHandler(1)}>Add to Cart</Button>
                                        :
                                        <div className='btn42hd73'>
                                            <Button shape='circle' className='btn43sfs' style={{ margin: "5px 20px 5px 10px" }} onClick={() => addQuantityHandler(1)}>+</Button>
                                            {cartQuantity} Added
                                            <Button shape='circle' className='btn43sfs' style={{ margin: "5px 10px 5px 25px" }} onClick={() => substractQuantityHandler(1)}>-</Button>
                                        </div>
                                }
                            </Col>
                        </Row>

                        <Divider />

                        <Row>
                            <Col span={2}>
                                <CarOutlined style={{ fontSize: "20px" }} />
                            </Col>
                            <Col>
                                <p style={{ fontSize: "16px" }}>
                                    Free shipping over amount of RS. 1000. <br /> <p style={{ color: "green" }}><u>Get it by Wed, Feb 16.</u></p>
                                </p>
                            </Col>
                        </Row>

                        <Row>
                            <Col span={2}>
                                <RollbackOutlined style={{ fontSize: "20px" }} />
                            </Col>
                            <Col>
                                <p style={{ fontSize: "16px" }}>
                                    7 Days Return Policy
                                </p>
                            </Col>
                        </Row>

                    </div>
                </Col>

            </Row>

            <Title level={4} style={{ marginTop: 40 }}>
                All About This Item
            </Title>

            <Divider />

            <Title level={5} className="title41">
                Product Details
            </Title>

            <p className='content41'>
                {data.productDetails.description}
            </p>

            <Divider />

            <Title level={5} style={{ marginBottom: "20px" }} className="title41">
                Specifications
            </Title>

            {/* showing all specifications */}
            {
                data.specifications.map(element => {
                    return <p className='content41 text41'>
                        <b>{element.specKey}</b>
                        <br />
                        {element.value}
                    </p>
                })
            }

            <Divider />

            <PeopleSearch title="Our Reccomendations" products={data.relatedProducts} />

        </div>
    )
}