/**
 * 
 * @param {*} id Product Id
 * @param {*} qty Product Quantity
 * 
 * This function is for adding and removing products in cart
 */

export default function cartFunction(id, qty) {

    // getting data from localstorage if present
    let cartItems = JSON.parse(localStorage.getItem("cart"));

    // setting message to send
    let message = "changed";

    if (qty === 0) {

        // if quantity of product gets 0, removing element from the object.
        delete cartItems[id];

        message = "delete";
    }
    else {

        // changing cart items for perticular product id with its quantity
        cartItems = {
            ...cartItems,
            [id]: qty
        }
    }

    if (Object.keys(cartItems).length === 0) {

        // if length of the object gets 0, removing cart object from the local storage
        localStorage.removeItem("cart")
        return "delete";
        
    } else {
        // setting object again to localstorage
        localStorage.setItem("cart", JSON.stringify(cartItems));
        return message;
    }

}//end cart