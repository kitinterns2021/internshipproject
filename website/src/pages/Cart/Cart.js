import React, { useState, useEffect } from 'react';
import "./Cart.css";
import { Layout, Typography, Divider, Image, Row, Col, Tag, Select, message, Button, Statistic } from 'antd';
import "antd/dist/antd.css";
import { Link } from 'react-router-dom';
import { FrownTwoTone } from "@ant-design/icons";
import cartFunction from "./cartFunction";
import url from '../../Components/httpRequests/httpRequest';

// getting all products from home array data
// import HomeArrayData from "../Home/HomeArrayData";

// extracting properties from components
const { Title } = Typography;
const { Option } = Select;

export default function Cart() {

    // getting cart products data from local storage
    const data = JSON.parse(localStorage.getItem("cart")) || {};

    // getting all available products
    // const homearraydata = HomeArrayData();

    // setting state for showing cart products or no products found if localstorage is empty
    const [cartState, setCartState] = useState(Object.keys(data).length === 0 ? false : true);
    // const [cartState, setCartState] = useState(true);

    //setting state to refresh data when event happens ==> find replacement for this
    const [state, setState] = useState(0);

    // setting initial state for main data to show in cart
    const [arrayData, setArrayData] = useState({
        array: [],
        qty: 0,
        totalAmount: 0
    });

    const getData = async () => {
        let array = [];
        let qty = 0;
        let totalAmount = 0;
        if (Object.keys(data).length !== 0) {

            // iterating to the local storage cart data
            for (let key in data) {

                const productId = parseInt(key);

                const response = await fetch(url + `/product-item?productId=${productId}`);
                const result = await response.json();

                //setting product in main array to map
                array.push(result[0]);

                // setting total quantity of products present in cart
                qty += data[key];

                // setting grand total amount of all products
                // totalAmount += data[key] * parseInt(result[0].price.replace(/,/g, ''))
                totalAmount += data[key] * parseInt(result[0].price)


            }//end for
        }//end if

        //setting main data to show in cart
        setArrayData({
            array,
            qty,
            totalAmount
        })
    }//end getData()

    // getting final data to show in cart
    useEffect(() =>
        getData()

        , [state]);


    // changing quantity of the product
    const changeQuantityHandler = (id, qty) => {
        // calling cartFunction to change quantity of the product
        const msg = cartFunction(id, qty);

        // displaying message
        if (msg === "changed") {
            message.success("Product quantity changed to " + qty);
        }
        else if (msg === "delete") {
            message.info("Product removed from cart.");
            if (!localStorage.getItem("cart")) {
                setCartState(false);
            }
        }
        setState(!state);
    }//end changeQuantityHandler


    return <Layout>
        {
            cartState ?

                <>
                    <div className='container411'>
                        <div style={{ float: "left" }}>
                            <Title className='title411'>
                                Shopping Cart
                            </Title>
                        </div>
                        <Divider />

                        {
                            arrayData["array"].map(element => {
                                console.log(element)
                                return <>
                                    <div>
                                        <Row>
                                            <Col span={5}>
                                                <Image
                                                    width="250px"
                                                    height="250px"
                                                    preview={false}
                                                    src={url + `/images/${element.imageURL}`}
                                                />
                                            </Col>
                                            <Col span={13}>
                                                <Link to={`/product/${element.id}`}>
                                                    <Title
                                                        className='title451'
                                                        level={5}
                                                    >
                                                        {element.name}
                                                    </Title>
                                                </Link>
                                                <p style={{ color: "green" }}>
                                                    In Stock
                                                </p>

                                                <Tag color="gold">
                                                    #From Best Seller
                                                </Tag>

                                                <Title
                                                    style={{ marginTop: "15px" }}
                                                    className='title451'
                                                    level={5}
                                                >
                                                    Date Modified - {new Date().toDateString()}
                                                </Title>

                                                <Title
                                                    className='title451'
                                                    level={5}
                                                >
                                                    Qty <Select defaultValue={data[element.id] || 1} style={{ width: "60px", marginLeft: "7px", marginRight: "7px" }} onChange={value => changeQuantityHandler(element.id, value)}>
                                                        <Option value={1}>1</Option>
                                                        <Option value={2}>2</Option>
                                                        <Option value={3}>3</Option>
                                                        <Option value={4}>4</Option>
                                                        <Option value={5}>5</Option>
                                                    </Select>
                                                    <Divider
                                                        type='vertical'
                                                        style={{ border: "0.01rem solid black", height: "20px" }}
                                                    />
                                                    <a style={{ marginLeft: "7px", marginRight: "7px" }} onClick={() => changeQuantityHandler(element.id, 0)}>Delete</a>

                                                    <Divider
                                                        type='vertical'
                                                        style={{ border: "0.01rem solid black", height: "20px" }}
                                                    />

                                                    <a style={{ marginLeft: "7px", marginRight: "7px" }}>Buy Now</a>
                                                </Title>

                                            </Col>
                                            <Col span={5}>
                                                <Title
                                                    style={{ fontWeight: "bold", float: "right" }}
                                                    level={5}
                                                >
                                                    <Statistic valueStyle={{ fontSize: "medium" }} value={element.price} precision={2} prefix={'RS'} />

                                                </Title>
                                            </Col>
                                        </Row>
                                    </div>

                                    <Divider />
                                </>
                            })
                        }

                        <div style={{ float: "right" }}>
                            <Title level={5}>
                                <Statistic value={arrayData.totalAmount} valueStyle={{ fontSize: "large" }} precision={2} prefix={`Subtotal(${arrayData.qty} items): Rs.`} />
                            </Title>
                            <Link to="/payment">
                                <Button type='secondary' style={{ marginLeft: "20px", width: "250px", marginBottom: "10px" }}>
                                    Proceed to checkout
                                </Button>
                            </Link>
                        </div>

                    </div>
                    <p style={{ marginLeft: "2rem" }}>
                        The price and availability of items are subject to change. The shopping cart is a temporary place to store a list of your items and reflects each item's most recent price.
                    </p>
                </>

                :

                <div className="container421">
                    <FrownTwoTone twoToneColor="#76c8e4" style={{ fontSize: "70px" }} />
                    <Title style={{ color: "#76c8e4" }}>
                        No Items Found
                    </Title>
                </div>
        }
    </Layout>
}