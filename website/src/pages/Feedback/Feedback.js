import {Content, Header } from 'antd/lib/layout/layout';
import React, { useState } from 'react';
import { Route, Switch, Link,useHistory } from "react-router-dom";
import { Form,Button,Input,Checkbox,Card,message,Rate } from 'antd';
import "antd/dist/antd.css";
import { render } from 'react-dom';
import TextArea from '../../Components/TextAreaComponent/TextArea';
import "./Feedback.css";
const Feedback = (props) => {
    const [form] = Form.useForm()
    const [currentValue,setCurrentValue] = useState()
    const onFinish = async (e) => {
    }
    const ratehandler =  (value) =>
    { 
        setCurrentValue(value); 
        let u={currentValue}
        if (u=1) {
            console.log("bad")
          
       }
       else {
        console.log("good")
   
   }
        
    
      

    }  
    const formItemLayout = {
        
        labelCol: {
            xs: {
                span: 24,
            },
            sm: {
                span: 8,
            },
        },
        wrapperCol: {
            xs: {
                span:24,
            },
            sm: {
                span:16,
            },
        },
    };
    const tailFormatLayout = {
        wrapperCol: {
            xs: {
                span:15,
                offset:0,
            },
            sm: {
                span:10,
                offset:0,
            },
        },
    };
    return (
        <Content className="container5" >
        <Form {...formItemLayout} form={form}name="register"onFinish={onFinish} scrollToFirstError >
         <Card hoverable="true"  className="card5">
          <Header style={{ marginLeft:"160px",marginTop:"8px",textDecoration:"underline", backgroundColor: "transparent",width:"480px", fontSize: "xx-large", fontWeight: "bolder",paddingBottom:"80px" }}><h2>Feedback</h2></Header> 
            <span className="feedback1">Your feedback matters!</span>
             <br />
             <span className="feedback2">Help us improve our Website </span>
           
             
            
            
           
           <Form.Item style={{ marginLeft:"240px",marginTop:"8px",width:"480px", fontSize: "x-large",textDecoration: "underline", fontWeight: "600px"}} >
           <span style={{ marginLeft:"30px"}}>Rating</span>
           <br/>
           <Rate defaultValue={5} value={currentValue} onChange ={ratehandler}/>
           <br/> current rating ={currentValue} 
          
         
           
           </Form.Item>
            
           <TextArea style={{ marginLeft:"140px",marginTop:"0px",width:"350px",reSize:"none" }} placeholder="write your comments"/>
           <Form.Item>
          <Button  block type="primary" htmlType="submit" className="login-form-button">
            share with us
          </Button>

         </Form.Item>
          

           </Card>
        </Form>
     </Content>
     
   
            
    )
}

export default Feedback;
  