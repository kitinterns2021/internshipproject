import React, { useState } from 'react';
import { Link } from "react-router-dom";
import { Form,Button,Checkbox,Card,notification} from 'antd';
import "antd/dist/antd.css";
import "./Signup.css";
import {Content, Header } from 'antd/lib/layout/layout';
import InputComponents from '../../Components/InputComponents/InputComponents';
import EmailComponents from '../../Components/EmailComponent/EmailComponent';
import Password from '../../Components/Password/Password';
import url from '../../Components/httpRequests/httpRequest';
const Login = (props) => {

    const [form] = Form.useForm()
    const [data, setData] = useState(
        {
            name: "",
            emailId: "",
            password: "",
           
        }

    );

    // setting state to check email
    const [isEmailExists,setIsEmailExists] = useState(false);
    
    const openNotificationWithIcon = (type,msg) => {
        if(type === 'success')
        {
            notification.success({
                message: "Success!",
                description: msg
            });
        }
        else if(type === 'error')
        {
            notification.error({
                message: "Error",
                description: msg
            });
        }
    };


    const formItemLayout = {
        
        labelCol: {
            xs: {
                span: 24,
            },
            sm: {
                span: 8,
            },
        },
        wrapperCol: {
            xs: {
                span:24,
            },
            sm: {
                span:16,
            },
        },
    };
    const tailFormatLayout = {
        wrapperCol: {
            xs: {
                span:15,
                offset:0,
            },
            sm: {
                span:10,
                offset:0,
            },
        },
    };

    const onChangeHandler = (name, e) => {
        setData({
            ...data,
            [name]: e.target.value
        })

        isEmailExists === true && setIsEmailExists(false);
    }
    const onFinish = async (e) => {
       
       
        

         console.log(data);
          const obj={
              method:"POST",
              headers : {
                  "Content-Type":"application/json",
                  "Accept":"application/json"

              },
              body: JSON.stringify(data)
            }

               
         const response = await fetch(url + `/signup`,obj);
         const result = await response.json();
         console.log(result);
         if (result.id) {
            openNotificationWithIcon("success","Record Inserted Successfully...");

            // setting id to the local storage
            localStorage.setItem("userId",result.id);

            props.setIsLoggedIn();

            // redirecting the user to the home page
            window.location.href="/"
        }
        else  if(result.err){
            setIsEmailExists(true)
        }
        else{
            openNotificationWithIcon("error","Something went wrong. Please try again...");
        }

        
    };
   
      
      return (
        <Content className="container5" >
        <Form {...formItemLayout} form={form}name="register"onFinish={onFinish} scrollToFirstError >
         <Card hoverable="true"  className="card5">
          <Header style={{ marginLeft:"50px",marginTop:"10px", backgroundColor: "transparent",width:"480px", fontSize: "x-large", fontWeight: "bolder",paddingBottom:"80px" }}><h2>Create Your Account</h2></Header> 

         
           
           <InputComponents label="Name" name="name" value={data.name?data.name:""} onChange={onChangeHandler} style={{ width: "70%" }}  message="Please enter your Name" placeholder="Please enter your Name"/>  
           
           <EmailComponents label="Email" name="emailId" value={data.emailId?data.emailId:""} onChange={onChangeHandler}  message="Please enter your E-mail" placeholder="Please enter your E-mail"/> 
           {
               isEmailExists && <p style={{color:"red", marginLeft:"170px"}}>This email is already exists.<br/>Please try with different email.</p>
           }
           <Password label="Password" name="password" value={data.password?data.password:""} onChange={onChangeHandler} />
           
           <Form.Item className='form-button5'{...tailFormatLayout}  >
               <Button block type="primary" htmlType="submit">Create account</Button>
           </Form.Item>
            
            <div  className="div5">
            <Form.Item  name="account" valuePropName='checked'>
            <Checkbox ><h2>Already have an account </h2></Checkbox>
            </Form.Item>
            </div>
            
            
            
            
            <Form.Item className='form-button5'{...tailFormatLayout} >
          
               <Link to ="/sign-in">
                    <Button block  type="primary" >SignIn</Button>
               </Link>
               
           </Form.Item>
          

           </Card>
        </Form>
     </Content>
     
   
            
    )
}

export default Login;