import { Form, Input, Button, Checkbox, Card,notification } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import "./Signin.css";
import { Link } from "react-router-dom";
import { Header } from 'antd/lib/layout/layout';
import url from '../../Components/httpRequests/httpRequest';
import React, { useState } from 'react';
import { keyNumberHandler, keyNameHandler, keyAplhaNumHandler } from '../../validation/validation';

const Signin = (props) => {
  
  const [isError, setIsError] = useState(false);
  const openNotificationWithIcon = (msg) => {
    notification.success({
        message: "Success!",
        description: msg
    });
  };

   const onKeyPress= (e) => {
     props.type === "text" && keyNameHandler(e) || props.type === "number" && keyNumberHandler(e) || props.type === "alphanum" && keyAplhaNumHandler(e) }
                   
  
  const onFinish = async (values) => {
     
    const response = await fetch(url + `/signin?email=${values.email}&password=${values.password}`);
    const result = await response.json();
    if (result.id) {
      openNotificationWithIcon("Login Successfully...");
      setTimeout(() => {
        localStorage.setItem("userId",result.id);
      window.location.href="/"
      }, 500);
    }
    else{
      setIsError(true);
      setTimeout(() => {
        setIsError(false);
      }, 5000);
      // openNotificationWithIcon("Something went wrong. Please try again...");
   
    }
  };


  return (
    <Form
      className="form5"
      name="normal_login"

      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
    >
      <Card hoverable="true" className="card5">
        <Header style={{ marginLeft: "0px", marginRight: "0px", textDecoration: "underline", backgroundColor: "transparent", width: "350px", fontSize: "x-large", fontWeight: "bolder", paddingBottom: "80px" }}>Signin</Header>

        <Form.Item
          name="email"
          rules={[
            {
              required: true,
              message: 'Please enter your Email!',
            },
          ]}
        >
          <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Email" />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[
            {
              required: true,
              message: 'Please enter your Password!',
            },
          ]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Password"
            onKeyPress={onKeyPress}
          />
        </Form.Item>
        {
          isError &&
          <p style={{ color: "red" }}>Email or Password is invalid. Please check and try again!</p>
        }
        <Form.Item>
          <Form.Item name="remember" valuePropName="checked" noStyle>
            <Checkbox>Remember me</Checkbox>
          </Form.Item>

          <a className="login-form-forgot" href="">
            Forgot password
          </a>
        </Form.Item>

        <Form.Item>
          <Button type="primary" htmlType="submit" className="login-form-button">
            Sign in
          </Button>

        </Form.Item>
        <Link to ="/sign-up">
        <p>Do not have account? Create new account.</p>
        </Link>
      </Card>
    </Form>

  );
};

export default Signin;