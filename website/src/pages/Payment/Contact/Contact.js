import React, { useState } from 'react'
import { Form, Button, Typography, notification } from 'antd';
import "./Contact.css";
import InputComponent from "../../../Components/InputComponents/InputComponents";
import url from '../../../Components/httpRequests/httpRequest';

const { Title } = Typography;
export default function Contact(props) {

    const [contact,setContact] = useState('');
    // handling onChange event
    const onChangeHandler = (name,e) => {
        setContact(e.target.value)
    }//end onChangeHandler

    // defining data object to send
    const dataObj = {
        method:"PUT",
        headers:{
            "Content-Type":"application/json",
            "Accept":"application/json"},
        body:JSON.stringify({userId:localStorage.getItem("userId"),contact})}

    // handling onfinish event
    const onFinishHandler = async () =>{
        const response = await fetch(url + `/payment/add-contact`,dataObj);
        const result = response.json();
        if(result.err)
        {
            notification.error({
                message:"Error!",
                description:"Error adding contact number. Please try again."
            });
        }
        else{
            notification.success({
                message:"Success!",
                description:"Contact Number Added Successfully."
            });

            // sending contact number to the parent component
            props.onFinish(contact);
        }
    }//end onFinishHandler

  return (
//    Render a div and form insted of Drawer here and handle api logic to store contact also. Then execute an function coming from props i.e. props.ChangeContact(return setData(address.contact = contact)) to return an contact number and change the state
    <div className='container4sdh36'>
        <Form name="contactForm" onFinish={onFinishHandler}>
            <Title level={4}>
                Enter Contact Number
            </Title>
            <InputComponent
                name="contact"
                type="number"
                required={true}
                minLength={10}
                maxLength={10}
                message="This field can not be empty"
                placeholder="your contact here"
                onChange={onChangeHandler}

            />
            <Button type="primary" shape='round' htmlType='submit'>Save</Button>
        </Form>
    </div>
)
}
