import React, { useState, useEffect } from 'react';
import "./SelectAddress.css"
import { Modal, Button } from 'antd';
import url from '../../../Components/httpRequests/httpRequest';

export default function SelectAddress(props) {

    // setting state for data
    const [data, setData] = useState([]);

    // function for getting all addresses
    const getData = async () => {
        const response = await fetch(url + `/address-table?userId=${localStorage.getItem("userId")}`);
        const result = await response.json();
        setData([ ...data, ...result ]);
    }//end getData

    useEffect(() => {
        getData();
    }, []);

    // handling the onClick event
    const onClickHandler = (newAddress) => {

        // setting selected address in payment form
        props.setData({
            ...props.data,
            address: {...props.data.address, ...newAddress}
        })

        // closing the modal
        props.setSelectAddress(false);

    }//end onClickHandler

    // handling add address event
    const newAddressHandler = () => {
        props.setState({...props.state, address:true});
        props.setSelectAddress(false);
    }//end newAddressHandler

    return (
        <Modal
            title="Select Shipping Address"
            closable={true}
            centered
            visible={props.selectAddress}
            footer={null}
            width={700}
            onCancel={() => props.setSelectAddress(false)}

        >
            <div style={{overflow:"scroll", height:"300px",padding:"1rem 1rem 1rem 1rem"}}>
            {
                // mapping the addresses
                data.map(element => {
                    return <div className="selectAddressCard41" onClick={() => onClickHandler(element)}>
                        <h4>
                            {element.addressLine1} <br />
                            {element.addressLine2}<br />
                            {element.city}<br />
                            {element.state}<br />
                            {element.pincode}
                        </h4>
                    </div>
                })
            }
            </div>

            <Button style={{ marginTop: "20px" }} onClick={newAddressHandler}>
                New Address
            </Button>
        </Modal>

    )
}
