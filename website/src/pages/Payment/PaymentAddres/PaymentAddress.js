import React, { useState, useEffect } from 'react';
import { Typography, Form, Button, Select, notification } from 'antd';
import TextArea from '../../../Components/TextAreaComponent/TextArea';
import InputComponents from '../../../Components/InputComponents/InputComponents';
import "./PaymentAddress.css";
import url from '../../../Components/httpRequests/httpRequest';

const { Title } = Typography;
const { Option } = Select;
export default function PaymentAddress(props) {

    // setting state for data
    const [data,setData] = useState({
        userId: localStorage.getItem("userId"),
        addressLine1: "",
        addressLine2: "",
        city: "",
        state: "",
        pincode:""
    })
    const [form] = Form.useForm()

    const formItemLayout = {

        labelCol: {
            xs: {
                span: 24,
            },
            sm: {
                span: 4,
            },
        },
        wrapperCol: {
            xs: {
                span: 24,
            },
            sm: {
                span: 16,
            },
        },

    };

    // handling change event
    const onChangeHandler = (name,e) =>{
        setData({
            ...data,
            [name]:e.target.value
        })
    }//end onChangeHandler

    // handling onClick event
    const onClickHandler = async () => {

        // creating dataObj
        const dataObj = {
            method:"POST",
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json"
            },
            body: JSON.stringify(data)
        }
        const response = await fetch(url + "/address-table/add-address", dataObj);
        const result = await response.text();

        if(result){
            props.setData({...props.data,addressState:!props.data.addressState});
            notification.success({
                message:"Success!",
                description:"Address Added Successfully..."
            });

        }
    }//end onClickHandler

    return (
        <div>
            <Form {...formItemLayout} form={form} name="register" onFinish={"onFinish"} scrollToFirstError style={{ marginTop: "2rem" }}>
                    <Title level={4} style={{ textAlign: "left" }}>Enter Shipping Address</Title><br /><br />

                    {/* <p><Title level={5}>{!searchParams.get("id") ?
                        "If you want to add your address associated with your customer account, you may do so below." :
                        "If you want to update your address associated with your customer account, you may do so below."}
                    </Title></p> */}
                    <br />
                    <TextArea
                        id="addressLine1"
                        name="addressLine1"
                        label="Address Line 1"
                        message="Please enter address"
                        placeholder="Enter Address Line 1 here..."
                        whitespace="true"
                        maxLength="50"
                        onChange={onChangeHandler}
                        value={data.addressLine1}
                    />

                    <TextArea
                        id="addressLine2"
                        name="addressLine2"
                        label="Address Line 2"
                        message="Please enter address"
                        placeholder="Enter Address Line 2 here..."
                        whitespace="true"
                        maxLength="50"
                        onChange={onChangeHandler}
                        value={data.addressLine2}
                    />

                    <InputComponents
                        type="text"
                        name="city"
                        label="City"
                        message="Please enter your city"
                        placeholder="Enter your city here..."
                        maxLength="15"
                        whitespace="true"
                        onChange={onChangeHandler}
                        value={data.city}
                    />

                    <Form.Item
                        name="state"
                        label="State"
                        rules={[
                            {
                                required: true,
                                message: "Please Select Your State",
                            },
                        ]}
                    >
                        <Select placeholder="select your state" onChange={(value) => setData({ ...data,"state": value })}>
                            <Option value="Andhra Pradesh">Andhra Pradesh</Option>
                            <Option value="Arunachal Pradesh">Arunachal Pradesh</Option>
                            <Option value="Assam">Assam</Option>
                            <Option value="Bihar">Bihar</Option>
                            <Option value="Chhattisgarh">Chhattisgarh</Option>
                            <Option value="Goa">Goa</Option>
                            <Option value="Gujrat">Gujrat</Option>
                            <Option value="Haryana">Haryana</Option>
                            <Option value="Himachal Pradesh">Himachal Pradesh</Option>
                            <Option value="Jharkhand">Jharkhand</Option>
                            <Option value="Karnataka">Karnataka</Option>
                            <Option value="Kerala">Kerala</Option>
                            <Option value="Madhya Pradesh">Madhya Pradesh</Option>
                            <Option value="Maharashtra">Maharashtra</Option>
                            <Option value="Manipur">Manipur</Option>
                            <Option value="Meghalaya">Meghalaya</Option>
                            <Option value="Mizoram">Mizoram</Option>
                            <Option value="Nagaland">Nagaland</Option>
                            <Option value="Odisha">Odisha</Option>
                            <Option value="Punjab">Punjab</Option>
                            <Option value="Rajsthan">Rajsthan</Option>
                            <Option value="Sikkim">Sikkim</Option>
                            <Option value="Tamil Nadu">Tamil Nadu</Option>
                            <Option value="Telangana">Telangana</Option>
                            <Option value="Tripura">Tripura</Option>
                            <Option value="Uttarakhand">Uttarakhand</Option>
                            <Option value="Uttar Pradesh">Uttar Pradesh</Option>
                            <Option value="West Bengal">West Bengal</Option>

                        </Select>

                    </Form.Item>

                    <InputComponents
                        name="pincode"
                        type="number"
                        label="Pin-Code"
                        message="Please enter pin-code"
                        placeholder="Enter your pin-code here..."
                        // style={{ width: "50%" }}
                        maxLength="6"
                        minLength="6"
                        onChange={onChangeHandler}
                        value={data.pincode}
                    />

                    <Form.Item
                        wrapperCol={{
                            offset: 4,
                            span: 16,
                        }}
                    ><br />
                        
                        <Button type="primary" onClick={onClickHandler} shape="round" style={{ marginLeft: "0.1rem",width:200 }}>
                            Save
                        </Button><br /><br />
                    </Form.Item>
            </Form>
        </div>
    )
}

{/* <Link to="/account/your-addresses">
            <div style={{ margin: "3rem 5rem 7rem 13rem", width: "300px", height: "200px", textAlign: "center", paddingTop: "60px" }}>
                <FrownTwoTone style={{ fontSize: "50px" }} />
                <Title style={{ color: "#76c8e4" }}>
                    No Address Found
                </Title>
                Click to add address
            </div>
        </Link> */}
