import React, { useState, useEffect } from 'react';
import "./Payment.css";
import { notification, Typography, Row, Col, Divider, Button, Image, Tag, Radio, Statistic, Skeleton, Modal, Popover } from 'antd';
import "antd/dist/antd.css";
import { useHistory } from 'react-router-dom';
import PaymentAddress from './PaymentAddres/PaymentAddress';

// importing static images for payment cards
import masterCard from "./CardImages/mastercard.png";
import visa from "./CardImages/visa.png";
import citiBank from "./CardImages/citiBank.png";
import americanExpress from "./CardImages/american express.png";
import rupay from "./CardImages/rupay.png";
import url from '../../Components/httpRequests/httpRequest';
import Contact from './Contact/Contact';
import SelectAddress from "./PaymentAddres/SelectAddress";

const { Title } = Typography;

export default function Payment() {

  // getting history object
  const history = useHistory();

  // setting state for data
  const [data, setData] = useState({
    userId: localStorage.getItem("userId"),
    address: {},
    products: [],
    contact: 0,
    addressState:false
  });

  // setting state for skeleton
  const [isLoading, setIsLoading] = useState(true);

  // seting state for contact and address component
  const [state, setState] = useState({ contact: false, address: false });

  // setting state for selecting the adress
  const [selectAddress, setSelectAddress] = useState(false);

  // getting required data
  const getAllData = async () => {

    const response = await fetch(url + `/payment/get-data?userId=${localStorage.getItem("userId")}&products=${localStorage.getItem("cart")}`);
    const result = await response.json();
    // console.log(result);

    //setting data in state
    setData({
      ...data,
      ...result,
      contact: result.address.contact
    });

    // setting skelaton state to false
    setIsLoading(false);

  }//end getAllData

  useEffect(() => {

    // checking the user is logged in or not. if not, redirect it to login/sign-up page
    if (!localStorage.getItem("userId")) {
      notification.info({
        message: "Warning!",
        description: "You are not logged in. Please login to continue."
      });

      return history.push("/sign-in");

    }//end if
    else {
      getAllData();
    }
  }, [data.addressState])

  // getting date after 7 days
  const getDate = () => {

    const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    const date = new Date();
    date.setDate(date.getDate() + 7); //==> Setting date after 7 days
    let strDate = days[date.getDay()] + ", " + (months[date.getMonth()]) + " " + date.getDate();
    return strDate.toString();
  }//end getdate

  // handling proceed event
  const onProceedHandler = () => {
    return Modal.info({
      title: <h3 style={{ fontSize: "large", color: "blue" }}><b>Confirm Order!</b></h3>,
      content: (<>

        <b>
          <p>
            Name: {data.address.name}<br />
            <Statistic value={data.grandTotal} precision={2} prefix={"Order Total: RS. "} valueStyle={{ fontSize: "small" }} />
          </p>
        </b>

        <p style={{ color: "orange" }}>
          Placing the order may take some time. Please wait after clicking OK.
        </p>
      </>),
      closable: true,
      onOk: () => onConfirmHandler()
    });

  }//end onProceedHandler


  // handling onConfirm event to condirm the order
  const onConfirmHandler = async () => {
    // console.log(data);
    const response = await fetch(url + `/payment/make-payment`, { method: "POST", headers: { "Content-Type": "application/json", "Accept": "application,json" }, body: JSON.stringify(data) });
    const result = await response.json();
    if (result.err) {
      notification.error({
        message: "Error!",
        description: "Opps... Something went wrong. Please try again later."
      });
    }
    else if (result.success === true) {
      notification.success({
        message: "Success!",
        description: "Order Placed Successfully And The Email Has Been Sent To Your Registred Email Account!"
      });

      // removing items from cart
      localStorage.removeItem("cart");

      // redirecting user to the homepage
      history.push("/");
    }
  }//end onConfirmHandler

  // handling contact change event
  const changeContactHandler = contact => {
    setData({
      ...data,
      contact: contact
    });

    setState({
      ...state,
      contact: false
    });
  }//end changeContactHandler

  return (<>
    {
      isLoading ?
        <Skeleton /> //showing skelaton is the data is loading
        :
        <div className='container4111'>

          {
            // rendering the SelectAddress modal
            selectAddress === true && <SelectAddress selectAddress={selectAddress} setSelectAddress={setSelectAddress} data={data} setData={setData} state={state} setState={setState} />
          }
          <Title level={2}>
            Checkout Your Order
          </Title>
          <Divider />

          <Row gutter={[10, 0]}>
            <Col span={15}>
              <div className="innerContainer411">
                <Title level={3}>
                  Select Shipping Address
                </Title>
                <Divider />
                {
                  Object.keys(data.address).length > 1 ?
                    <>
                      <Title level={5}>Will be delivered to address -</Title>
                      <p>
                        {data.address.name} <br />
                        {data.address.addressLine1} <br />
                        {data.address.addressLine2} <br />
                        {data.address.city} <br />
                        {data.address.state} <br />
                        {data.address.pincode} <br />
                        {
                          data.contact === 0 || data.contact === null ?
                            <a onClick={() => setState({ ...state, contact: !state.contact })}>Please Enter Contact Number To Continue!</a>
                            :
                            "+91 " + data.contact
                        }
                      </p>
                      {
                        state.contact === true && <Contact onFinish={changeContactHandler} />
                      }
                      {
                        selectAddress === false && <a onClick={() => setSelectAddress(true)}>Change Delivery Address</a>
                      }
                    </>
                    :
                    // If no address found, this will be shown */}
                    <PaymentAddress data={data} setData={setData} />
                }
              </div>

              {/* Payment Methods */}
              <div className="innerContainer411" style={{ marginTop: "20px" }}>
                <Title level={3}>
                  Select Payment Method
                </Title>
                <Divider />

                <Radio.Group name="paymentMethod" defaultValue={4}>
                  <Radio value={1} disabled>
                    <Title level={5}>
                      Add Debit/Credit/ATM Card
                    </Title>
                    <p>You can save your cards as per new RBI guidelines.</p>
                    <div>
                      <Image preview={false} src={visa} className="paymentCardImg41" />
                      <Image preview={false} src={masterCard} className="paymentCardImg41" />
                      <Image preview={false} src={americanExpress} className="paymentCardImg41" />
                      <Image preview={false} src={citiBank} className="paymentCardImg41" />
                      <Image preview={false} src={rupay} className="paymentCardImg41" />
                    </div>
                    <p style={{ color: "blue" }}>
                      + Add Credit or Debit card here
                    </p>
                  </Radio>
                  <br />
                  <Radio value={2} style={{ marginTop: "20px" }} disabled>
                    <Title level={5}>
                      Net Banking
                    </Title>
                  </Radio>

                  <br />
                  <Radio value={3} style={{ marginTop: "20px" }} disabled>
                    <Title level={5}>
                      Other UPI Options
                    </Title>
                  </Radio>

                  <Radio value={4} style={{ marginTop: "20px" }}>
                    <Title level={5}>
                      Pay On Delivery
                    </Title>
                    <p>
                      Pay digitally with SMS Pay Link. Cash may not be accepted in COVID restricted areas.
                    </p>
                  </Radio>
                </Radio.Group>
              </div>

              {/* Order Details */}
              <div className="innerContainer411" style={{ marginTop: "20px" }}>
                <Title level={3}>
                  Order Items
                </Title>
                <Divider />
                <Title level={5} style={{ color: "green" }}>
                  Guaranteed delivery: {getDate()}
                </Title>

                {/* showing selected products */}
                {
                  data.products.map(element => {
                    return <div className="productDetails4323">
                      <Row>
                        <Col span={4}>
                          <Image src={url + `/images/${element.img}`} preview={false} width={100} height={100} />
                        </Col>
                        <Col offset={1}>
                          <p style={{ marginTop: "20px", marginBottom: "2px" }}>
                            <b>
                              {element.brandName}
                            </b>
                          </p>

                          <Title level={4}>
                            {element.productName}
                          </Title>

                          <p style={{ marginTop: "1px", color: "green", marginBottom: '0px', fontSize: "medium" }}>
                            <Tag color="#f50">
                              {element.tag}
                            </Tag>
                          </p>

                          <Statistic valueStyle={{ fontSize: "12px", color: "red", marginBottom: 2 }} value={element.deletedPrice} precision={2} prefix={'RS'} />
                          <Statistic valueStyle={{ fontSize: "18px", fontWeight: "bold", color: "green" }} value={element.price} precision={2} prefix={'RS'} />


                          <p>
                            <b>
                              Quantity:  {element.qty}
                            </b>
                          </p>
                        </Col>
                      </Row>
                    </div>

                  })//end map
                }
              </div>

            </Col>

            <Col span={6}>
              <div className="innerContainer411">
                <Title level={5}>
                  Order Summary
                </Title>
                <Divider />

                <Row>
                  <Col>
                    Total:
                  </Col>
                  <Col offset={13}>
                    <Statistic value={data.grandTotal} precision={2} prefix={"RS. "} valueStyle={{ fontSize: "small" }} />
                  </Col>
                </Row>
                <Row>
                  <Col>
                    Delivery Fees:
                  </Col>
                  <Col offset={9}>
                    RS: {data.grandTotal >= 1000 ? "0.00" : "50.00"}
                  </Col>
                </Row>

                <Divider />

                <Title style={{ color: "rgba(255, 94, 0, 0.945)" }} level={5}>
                  <Row>
                    <Col>
                      Order Total:
                    </Col>
                    <Col offset={5}>
                      <Statistic value={parseInt(data.grandTotal) + (data.grandTotal >= 1000 ? 0 : 50)} precision={2} prefix={"RS. "} valueStyle={{ color: "rgba(255, 94, 0, 0.945)", fontSize: "medium" }} />
                    </Col>
                  </Row>
                </Title>

                <Divider />
                <Popover content={(Object.keys(data.address).length === 0 && "Please Select Address To Continue Processing.") || (data.products.length === 0 && "You Have Not Selected Any Product Yet To Buy. Please Select A Product To Continue") || (data.contact === 0 && "Enter Your Contact To Continue") || "Click Here To Continue"}>
                  <Button disabled={Object.keys(data.address).length < 2 || data.products.length === 0 || data.contact === 0 ? true : false} type="secondary" style={{ width: "250px" }} onClick={onProceedHandler}>Proceed</Button>
                </Popover>

                <Popover content="Cancel Order" placement='bottom'>
                  <Button type="danger" style={{ width: "250px", marginTop: "10px" }} onClick={() => history.goBack()}>Cancel</Button>
                </Popover>

              </div>
            </Col>
          </Row>
          <Divider />
        </div>
    }
  </>
  )
}
