import React, { useState, useEffect } from 'react'
import "./Dashboard.css";
import { Link } from 'react-router-dom';
import { Typography, Skeleton } from 'antd';
import CategoryCard from '../../Components/CategoryCard/CategoryCard';
import ProductCard from '../../Components/ProductCard/ProductCard';
import PeopleSearch from '../../Components/PeopleSearch/PeopleSearch';
import url from "../../Components/httpRequests/httpRequest";

import banner from './Category Images/banner2.jpg';

// import images for category
import category1 from "./Category Images/category1.jpg";
import category2 from "./Category Images/category2.jpg";
import category3 from "./Category Images/category3.jpg";
import category4 from "./Category Images/category4.jpg";
import category5 from "./Category Images/category5.jpg";
import category6 from "./Category Images/category6.jpg";

// creating image object to iterate while rendering category
const imageObj = {
  1: category1,
  2: category2,
  3: category3,
  4: category4,
  5: category5,
  6: category6,
}

const { Title } = Typography;


export default function Dashboard() {

  // setting state for loading
  const [isLoading, setIsLoading] = useState(true);

  // setting state for data
  const [data, setData] = useState({});

  // getting data for dashboard
  const getData = async () => {
    const response = await fetch(url + `/dashboard`);
    const result = await response.json();

    // setting data in state
    setData({
      ...data,
      category: result.category,
      products1: result.products.slice(1, 13),
      products2: result.products.slice(13, 21)
    });

    setIsLoading(false);


  }//end getData

  useEffect(() => {
    getData();
    return () => {
      // scrolling smoothly to the top of the page when props or id changes, using component didUnmount function.
      window.scrollTo({
        top: 0, 
        behavior: 'smooth'
      });
    };
  }, [])

  return (
    <>
      {
        isLoading === true ?
          <Skeleton />
          :
          <>
            {/* Banner */}
            <div className='banner2'>
              {/* <Carousel autoplay> */}
                <div>
                  {/* <h2 style={contentStyle}>1</h2> */}
                  <img  src={banner} width='100%' height='400px'/>
                  {/* <div className='text-block'>
                    <h1 style={{ color: 'white', textShadow: '2px 2px 4px #000000', textAlign: 'left' }}>We connect <h1>Buyers & Sellers</h1></h1>
                  </div> */}
                </div>
                {/* <div>
                  <h3 style={contentStyle}>2</h3>
                </div>
                <div>
                  <h3 style={contentStyle}>3</h3>
                </div>
                <div>
                  <h3 style={contentStyle}>4</h3>
                </div> */}
              {/* </Carousel> */}
            </div>


            {/* Categories */}
            <div className='division2'><br />
              <Title className='title2' level={2}>
                Our Categories
              </Title>
              <div className='wrapper'>


                {
                  // setting category
                  data.category.map(element => {
                    return <Link to={`/products/category?id=${element.id}`}>
                      <div className='item'>
                        <CategoryCard
                          img={imageObj[element.id]}
                          alt={element.name}
                          category={element.name}
                        />
                      </div>
                    </Link>
                  })//end map function
                }

              </div>
            </div>



            {/* products */}
            <div className='division2'><br />
              <Title className='title2' level={2}>
                Our Products
              </Title><br />
              <div className='products2'>
                {
                  data.products1.map(product => {
                    return <div className='item2'>
                      <ProductCard
                        id={product.id}
                        img={url + `/images/${product.imageURL}`}
                        tag={product.tag}
                        brand={product.brandName}
                        title={product.productName}
                        price={product.price}
                        deletedPrice={product.deletedPrice}
                      />
                    </div>
                  })
                }

              </div>

            </div>


            {/* people also search for */}
            <PeopleSearch title="Recommended Products" products={data.products2} />
          </>
      }
    </>


  )
}
