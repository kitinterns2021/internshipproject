import React from 'react';
import "./Search.css";

const Searchbar = () => {
  return (
    <div class="search-box">
      <input className='search-txt' type="text" placeholder="Search..." name=''/>
        <a className='search-btn' href='#'>
          <i class='fas fa-search'></i>
        </a>
    </div>
  );
};

export default Searchbar;
