import React, { useState, useEffect } from 'react'
import { Layout, Menu, Dropdown, Row, Col } from 'antd';
import { Route, Switch, Link } from "react-router-dom";
// import "antd/dist/antd.css";
import "./Navigation.css";
import "../searchBar/Search.css";
import Searchbar from '../searchBar/Searchbar';
import PersonalProfile from '../../Accounts/PersonalProfile/PersonalProfile';
import { ShoppingCartOutlined, AppstoreOutlined, UserAddOutlined, HomeOutlined, CopyrightOutlined } from '@ant-design/icons';
import Account from '../../Accounts/Account';
import Addresses from '../../Accounts/Addresses/Addresses';
import LoginSecurity from '../../Accounts/LoginSecurity/LoginSecurity';
import ChangePassword from '../../Accounts/LoginSecurity/ChangePassword';
import AddressTable from '../../Accounts/Addresses/AddressTable';
import Login from '../../Signup/Signup';
import PurchaseHistory from '../../Accounts/PurchaseHistory/PurchaseHistory';
import Signup from '../../Signup/Signup';
import Signin from "../../Signup/Signin";
import HistoryPage from '../../Accounts/PurchaseHistory/HistoryPage';
import Dashboard from '../Dashboard';
import Cart from "../../Cart/Cart";
import SingleProduct from '../../Products/Single-Product/SingleProduct';
import Payment from '../../Payment/Payment';
import RelatedProducts from "../../Products/Related-Products/RelatedProduct"
import url from '../../../Components/httpRequests/httpRequest';
const { Content, Footer } = Layout;

export default function Navigation() {

    const [isLoggedIn, setIsLoggedIn] = useState(localStorage.getItem("userId") ? true : false);

    // setting state for the data to show in categories and subcatergories
    const [data,setData] = useState({categories:[],subCategories:[]});

    // getting categories and sub categories
    const getData = async () => {
        const response = await fetch(url + `/dashboard/nav-data`);
        const result = await response.json();
        setData({
            ...data,
            categories: result.categories,
            subCategories: result.subCategories
        });
    }//end getData

    useEffect(()=>{
        getData();
    },[])


    const menu = () => {
        return (
            <Menu>
            <Row>
                <Col>
                    <Menu.ItemGroup title="Categories">
                        {
                            data.categories.map(element => {
                                return <Link to={`/products/category?id=${element.id}`}>
                                    <Menu.Item>{element.name}</Menu.Item>
                                </Link>
                            })
                        }
                    </Menu.ItemGroup>
    
                </Col>
                <Col>
                    <Menu.ItemGroup title="Sub Categories">
                    {
                            data.subCategories.map(element => {
                                return <Link to={`/products/subcategory?id=${element.id}`}>
                                    <Menu.Item>{element.name}</Menu.Item>
                                </Link>
                            })
                        }
                    </Menu.ItemGroup>
                </Col>
            </Row>
        </Menu>
        )
    };


    return (
        <div>
            <Layout>
                <header>
                    <nav className="navbar2" >
                        <h3 className="logo">
                            <b>Fathom</b>
                        </h3>
                        <ul className="nav-links">
                            <li><Searchbar /></li>
                            <Link to="/" className="home">
                                <li><HomeOutlined style={{ fontSize: "20px", width: "50px" }} />Home</li>
                            </Link>
                            {/* <Link to="/category" className="category"> */}
                            <Dropdown overlay={() => menu()} trigger={['hover']}>
                                <li className="category">
                                    <AppstoreOutlined style={{ fontSize: "20px", width: "50px" }} />
                                    Categories
                                </li>
                            </Dropdown>
                            {/* </Link> */}
                            {/* <Link to="/services" className="services">
                                <li><BarsOutlined style={{ fontSize: "20px", width: "50px" }} />Services</li>
                            </Link> */}

                            {
                                !isLoggedIn ?
                                    <Link to="/sign-up" className="signup">
                                        <li><UserAddOutlined style={{ fontSize: "20px", width: "50px" }} />Sign Up</li>
                                    </Link>
                                    :
                                    <Link to="/account" className="signup">
                                        <li><UserAddOutlined style={{ fontSize: "20px", width: "50px" }} />Account</li>
                                    </Link>
                            }

                            <Link to="/cart" className="cart">
                                <li><ShoppingCartOutlined style={{ fontSize: "20px", width: "50px" }} />Cart</li>
                            </Link>

                        </ul>
                    </nav>
                </header>
                {/* </Header> */}
                <div>
                    <Content className='Content2'>
                        {/* Defining routers temperory */}
                        <Switch>
                            <Route exact path="/"><Dashboard /></Route>
                            <Route exact path="/account">
                                <Account setIsLoggedIn = {setIsLoggedIn}/>
                            </Route>

                            <Route exact path="/login">
                                <Login />
                            </Route>
                            <Route exact path="/sign-in">
                                <Signin setIsLoggedIn={() => setIsLoggedIn(!isLoggedIn)} />
                            </Route>
                            <Route exact path="/sign-up">
                                <Signup setIsLoggedIn={() => setIsLoggedIn(!isLoggedIn)} />
                            </Route>

                            <Route exact path="/cart">
                                <Cart />
                            </Route>

                            <Route exact path="/account/personal-profile">
                                <PersonalProfile />
                            </Route>
                            <Route exact path="/account/your-addresses">
                                <AddressTable />
                            </Route>
                            <Route exact path="/add-address">
                                <Addresses />
                            </Route>
                            <Route exact path="/update-address">
                                <Addresses />
                            </Route>
                            <Route exact path="/account/login-security">
                                <LoginSecurity />
                            </Route>
                            <Route exact path="/change-password">
                                <ChangePassword />
                            </Route>
                            <Route exact path="/account/purchase-history">
                                <PurchaseHistory />
                            </Route>
                            <Route exact path={`/account/purchase-history/history-page/:orderItemId`}>
                                <HistoryPage />
                            </Route>


                            {/* Rendering products here */}
                            <Route exact path="/product/:productId">
                                <SingleProduct />
                            </Route>

                            {/* Rendering products related with categories */}
                            <Route exact path="/products/category">
                                <RelatedProducts />
                            </Route>

                            {/* Rendering products related with sub categories */}
                            <Route exact path="/products/subcategory">
                                <RelatedProducts />
                            </Route>


                            {/* Routing to the payments */}
                            <Route exact path="/payment">
                                <Payment />
                            </Route>

                        </Switch>
                    </Content>
                </div>
                <Footer style={{ textAlign: 'center' }}><h4><CopyrightOutlined />2021 Made By Fathom Software Pvt. Ltd.</h4></Footer>

            </Layout>


        </div>
    )
}
