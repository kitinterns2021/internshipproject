import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom';
import { Form, Button, Card, Typography, notification } from 'antd';
import "antd/dist/antd.css";
import { Content } from 'antd/lib/layout/layout';
import "../Addresses/Form.css";
import EmailComponent from "../../../Components/EmailComponent/EmailComponent";
import OnlyPassword from '../../../Components/OnlyPassword/OnlyPassword';
import url from '../../../Components/httpRequests/httpRequest';
import ChangePassword from './ChangePassword';

const { Title } = Typography;


export default function LoginSecurity() {

    const [form] = Form.useForm()

    const formItemLayout = {

        labelCol: {
            xs: {
                span: 24,
            },
            sm: {
                span: 4,
            },
        },
        wrapperCol: {
            xs: {
                span: 24,
            },
            sm: {
                span: 16,
            },
        },

    };

    const history = useHistory();
    const [data, setData] = useState(false);

    // setting state for changing the password
    const [isTrue, setIsTrue] = useState(false);

    //setting state for showing error
    const [isError, setIsError] = useState(false);

    const getData = async () => {
        const response = await fetch(url + `/login-info?id=${localStorage.getItem("userId")}`);
        const rows = await response.json();
        setData({
            ...data,
            emailId: rows[0].emailId
            // password: rows[0].password
        });
        // console.log(rows[0]);
        form.setFieldsValue({ emailId: rows[0].emailId });

    }

    useEffect(() => {
        getData();

    }, []
    )

    const onChangeHandler = (name, e) => {
        setData({
            ...data,
            [name]: e.target.value
        })
    }

    
    const onFinish = async () => {
        const dataObj = {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(data)
        }
        const response = await fetch(url + `/change-password`, dataObj);
        const result = await response.json();
        console.log(result);

        if (result.res === true) {
            setIsTrue(true);
        }
        else {
            setIsError(true);
            setTimeout(()=>{
                setIsError(false);
            }, 5000);
        }

        // console.log(data.emailId);
        // console.log(data.password);
    
    }


    return (
        <>
            {
                isTrue ?

                    <ChangePassword />
                    :
                    <Content>
                        < Form {...formItemLayout} form={form} name="register" onFinish={onFinish} scrollToFirstError style={{ marginTop: "2rem" }
                        }>
                            

                            <Card hoverable="true" className="card2">
                            <Title level={2} style={{ textAlign: "left" }}><u>Login & Security</u></Title><br/><br/>
                            
                                <p><Title level={5}>If you want to change the password associated with your email, you may do so below.</Title></p>
                                <br />
                                <EmailComponent name="emailId" readOnly={true} label="Email" onChange={onChangeHandler} value={data.emailId ? data.emailId : ""} />
                                <OnlyPassword name="password" label="Password" onChange={onChangeHandler} />
                                {
                                    isError && <p style={{color: "red", marginLeft: "140px"}}>Entered password does'nt match, please enter correct password.</p>
                                }
                                <Form.Item
                                    wrapperCol={{
                                        offset: 4,
                                        span: 16,
                                    }}
                                ><br />
                                    <Button type="primary" htmlType="submit" >
                                        Edit Password
                                    </Button>
                                    <Button type="primary" onClick={() => { history.goBack() }} style={{ marginLeft: "2rem" }}>
                                        Back
                                    </Button><br /><br />
                                </Form.Item>

                            </Card>
                        </Form >
                    </Content >
            }
        </>
    )
}
