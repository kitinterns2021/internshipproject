import React, { useState, useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom';
import { Form, Button, Card, Typography, Input, notification } from 'antd';
import "antd/dist/antd.css";
import { Content } from 'antd/lib/layout/layout';
import "../Addresses/Form.css";
import Password from '../../../Components/Password/Password';
import url from '../../../Components/httpRequests/httpRequest';


const { Title } = Typography;


export default function ChangePassword() {

    const history = useHistory();

    const [form] = Form.useForm()

    const formItemLayout = {

        labelCol: {
            xs: {
                span: 24,
            },
            sm: {
                span: 4,
            },
        },
        wrapperCol: {
            xs: {
                span: 24,
            },
            sm: {
                span: 16,
            },
        },

    };

    const [data, setData] = useState({
        userId: localStorage.getItem("userId"),
        password: ""
    })

    const getData = async (id) => {
        const response = await fetch(url + `/login-info?id=${localStorage.getItem("userId")}`);
        const rows = await response.json();
        setData({
            ...data,
            // id: rows[0].id,
            emailId: rows[0].emailId
        });
        console.log(rows[0]);
        // form.setFieldsValue({...rows[0]});
    }

    useEffect(() => {
        // if (searchParams.get("id")) {
        getData(localStorage.getItem("userId"));
        // }
    }, [])

    //handling onChange event
    const onChangeHandler = (e) => {
        setData({
            ...data,
            [e.target.name]: e.target.value
        });
    }

    // notification for confirmation
    const openNotificationWithIcon = (msg) => {
        notification.success({
            message: "Success!",
            description: msg
        });
    };


    const onFinish = async () => {
            const dataObj = {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
                body: JSON.stringify(data)
            }
            const response = await fetch(url + `/update-password`, dataObj);
            const result = await response.text();
            if (result) {
                openNotificationWithIcon("Password Reset Successfull...");
                history.goBack();
                console.log(result);
            }
            else {
                openNotificationWithIcon("Something went wrong. Please try again...");
            }
        console.log(data);
    };
    return (
        <Content>
            <Form {...formItemLayout} form={form} name="register" onFinish={onFinish} scrollToFirstError style={{ marginTop: "2rem" }}>
                

                <Card hoverable="true" className="card2">
                <Title level={2} style={{ textAlign: "left" }}><u>Change Password</u></Title><br /><br />
                    <p><Title level={5}>Use the form below to change the password for your account.</Title></p>
                    <br />
                    <Form.Item
                        name="password"
                        label="New Password"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your new password!',
                            },
                        ]}
                        hasFeedback
                    >
                        <Input.Password name="password" style={{ width: "70%" }} placeholder='Enter new password' onChange={onChangeHandler} value={data.password} />
                    </Form.Item>
                    <Form.Item
                        name="confirm"
                        label="Confirm Password"
                        dependencies={['password']}
                        hasFeedback
                        rules={[
                            {
                                required: true,
                                message: 'Please confirm your password!',
                            },
                            ({ getFieldValue }) => ({
                                validator(_, value) {
                                    if (!value || getFieldValue('password') === value) {
                                        return Promise.resolve();
                                    }

                                    return Promise.reject(new Error('The two passwords that you entered do not match!'));
                                },
                            }),
                        ]}
                    >
                        <Input.Password style={{ width: "70%" }} placeholder='Re-enter above password' />
                    </Form.Item>
                    <Form.Item
                        wrapperCol={{
                            offset: 4,
                            span: 16,
                        }}
                    ><br />
                        <Button type="primary" htmlType="submit">
                            Save Changes
                        </Button>
                        <Button type="primary" onClick={() => { history.goBack() }} style={{ marginLeft: "2rem" }}>
                            Back
                        </Button><br /><br />
                    </Form.Item>

                </Card>
            </Form>
        </Content>

    )
}
