import React, { useState, useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom';
import InputComponents from '../../../Components/InputComponents/InputComponents';
import { Form, Button, Card, Typography, notification, DatePicker, Row, Col } from 'antd';
import "antd/dist/antd.css";
import { Content } from 'antd/lib/layout/layout';
import RadioComponent from '../../../Components/RadioComponent/RadioComponent';
import PhoneNumber from '../../../Components/PhoneNumber/PhoneNumber';
import "../Addresses/Form.css";
import url from '../../../Components/httpRequests/httpRequest';
import moment from "moment";

const { Title } = Typography;
const dateFormat = 'YYYY/MM/DD';
// const customFormat = value => `custom format: ${value.format(dateFormat)}`;


export default function PersonalProfile() {

    const [form] = Form.useForm()

    const formItemLayout = {

        labelCol: {
            xs: {
                span: 24,
            },
            sm: {
                span: 4,
            },
        },
        wrapperCol: {
            xs: {
                span: 24,
            },
            sm: {
                span: 16,
            },
        },

    };

    const history = useHistory();

    const [data, setData] = useState(false);

    const getData = async (id) => {
        const response = await fetch(url + `/user-info?id=${id}&userId=${localStorage.getItem("userId")}`);
        const rows = await response.json();
        setData({
            ...data,
            userId: localStorage.getItem("userId"),
            name: rows[0].name,
            contact: rows[0].contact,
            gender: rows[0].gender,
            dob: moment(rows[0].dob,"YYYY/MM/DD")
        });
        // console.log(rows[0]);
        form.setFieldsValue({ ...rows[0],dob:moment(rows[0].dob,"YYYY/MM/DD") });
    }

    // getting location object of the url and getting the query parameters from it
    // const location = useLocation();

    // //searchParams are query parameters
    // const searchParams = new URLSearchParams(location.search);

    useEffect(() => {
        getData();

    }, []
    )

    const onChangeHandler = (name, e) => {
        setData({
            ...data,
            [name]: e.target.value
        })
    }

    // notification for confirmation
    const openNotificationWithIcon = (msg) => {
        notification.success({
            message: "Success!",
            description: msg
        });
    };


    const onFinish = async () => {
        const dataObj = {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            },
            body: JSON.stringify(data)
        }
        const response = await fetch(url + `/user-info/update-user-info`, dataObj);
        const result = await response.text();
        if (result) {
            openNotificationWithIcon("Record Updated Successfully...");
            history.goBack();
            console.log(result);
        }
        else {
            openNotificationWithIcon("Something went wrong. Please try again...");
        }

        // console.log(data);
    };


    return (

        <Content >
            <Form {...formItemLayout} form={form} name="register" onFinish={onFinish} scrollToFirstError style={{ marginTop: "2rem" }}>
                <Card hoverable="true" className="card2">
                    <Title level={2} style={{ textAlign: "left" }}><u>Change Your Personal Information</u></Title><br /><br />
                    <p><Title level={5}>If you want to change the personal information associated with your customer account, you may do so below.<br />Be sure to click the <b>Save Changes</b> button when you are done.</Title></p>
                    <br />
                    <InputComponents label="Name" onChange={onChangeHandler} name="name" message="Please enter your name..." placeholder="Enter name here" value={data.name ? data.name : ""} />
                    <PhoneNumber label="Contact" maxLength='10' minLength='10' onChange={onChangeHandler} name="contact" message="Please enter your phone number..." placeholder="Enter phone number here" value={data.contact ? data.contact : ""} />
                    <RadioComponent label="Gender" name="gender" message="Please select your gender..."
                        onChange={(value) => { setData({ ...data, gender: value }) }}
                        defaultValue={data.gender !== "" ? data.gender : ""}
                    />
                    {/* <DateFun name="dob" value={data.dob ? data.dob : ""} onChange={date => setData({ ...data, [data.dob]: date })}  /> */}

                    <Form.Item
                        name="dob"
                        label="Date Of Birth"
                        rules={[
                            {
                                required: false,
                                message: 'Please input your birth date!',
                            },
                        ]}
                    >
                        {/* <Row>
                            <Col>
                                <InputComponents name="dob" readOnly={true} style={{width:'300px'}} />
                            </Col>
                            <Col> */}
                                <DatePicker
                                    defaultValue={data.dob}
                                    onChange={date => setData({ ...data, dob: date['_d'] })}
                                    style={{width:"325px"}}
                                    
                                />
                            {/* </Col>
                        </Row> */}

                    </Form.Item>
                    <Form.Item
                        wrapperCol={{
                            offset: 4,
                            span: 16,
                        }}
                    ><br />
                        <Button type="primary" htmlType="submit">
                            Save Changes
                        </Button>
                        <Button type="primary" onClick={() => { history.goBack() }} style={{ marginLeft: "2rem" }}>
                            Back
                        </Button><br /><br />
                    </Form.Item>
                </Card>
            </Form>
        </Content>

    )
}
