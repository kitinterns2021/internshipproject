import React, { useState, useEffect } from 'react'
import { Card, Row, Col, Divider, Statistic, Button, Modal, notification } from 'antd';
import "./PurchaseHistory.css";
import { useHistory, useParams, Link } from 'react-router-dom';
import { DeliveredProcedureOutlined, DollarOutlined, PhoneOutlined, MailOutlined } from '@ant-design/icons';
import url from '../../../Components/httpRequests/httpRequest';


export default function HistoryPage() {


    // creating history object
    const history = useHistory();

    // getting query params
    const { orderItemId } = useParams();

    // checking if the user is logged in or not
    if (!localStorage.getItem("userId")) {
        // pushing user to the login page if not logged in or visited this page accendently
        history.push("/sign-up");
    }//end if

    // setting state for data
    const [data, setData] = useState({userId:localStorage.getItem("userId")});

    // getting data of the order item
    const getData = async () => {

        const response = await fetch(url + `/purchaseHistory/order-item?userId=${localStorage.getItem("userId")}&orderItemId=${orderItemId}`);
        const result = await response.json();
        // console.log(result)

        // changing order status before updating the state
        switch (result.status) {
            case "BOK":
                result["status"] = "Booked"
                break;
            case "DIS":
                result["status"] = "Dispatched"
                break; case "DLI":
                result["status"] = "Delivered"
                break; case "CNL":
                result["status"] = "Cancelled"
                break;
            default:
                break;
        }

        setData({...data,...result});

    }//end getData()

    useEffect(() => {
        getData();
    });

    // creating modal for cancel button click event
    const openModelHandler = () => {
        return Modal.error({
            title: <h3>You are about to cancel this order!</h3>,
            content: (
                <>
                    <h3>Cancel Order</h3>
                    {/* <h4>
                        Product: {data.productName} <br />
                        Quantity: {data.qty} <br />
                        <Row>
                            <Col>
                                Order Total:
                            </Col>
                            <Col offset={1}>
                                <Statistic value={data.price} precision={2} prefix="RS. " valueStyle={{ fontSize: "13px", marginTop:"2px" }} />
                            </Col>
                        </Row>
                    </h4> */}
                    <h4>
                        Cancelling this order item will also cancel all the other items present in this order.<br/>
                        Are you sure that you want to cancel this order?
                    </h4>
                    <br/>
                    <br/>
                    <h6 style={{color:"red"}}>
                        Cancelling the order can take a couple of seconds. Please wait for the some time after clicking Proceed.
                    </h6>
                </>
            ),
            closable: true,
            okType: "danger",
            okText: "Proceed",
            onOk: onCancelHandler
        })
    }//end openModelHandler

    // cancelling the order
    const onCancelHandler = async () => {

        const dataObj = {
            method:"PUT",
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json"
            },
            body:JSON.stringify(data)
        }
        const response = await fetch(url + `/payment/cancel-order`, dataObj);
        const result = await response.json();

        if(result.err)
        {
            notification.error({
                message:"Something went wrong.",
                description:"Please try after some time."
            })
        }
        else if(result.success === true)
        {
            notification.info({
                message:"Order Cancelled!",
                description:"Your Order Has Been Cancelled."
            })
        }

    }//end onCancelHandler

    return (
        <div>
            <div >
                <Card className="card2" style={{ backgroundColor: "whitesmoke" }}>
                    <Row align='center'>
                        <img src={url + `/images/${data.imageURL}`} alt='Laptop' width="270px" height="250px" />
                    </Row>
                    <Row align='center' >
                        <div className='div23'>
                            <Link to={`/product/${data.productId}`}>
                                <h2>
                                    {data.productName}
                                </h2>
                            </Link>
                            <p>Product Quantity - {data.qty}</p>
                        </div>
                    </Row>
                    <Row align='center'>
                        <div className='div24'>
                            <DeliveredProcedureOutlined style={{ marginLeft: "15px", marginRight: "15px", fontSize: "20px" }} />
                            <b>
                                {
                                    // displaying order status here
                                    data.status
                                }
                            </b>
                            <p style={{ marginLeft: "50px", color: "lightgray" }}>On {data.updatedDate}</p>
                        </div>
                    </Row>
                    <div className='div25'>
                        <Row><h3>Delivery Address</h3></Row>
                        <Row>
                            <Col><h4>{data.userName}</h4> </Col>
                            <Col offset={2}><h4>{data.contact}</h4></Col>
                        </Row>
                        <p>{
                            data.addressLine1 + ", " + data.addressLine2 + ", " + data.city + ", " + data.state + ". " + data.city + " - " + data.pincode
                        }</p>
                        {/* <p>
                            {data.addressLine1}
                            <br/>
                            {data.addressLine2}
                            <br/>
                            {data.city}
                            <br/>
                            {data.state}
                            <br/>
                            {data.city + " - " + data.pincode}
                            <br/>
                        </p> */}
                    </div>
                    <div className='div25'>
                        <Row>
                            <Col><h3>Total Order Price</h3></Col>
                            <Col offset={1}>
                                <Row>
                                    <Col>
                                        <Statistic value={data.price} prefix="RS. " precision={2} valueStyle={{ color: "green", fontSize: "large", marginTop: "3px", fontWeight: "bold" }} />
                                    </Col>
                                    <Col>
                                        <Statistic value={data.deletedPrice} prefix="RS. " precision={2} valueStyle={{ color: "red", fontSize: "smaller", marginTop: "8px", marginLeft: "2px" }} />
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                        <Row>
                            <p>You saved Rs. {data.youSaved} on this order.</p>
                        </Row>
                        <Row>
                            <div className='div26'>
                                <DollarOutlined style={{ marginLeft: "15px", marginRight: "15px", fontSize: "20px" }} />Pay on delivery.
                            </div>
                        </Row>
                        <Divider />
                        <p>Item sold by: {data.brandName}</p>
                    </div>
                    <div className='div25'>
                        <Row>
                            <h3>Updates sent to</h3>
                        </Row>
                        <Row>
                            <p><PhoneOutlined style={{ marginLeft: "5px", marginRight: "15px", fontSize: "20px" }} />{data.contact}</p>
                        </Row>
                        <Row>
                            <p><MailOutlined style={{ marginLeft: "5px", marginRight: "15px", fontSize: "20px" }} />{data.emailId}</p>
                        </Row>
                    </div>
                    {
                        // cancel button will be visible only if the order is not delivered yet
                        data.status !== ("Delivered" && "Cancelled") &&
                        <Row>
                            <Button type='danger' style={{ width: "100%" }} onClick={openModelHandler}>
                                Cancel Order
                            </Button>
                        </Row>
                    }
                    <Row>
                        <Button type='secondary' style={{ width: "100%", marginTop: "10px" }} onClick={() => history.goBack()}>
                            Back
                        </Button>
                    </Row>
                </Card>
            </div>
        </div>
    )
}
