import React, { useState, useEffect } from 'react'
import "antd/dist/antd.css";
import { Col, Row, Typography, Divider } from 'antd';
import "./PurchaseHistory.css";
import { Link } from "react-router-dom";
import PurchaseHistoryCard from '../../../Components/PurchaseHistoryCard/PurchaseHistoryCard';
import url from '../../../Components/httpRequests/httpRequest';


const { Title } = Typography;

export default function PurchaseHistory() {

    const [data, setData] = useState([]);

    const getData = async () => {
        const response = await fetch(url + `/purchaseHistory/orders?userId=${localStorage.getItem("userId")}`);
        const rows = await response.json();
        console.log(rows);
        setData(rows);
    }

    useEffect(
        () => {
            getData();
        }, []
    )


    return (
        <div>
            <div className="div22">
                <div className="div21">
                    <Title level={3} >Showing all orders</Title>
                </div>
                <Divider />
                <div style={{ marginTop: "20px", marginBottom: "15px" }}>
                    <Row >
                        <Col span={20} >
                            {
                                data.map(element => {
                                    return <Link to={`/account/purchase-history/history-page/${element.orderItemId}`} style={{ color: "black" }}>
                                        <PurchaseHistoryCard
                                            src={url + `/images/${element.imageURL}`}
                                            tag={element.tagName}
                                            brand={element.brandName}
                                            productName={element.productName}
                                            orderStatus={(element.status === "BOK" && "Booked") || (element.status === "DIS" && "Dispatched") || (element.status === "DLI" && "Delivered") || (element.status === "CNL" && "Cancelled")}
                                            city={element.city}
                                        />
                                    </Link>
                                })//end map function
                            }
                        </Col>
                    </Row>
                </div>
            </div>
        </div>
    )
}
