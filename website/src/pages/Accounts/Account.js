import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import "antd/dist/antd.css";
import { Card, Typography, Row, Col, Button, Popconfirm, message } from 'antd';
import "./Account.css";
import { CreditCardFilled, EnvironmentFilled, LockFilled, ShoppingFilled, IdcardFilled, SmileFilled, LogoutOutlined } from '@ant-design/icons';
// import { useState } from 'react/cjs/react.development';
import AccountCard from '../../Components/AccountCard/AccountCard';
import url from '../../Components/httpRequests/httpRequest';


const { Title } = Typography;
export default function Account( props ) {
    const [data, setData] = useState({});

    const getData = async (id) => {

        // checking if user id is not present and redirecting user to the login page
        if(!localStorage.getItem("userId"))
        {
            return history.push("/sign-in")
        }
        const response = await fetch(url + `/user-info?id=${id}&userId=${localStorage.getItem("userId")}`);
        const result = await response.json();
        console.log(result)
        setData(result[0])

    }

    useEffect(() => {
        getData()
    }, [])

    const history = useHistory();

    const logOutHandler = () => {
        localStorage.removeItem("userId");
        message.success("Log out successfully");
        props.setIsLoggedIn(false);
        history.push("/");
    }
    return (<>
        <Card style={{ width: '100%' }}>
            <Row>
                <Col>
                    <SmileFilled style={{ marginLeft: "2rem", fontSize: "80px", width: "80px", color: "#780650" }} />
                </Col>
                <Col style={{ marginLeft: "2rem" }}>
                    <Title level={2}>
                        Hi {data.name} !
                    </Title>
                    <Title level={4}>
                        Thanks for being our customer
                    </Title>
                </Col>
                <Col offset={16}>
                    <Popconfirm
                        placement="bottomRight"
                        title="Do you want to Log Out from the account?"
                        onConfirm={logOutHandler}
                        onCancel={() => message.error("Log out aborted!")}
                        okText="Yes"
                        cancelText="No"
                    >
                        <Button>
                            <LogoutOutlined />
                            Logout
                        </Button>
                    </Popconfirm>
                </Col>
            </Row>
        </Card>
        <div className="site-card-wrapper">
            <Row gutter={[16, 50]}>
                <AccountCard
                    icon={<ShoppingFilled style={{ fontSize: "50px", width: "50px", color: "#bae637" }} />}
                    title="Purchase History"
                    content="Return or buy things again"
                    link="/account/purchase-history"
                />

                <AccountCard
                    icon={<IdcardFilled style={{ fontSize: "50px", width: "50px", color: "#b37feb" }} />}
                    title="Manage Account"
                    content="Edit personal profile"
                    link="/account/personal-profile"
                />

                <AccountCard
                    icon={<LockFilled style={{ fontSize: "50px", width: "50px", color: "gray" }} />}
                    title="Login and Security"
                    content="Edit mail and password"
                    link="/account/login-security"
                />
            </Row>
        </div>
        <div className="site-card-wrapper">
            <Row gutter={[16, 50]}>
                <AccountCard
                    icon={<EnvironmentFilled style={{ fontSize: "50px", width: "50px", color: "orange" }} />}
                    title="Addresses"
                    content="Edit addresses for orders and gifts"
                    link="/account/your-addresses"
                />

                <AccountCard
                    icon={<CreditCardFilled style={{ fontSize: "50px", width: "50px", color: "#40a9ff" }} />}
                    title="Payment Options"
                    content="Edit or add payment options"
                    link="#"
                />
            </Row>
        </div>
    </>
    )
}
