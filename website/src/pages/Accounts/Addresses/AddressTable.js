import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom';
import { Table, Button, Typography, message } from 'antd';
import "./AddressTable.css";
import { Link } from "react-router-dom";
import url from '../../../Components/httpRequests/httpRequest';
import ButtonTable from '../../../Components/ButtonTable/ButtonTable';

const { Title } = Typography;

export default function AddressTable() {

    const history = useHistory();

    const [data, setData] = useState(false);

    //getting address from backend
    const getData = async () => {
        const response = await fetch(url + `/address-table?userId=${localStorage.getItem("userId")}`);
        const rows = await response.json();
        setData(rows);
    }

    useEffect(
        () => {
            getData();
        }, [data]
    )

    //function for deleting address
    async function confirmHandler(obj, e) {
        //update address 
        const response = await fetch(url + `/address-table/delete-address?id=${obj.id}`, { "method": "DELETE" });
        const msg = await response.json();
        console.log(e);
        message.success(msg.textMsg);
        setData(false);
    }

    function cancel(e) {
        console.log(e);
        message.error('Action aborted.');
    }

    //Defining all required columns of the table
    const columns = [
        {
            title: 'Sr.no',
            dataIndex: 'id',
            key: 'id',
            render: id => <a>{id}</a>,
        },
        {
            title: 'Address Line 1',
            dataIndex: 'addressLine1',
            key: 'addressLine1',
        },
        {
            title: 'Address Line 2',
            dataIndex: 'addressLine2',
            key: 'addressLine2',
        },
        {
            title: 'City',
            dataIndex: 'city',
            key: 'city',
        },
        {
            title: 'State',
            dataIndex: 'state',
            key: 'state',
        },
        {
            title: 'Pin Code',
            dataIndex: 'pincode',
            key: 'pincode',
        },
        {
            title: "Actions",
            key: "key",
            dataIndex: "action",
            render: (text, record) => {
                return <>
                    <Button type='primary' className='button2'>
                        <Link to={`/update-address?id=${record.id}`}>
                            Edit
                        </Link>
                    </Button>

                    {
                        <ButtonTable
                            type="danger"
                            id={record.id}
                            className="button2"
                            title="Do you want to delete this address?"
                            buttonText="Delete"
                            onConfirm={confirmHandler}
                            onCancel={cancel}
                        />
                    }
                </>
            }
        }

    ];

    return (
        <div className='container21'>
            <Title level={2} style={{ textAlign: "left" }}>Your Addresses</Title>
            <div style={{ display: "flex" }}>
                <Button type="default" htmlType="submit" style={{ marginLeft: "auto" }}>
                    <Link to="/add-address">Add Address</Link>
                </Button>
            </div>
            <br />
            <Table columns={columns} dataSource={data} /><br />
            <div style={{ display: "flex" }}>
                <Button type="default" onClick={() => { history.goBack() }} style={{ marginRight: "auto" }}>
                    Back
                </Button><br /><br />
            </div>
        </div>
    )
}
