
import React, { useState, useEffect } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { Form, Button, Card, Select, Typography, message, notification } from 'antd';
import "antd/dist/antd.css"
import "./Form.css";
import { Content } from 'antd/lib/layout/layout';
import TextArea from '../../../Components/TextAreaComponent/TextArea';
import InputComponents from '../../../Components/InputComponents/InputComponents';
import url from '../../../Components/httpRequests/httpRequest';
// const keyNameHandler = require('../../../validation/validation');
const { Option } = Select;
const { Title } = Typography;

const Addresses = () => {

    const history = useHistory();

    //setting state for data
    const [data, setData] = useState(
        {
            userId: localStorage.getItem("userId"),
            addressLine1: "",
            addressLine2: "",
            city: "",
            state: "",
            pincode: ""
        }

    );

    const [form] = Form.useForm()

    const formItemLayout = {

        labelCol: {
            xs: {
                span: 24,
            },
            sm: {
                span: 4,
            },
        },
        wrapperCol: {
            xs: {
                span: 24,
            },
            sm: {
                span: 16,
            },
        },

    };

    //getting all data
    const getAllData = async (id) => {
        const response = await fetch(url + `/get-address?id=${id}`);
        const rows = await response.json();
        setData({
            ...data,
            id: rows[0].id,
            addressLine1: rows[0].addressLine1,
            addressLine2: rows[0].addressLine2,
            city: rows[0].city,
            state: rows[0].state,
            pincode: rows[0].pincode
        })
        // console.log(rows[0]);
        form.setFieldsValue({ ...rows[0] });
    }

    // getting location object of the url and getting the query parameters from it
    const location = useLocation();

    //searchParams are query parameters
    const searchParams = new URLSearchParams(location.search);

    useEffect(() => {
        if (searchParams.get("id")) {
            getAllData(searchParams.get("id"));
        }

        return () => {
            setData({
                userId: localStorage.getItem("userId"),
                addressLine1: "",
                addressLine2: "",
                city: "",
                state: "",
                pincode: ""
            })
        }
    }, []);

    //handling onChange event
    const onChangeHandler = (name, e) => {
        setData({
            ...data,
            [name]: e.target.value
        })
    }

    // notification for confirmation
    const openNotificationWithIcon = (msg) => {
        notification.success({
            message: "Success!",
            description: msg
        });
    };

    const onFinish = async () => {
        if (searchParams.get("id")) {
            const dataObj = {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
                body: JSON.stringify({ ...data, id: searchParams.get("id") })
            }
            const response = await fetch(url + `/address-table/update-address`, dataObj);
            const result = await response.text();
            if (result) {
                openNotificationWithIcon("Record Updated Successfully...");
                history.goBack();
                console.log(result);
            }
            else {
                openNotificationWithIcon("Something went wrong. Please try again...");
            }
        }
        else {
            console.log(data);
            const dataObj = {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json"
                },
                body: JSON.stringify(data)
            }
            const response = await fetch(url + `/address-table/add-address`, dataObj);
            const result = await response.text();
            if (result) {
                openNotificationWithIcon("Record Inserted Successfully...");
                history.goBack();
                console.log(result);
            }
            else {
                openNotificationWithIcon("Something went wrong. Please try again...");
            }
        }
    }


    return (
        <Content >
            <Form {...formItemLayout} form={form} name="register" onFinish={onFinish} scrollToFirstError style={{ marginTop: "2rem" }}>
                <Card hoverable="true" className="card2">
                    <Title level={2} style={{ textAlign: "left" }}><u>{!searchParams.get("id") ? "Add Your Address" : "Update Your Address"}</u></Title><br /><br />

                    <p><Title level={5}>{!searchParams.get("id") ?
                        "If you want to add your address associated with your customer account, you may do so below." :
                        "If you want to update your address associated with your customer account, you may do so below."}
                    </Title></p>
                    <br />
                    <TextArea
                        id="addressLine1"
                        name="addressLine1"
                        label="Address Line 1"
                        message="Please enter address"
                        placeholder="Enter Address Line 1 here..."
                        whitespace="true"
                        maxLength="50"
                        onChange={onChangeHandler}
                        value={data.addressLine1 ? data.addressLine1 : ""}
                    />

                    <TextArea
                        id="addressLine2"
                        name="addressLine2"
                        label="Address Line 2"
                        message="Please enter address"
                        placeholder="Enter Address Line 2 here..."
                        whitespace="true"
                        maxLength="50"
                        onChange={onChangeHandler}
                        value={data.addressLine2 ? data.addressLine2 : ""}
                    />

                    <InputComponents
                        name="city"
                        type="text"
                        label="City"
                        message="Please enter your city"
                        placeholder="Enter your city here..."
                        maxLength="15"
                        whitespace="true"
                        onChange={onChangeHandler}
                        value={data.city ? data.city : ""}
                    />

                    <Form.Item
                        name="state"
                        label="State"
                        rules={[
                            {
                                required: true,
                                message: "Please Select Your State",
                            },
                        ]}
                    >
                        <Select placeholder="select your state" style={{ width: "50%" }} onChange={(value) => setData({ ...data, ["state"]: value })}>
                            <Option value="Andhra Pradesh">Andhra Pradesh</Option>
                            <Option value="Arunachal Pradesh">Arunachal Pradesh</Option>
                            <Option value="Assam">Assam</Option>
                            <Option value="Bihar">Bihar</Option>
                            <Option value="Chhattisgarh">Chhattisgarh</Option>
                            <Option value="Goa">Goa</Option>
                            <Option value="Gujrat">Gujrat</Option>
                            <Option value="Haryana">Haryana</Option>
                            <Option value="Himachal Pradesh">Himachal Pradesh</Option>
                            <Option value="Jharkhand">Jharkhand</Option>
                            <Option value="Karnataka">Karnataka</Option>
                            <Option value="Kerala">Kerala</Option>
                            <Option value="Madhya Pradesh">Madhya Pradesh</Option>
                            <Option value="Maharashtra">Maharashtra</Option>
                            <Option value="Manipur">Manipur</Option>
                            <Option value="Meghalaya">Meghalaya</Option>
                            <Option value="Mizoram">Mizoram</Option>
                            <Option value="Nagaland">Nagaland</Option>
                            <Option value="Odisha">Odisha</Option>
                            <Option value="Punjab">Punjab</Option>
                            <Option value="Rajsthan">Rajsthan</Option>
                            <Option value="Sikkim">Sikkim</Option>
                            <Option value="Tamil Nadu">Tamil Nadu</Option>
                            <Option value="Telangana">Telangana</Option>
                            <Option value="Tripura">Tripura</Option>
                            <Option value="Uttarakhand">Uttarakhand</Option>
                            <Option value="Uttar Pradesh">Uttar Pradesh</Option>
                            <Option value="West Bengal">West Bengal</Option>

                        </Select>

                    </Form.Item>

                    <InputComponents
                        name="pincode"
                        label="Pin-Code"
                        message="Please enter pin-code"
                        placeholder="Enter your pin-code here..."
                        style={{ width: "50%" }}
                        maxLength="6"
                        minLength="6"
                        onChange={onChangeHandler}
                        value={data.pincode ? data.pincode : ""}
                    />

                    <Form.Item
                        wrapperCol={{
                            offset: 4,
                            span: 16,
                        }}
                    ><br />
                        <Button type="primary" htmlType="submit">
                            {!searchParams.get("id") ? "Add Address" : "Save Changes"}
                        </Button>
                        <Button type="primary" onClick={() => { history.goBack() }} style={{ marginLeft: "2rem" }}>
                            Cancel
                        </Button><br /><br />
                    </Form.Item>
                </Card>
            </Form>
        </Content>



    )
}
export default Addresses;