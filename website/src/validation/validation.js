
// use this validation for input type="text" where you want to allow only characters in textfield
export const keyNameHandler = (e) =>{
    const specialCharRegex = new RegExp("[a-zA-Z]");
    const pressedKey = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if(!specialCharRegex.test(pressedKey)){
      e.preventDefault();
      return false;
    }
  }

  // use this validation for input type="number" where you want to allow only digits in textfield
  export const keyNumberHandler = (e) =>{
   const specialCharRegex = new RegExp("[0-9]");
   const pressedKey = String.fromCharCode(!e.charCode ? e.which : e.charCode);
   if(!specialCharRegex.test(pressedKey)){
     e.preventDefault();
     return false;
   }
 }

 // use this validation for input type="email"
 export const keyEmailHandler = (e) =>{
   const specialCharRegex = new RegExp("[a-zA-Z0-9@.]");
   const pressedKey = String.fromCharCode(!e.charCode ? e.which : e.charCode);
   if(!specialCharRegex.test(pressedKey)){
     e.preventDefault();
     return false;
   }
 }

 // use this validation for input type="alphanum" where you want to allow characters as well as numbers in textfield
 export const keyAplhaNumHandler = (e) =>{
  const specialCharRegex = new RegExp("[0-9a-zA-Z]");
  const pressedKey = String.fromCharCode(!e.charCode ? e.which : e.charCode);
  if(!specialCharRegex.test(pressedKey)){
    e.preventDefault();
    return false;
  }
}

// use this validation for input type="alphanum" where you want to allow characters as well as numbers in textfield
export const keyAddressHandler = (e) =>{
  const specialCharRegex = new RegExp("[a-zA-Z0-9,.-()/]");
  const pressedKey = String.fromCharCode(!e.charCode ? e.which : e.charCode);
  if(!specialCharRegex.test(pressedKey)){
    e.preventDefault();
    return false;
  }
}
