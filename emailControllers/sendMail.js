// importing transporter
const transporter = require("../config/emailConfig");

// function to send email
/**
 * This function sends email to the registered email account.
 * This function gets object which should contain the following parameters:
 * Must Required Params ==> 1. from: email of the user from which this email will be sent or can be a heading.
 *                          2. to: email address of the user which thes is going to be sent. It can be a single email of a list of emails with comma seperated values. i.e. 'example1@gmail.com, example2.gmail.com'.
 * Optional Params ==> 1. subject: the subject of the email.
 *                     2. text: the text which will be sent through an email.
 *                     3. html: the html file to be sent as email.
 *                     4. attachments: this is the array of files to be sent
 * @param {*} object 
 * @returns 
 */
const sendMail = async (emailOptions) =>{


    // const emailOptions = {
    //     from: object.from,
    //     to: object.to,
    //     subject: object.subject,
    //     text: object.text || "",
    //     html: object.html || "",
    //     attachments: object.attachments || ""
    // };

    const mailTransporter = await transporter();
    

    // this will take a couple of seconds to return a response, because here we are waiting for sending the email
    // the Promise() takes a callback function which returns two objects, either resolve or reject. Resolve is returned when all process gets completed without any problem, otherwise it returns reject.
    const result = await new Promise((resolve, reject) => {
        mailTransporter.sendMail(emailOptions, (err, info) => {

           if (err) {
            //   console.log("error: ", err);
              reject(err); // ==> returning using rejecting the error occured
           } else {
            //   console.log(`Mail sent successfully!`);
              resolve(info); //==> reutrning using resolve when email has been sent
           }

        });//end sendMail
     });//end Promise()

     return result;
    
}//end sendMail

// exporting mailFunction
module.exports = sendMail;